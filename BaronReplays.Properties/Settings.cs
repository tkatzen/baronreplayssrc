using System;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;
namespace BaronReplays.Properties
{
	[GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0"), System.Runtime.CompilerServices.CompilerGenerated]
	internal sealed class Settings : ApplicationSettingsBase
	{
		private static Settings eval_a;
		public static Settings Default
		{
			get
			{
				switch ((1584988358 == 1584988358) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return Settings.eval_a;
			}
		}
		[DefaultSettingValue(""), UserScopedSetting, System.Diagnostics.DebuggerNonUserCode]
		public string ReplayDir
		{
			get
			{
				switch ((339367907 == 339367907) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return (string)this["ReplayDir"];
			}
			set
			{
				switch ((540426048 == 540426048) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this["ReplayDir"] = value;
			}
		}
		[DefaultSettingValue("True"), UserScopedSetting, System.Diagnostics.DebuggerNonUserCode]
		public bool KillProcess
		{
			get
			{
				switch ((1941029031 == 1941029031) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return (bool)this["KillProcess"];
			}
			set
			{
				switch ((597944891 == 597944891) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				this["KillProcess"] = value;
			}
		}
		[DefaultSettingValue("TW"), UserScopedSetting, System.Diagnostics.DebuggerNonUserCode]
		public string Platform
		{
			get
			{
				switch ((986624454 == 986624454) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return (string)this["Platform"];
			}
			set
			{
				switch ((1490751631 == 1490751631) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this["Platform"] = value;
			}
		}
		[DefaultSettingValue("<SummonerName>-<Champion> <Year>-<Month>-<Day> <Hour>-<Minute>-<Second> <WinLose>.lpr"), UserScopedSetting, System.Diagnostics.DebuggerNonUserCode]
		public string FileNameFormat
		{
			get
			{
				switch ((14199379 == 14199379) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return (string)this["FileNameFormat"];
			}
			set
			{
				switch ((1422018153 == 1422018153) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				this["FileNameFormat"] = value;
			}
		}
		[DefaultSettingValue("False"), UserScopedSetting, System.Diagnostics.DebuggerNonUserCode]
		public bool AlwaysTaskbar
		{
			get
			{
				switch ((942785535 == 942785535) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return (bool)this["AlwaysTaskbar"];
			}
			set
			{
				switch ((1715234734 == 1715234734) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				this["AlwaysTaskbar"] = value;
			}
		}
		[DefaultSettingValue("False"), UserScopedSetting, System.Diagnostics.DebuggerNonUserCode]
		public bool StartWithSystem
		{
			get
			{
				switch ((27575353 == 27575353) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return (bool)this["StartWithSystem"];
			}
			set
			{
				switch ((2098009136 == 2098009136) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this["StartWithSystem"] = value;
			}
		}
		[DefaultSettingValue("False"), UserScopedSetting, System.Diagnostics.DebuggerNonUserCode]
		public bool CreateShortCutOnDesktop
		{
			get
			{
				if (1 != 0)
				{
				}
				switch ((1385239968 == 1385239968) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_28:
					break;
				case 1:
					goto IL_29;
				}
				goto IL_28;
				IL_29:
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return (bool)this["CreateShortCutOnDesktop"];
			}
			set
			{
				switch ((1135026327 == 1135026327) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this["CreateShortCutOnDesktop"] = value;
			}
		}
		[DefaultSettingValue(""), UserScopedSetting, System.Diagnostics.DebuggerNonUserCode]
		public string LoLGameExe
		{
			get
			{
				switch ((363525726 == 363525726) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return (string)this["LoLGameExe"];
			}
			set
			{
				switch ((1233550310 == 1233550310) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this["LoLGameExe"] = value;
			}
		}
		[DefaultSettingValue("3.14.41"), UserScopedSetting, System.Diagnostics.DebuggerNonUserCode]
		public string NAVersion
		{
			get
			{
				switch ((1850704860 == 1850704860) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return (string)this["NAVersion"];
			}
			set
			{
				switch ((1295990767 == 1295990767) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				this["NAVersion"] = value;
			}
		}
		[DefaultSettingValue(""), UserScopedSetting, System.Diagnostics.DebuggerNonUserCode]
		public string Language
		{
			get
			{
				switch ((91729238 == 91729238) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				return (string)this["Language"];
			}
			set
			{
				switch ((419454103 == 419454103) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				this["Language"] = value;
			}
		}
		[DefaultSettingValue("True"), UserScopedSetting, System.Diagnostics.DebuggerNonUserCode]
		public bool LprFileSupport
		{
			get
			{
				switch ((1743383994 == 1743383994) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				return (bool)this["LprFileSupport"];
			}
			set
			{
				switch ((1425507323 == 1425507323) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this["LprFileSupport"] = value;
			}
		}
		static Settings()
		{
			// Note: this type is marked as 'beforefieldinit'.
			switch ((1469369481 == 1469369481) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			Settings.eval_a = (Settings)SettingsBase.Synchronized(new Settings());
		}
	}
}
