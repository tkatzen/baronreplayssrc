using System;
using System.Runtime.InteropServices;
[System.AttributeUsage(System.AttributeTargets.Assembly), System.Runtime.InteropServices.ComVisible(false)]
public sealed class DotfuscatorAttribute : System.Attribute
{
	private string a;
	private bool b;
	private int c;
	public string A
	{
		get
		{
			return this.a;
		}
	}
	public bool B
	{
		get
		{
			return this.b;
		}
	}
	public int C
	{
		get
		{
			return this.c;
		}
	}
	public DotfuscatorAttribute(string a, int c, bool b)
	{
		this.a = a;
		this.c = c;
		this.b = b;
	}
}
