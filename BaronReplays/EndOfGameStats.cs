using FluorineFx;
using FluorineFx.AMF3;
using FluorineFx.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;
namespace BaronReplays
{
	public class EndOfGameStats : INotifyPropertyChanged
	{
		private PropertyChangedEventHandler eval_a;
		private byte[] eval_b;
		private bool eval_c;
		private bool eval_d;
		private string eval_e;
		private ulong eval_f;
		private uint eval_g;
		private string eval_h;
		private System.Collections.Generic.List<PlayerStats> eval_i;
		private string eval_j;
		private string eval_k;
		private bool eval_l;
		public event PropertyChangedEventHandler PropertyChanged
		{
			add
			{
				int num;
				PropertyChangedEventHandler propertyChangedEventHandler;
				switch (0)
				{
				case 0:
					break;
				default:
					while (true)
					{
						IL_0A:
						if (1 != 0)
						{
						}
						PropertyChangedEventHandler propertyChangedEventHandler2;
						switch (num)
						{
						case 0:
							return;
						case 1:
							if (propertyChangedEventHandler == propertyChangedEventHandler2)
							{
								num = 0;
								continue;
							}
							goto IL_6A;
						case 2:
							switch ((1697686403 == 1697686403) ? 1 : 0)
							{
							case 0:
							case 2:
								goto IL_39;
							case 1:
								IL_61:
								if (0 != 0)
								{
								}
								goto IL_6A;
							}
							goto IL_61;
						}
						goto IL_26;
						IL_6A:
						propertyChangedEventHandler2 = propertyChangedEventHandler;
						PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)System.Delegate.Combine(propertyChangedEventHandler2, value);
						propertyChangedEventHandler = System.Threading.Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.eval_a, value2, propertyChangedEventHandler2);
						num = 1;
					}
					return;
				}
				IL_26:
				int arg_2A_0 = 0;
				propertyChangedEventHandler = this.eval_a;
				IL_39:
				num = 2;
				goto IL_0A;
			}
			remove
			{
				int num;
				PropertyChangedEventHandler propertyChangedEventHandler;
				switch (0)
				{
				case 0:
					break;
				default:
					while (true)
					{
						IL_0A:
						PropertyChangedEventHandler propertyChangedEventHandler2;
						switch (num)
						{
						case 0:
							if (propertyChangedEventHandler == propertyChangedEventHandler2)
							{
								num = 1;
								continue;
							}
							goto IL_60;
						case 1:
							return;
						case 2:
							switch ((1345874929 == 1345874929) ? 1 : 0)
							{
							case 0:
							case 2:
								goto IL_23;
							case 1:
								IL_57:
								if (0 != 0)
								{
								}
								goto IL_60;
							}
							goto IL_57;
						}
						goto IL_1C;
						IL_60:
						propertyChangedEventHandler2 = propertyChangedEventHandler;
						PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)System.Delegate.Remove(propertyChangedEventHandler2, value);
						propertyChangedEventHandler = System.Threading.Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.eval_a, value2, propertyChangedEventHandler2);
						if (1 != 0)
						{
						}
						num = 0;
					}
					return;
				}
				IL_1C:
				propertyChangedEventHandler = this.eval_a;
				IL_23:
				int arg_27_0 = 0;
				num = 2;
				goto IL_0A;
			}
		}
		public bool Broken
		{
			get
			{
				int arg_04_0 = 0;
				switch ((249399543 == 249399543) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				return this.eval_c;
			}
		}
		public bool Ranked
		{
			get
			{
				int arg_04_0 = 0;
				if (1 != 0)
				{
				}
				switch ((1925524000 == 1925524000) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_34:
					break;
				case 1:
					goto IL_35;
				}
				goto IL_34;
				IL_35:
				if (0 != 0)
				{
				}
				this.DecodeData();
				return this.eval_d;
			}
			set
			{
				int arg_04_0 = 0;
				switch ((966848842 == 966848842) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				this.eval_d = value;
				this.eval_a("Ranked");
			}
		}
		public string GameType
		{
			get
			{
				int arg_04_0 = 0;
				switch ((648570937 == 648570937) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				this.DecodeData();
				return this.eval_e;
			}
			set
			{
				int arg_04_0 = 0;
				switch ((1553547893 == 1553547893) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				this.eval_e = value;
				this.eval_a("GameType");
			}
		}
		public ulong GameId
		{
			get
			{
				int arg_04_0 = 0;
				switch ((953419910 == 953419910) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				this.DecodeData();
				return this.eval_f;
			}
			set
			{
				int arg_04_0 = 0;
				switch ((1134991164 == 1134991164) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				this.eval_f = value;
				this.eval_a("GameId");
			}
		}
		public uint GameLength
		{
			get
			{
				int arg_04_0 = 0;
				if (1 != 0)
				{
				}
				switch ((2080469425 == 2080469425) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_34:
					break;
				case 1:
					goto IL_35;
				}
				goto IL_34;
				IL_35:
				if (0 != 0)
				{
				}
				this.DecodeData();
				return this.eval_g;
			}
			set
			{
				int arg_04_0 = 0;
				if (1 != 0)
				{
				}
				switch ((1467372439 == 1467372439) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_34:
					break;
				case 1:
					goto IL_35;
				}
				goto IL_34;
				IL_35:
				if (0 != 0)
				{
				}
				this.eval_g = value;
				this.eval_a("GameLength");
			}
		}
		public string GameMode
		{
			get
			{
				int arg_04_0 = 0;
				switch ((459430841 == 459430841) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				this.DecodeData();
				return this.eval_h;
			}
			set
			{
				int arg_04_0 = 0;
				if (1 != 0)
				{
				}
				switch ((2009889884 == 2009889884) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_34:
					break;
				case 1:
					goto IL_35;
				}
				goto IL_34;
				IL_35:
				if (0 != 0)
				{
				}
				this.eval_h = value;
				this.eval_a("GameMode");
			}
		}
		public System.Collections.Generic.List<PlayerStats> Players
		{
			get
			{
				int arg_04_0 = 0;
				switch ((1402375804 == 1402375804) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				this.DecodeData();
				return this.eval_i;
			}
			set
			{
				int arg_04_0 = 0;
				switch ((2043720706 == 2043720706) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				this.eval_i = value;
				this.eval_a("Players");
			}
		}
		public string BlueTeamKDA
		{
			get
			{
				uint num;
				uint num2;
				uint num3;
				while (true)
				{
					switch (0)
					{
					case 0:
						IL_0F:
						this.DecodeData();
						num = 0u;
						num2 = 0u;
						num3 = 0u;
						using (System.Collections.Generic.List<PlayerStats>.Enumerator enumerator = this.Players.GetEnumerator())
						{
							int num4 = 2;
							while (true)
							{
								switch (num4)
								{
								case 0:
								{
									PlayerStats current;
									num += current.DetailStats.K;
									num2 += current.DetailStats.D;
									num3 += current.DetailStats.A;
									num4 = 3;
									continue;
								}
								case 1:
								{
									if (!enumerator.MoveNext())
									{
										num4 = 4;
										continue;
									}
									PlayerStats current = enumerator.Current;
									num4 = 5;
									continue;
								}
								case 2:
									switch (0)
									{
									case 0:
										goto IL_62;
									}
									continue;
								case 4:
									num4 = 6;
									continue;
								case 5:
								{
									PlayerStats current;
									if (current.TeamId == 100u)
									{
										num4 = 0;
										continue;
									}
									break;
								}
								case 6:
									goto IL_FB;
								}
								IL_8C:
								if (1 != 0)
								{
								}
								num4 = 1;
								continue;
								IL_62:
								goto IL_8C;
							}
							IL_FB:;
						}
						switch ((853654065 == 853654065) ? 1 : 0)
						{
						case 0:
						case 2:
							continue;
						}
						goto IL_12A;
					}
					goto IL_0F;
				}
				IL_12A:
				int arg_12E_0 = 0;
				if (0 != 0)
				{
				}
				return string.Format("{0,-2}/{1,-2}/{2,-2}", num, num2, num3);
			}
		}
		public string WonTeam
		{
			get
			{
				int num = 8;
				while (true)
				{
					switch (num)
					{
					case 0:
						goto IL_65;
					case 1:
						switch ((1562778262 == 1562778262) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_65;
						case 1:
							IL_16A:
							if (0 != 0)
							{
							}
							num = 0;
							continue;
						}
						goto IL_16A;
					case 2:
						goto IL_5A;
					case 3:
						num = 6;
						continue;
					case 4:
						if (1 != 0)
						{
						}
						goto IL_11B;
					case 5:
						if (this.Players[0].DetailStats.Win)
						{
							num = 3;
							continue;
						}
						goto IL_11B;
					case 6:
						if (this.Players[0].TeamId != 100u)
						{
							num = 4;
							continue;
						}
						goto IL_DB;
					case 7:
						if (!this.Players[0].DetailStats.Win)
						{
							num = 1;
							continue;
						}
						goto IL_176;
					case 8:
						switch (0)
						{
						case 0:
							goto IL_41;
						}
						continue;
					case 9:
						goto IL_89;
					}
					IL_41:
					if (this.Players.Count == 0)
					{
						num = 2;
						continue;
					}
					num = 5;
					continue;
					IL_65:
					if (this.Players[0].TeamId == 200u)
					{
						num = 9;
						continue;
					}
					goto IL_176;
					IL_11B:
					num = 7;
				}
				IL_5A:
				int arg_C8_0 = 0;
				return Utilities.GetString("NoResult");
				IL_89:
				IL_DB:
				return Utilities.GetString("BlueTeamWon");
				IL_176:
				return Utilities.GetString("PurpleTeamWon");
			}
		}
		public string PurpleTeamKDA
		{
			get
			{
				uint num;
				uint num2;
				uint num3;
				while (true)
				{
					switch (0)
					{
					case 0:
						IL_0F:
						if (1 != 0)
						{
						}
						this.DecodeData();
						num = 0u;
						num2 = 0u;
						num3 = 0u;
						using (System.Collections.Generic.List<PlayerStats>.Enumerator enumerator = this.Players.GetEnumerator())
						{
							int num4 = 0;
							while (true)
							{
								switch (num4)
								{
								case 0:
									switch (0)
									{
									case 0:
										goto IL_6C;
									}
									continue;
								case 1:
								{
									PlayerStats current;
									if (current.TeamId == 200u)
									{
										num4 = 5;
										continue;
									}
									break;
								}
								case 2:
									num4 = 4;
									continue;
								case 4:
									goto IL_FE;
								case 5:
								{
									PlayerStats current;
									num += current.DetailStats.K;
									num2 += current.DetailStats.D;
									num3 += current.DetailStats.A;
									num4 = 3;
									continue;
								}
								case 6:
								{
									if (!enumerator.MoveNext())
									{
										num4 = 2;
										continue;
									}
									PlayerStats current = enumerator.Current;
									num4 = 1;
									continue;
								}
								}
								IL_99:
								num4 = 6;
								continue;
								IL_6C:
								goto IL_99;
							}
							IL_FE:;
						}
						switch ((1979405294 == 1979405294) ? 1 : 0)
						{
						case 0:
						case 2:
							continue;
						}
						goto IL_12D;
					}
					goto IL_0F;
				}
				IL_12D:
				if (0 != 0)
				{
				}
				int arg_138_0 = 0;
				return string.Format("{0,-2}/{1,-2}/{2,-2}", num, num2, num3);
			}
		}
		public string BlueTeamInfo
		{
			get
			{
				switch ((1963620381 == 1963620381) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				int arg_2D_0 = 0;
				if (0 != 0)
				{
				}
				this.DecodeData();
				return this.eval_j;
			}
			set
			{
				if (1 != 0)
				{
				}
				switch ((327066362 == 327066362) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_28:
					break;
				case 1:
					goto IL_29;
				}
				goto IL_28;
				IL_29:
				int arg_2D_0 = 0;
				if (0 != 0)
				{
				}
				this.eval_j = value;
				this.eval_a("BlueTeamInfo");
			}
		}
		public string PurpleTeamInfo
		{
			get
			{
				switch ((636641489 == 636641489) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				int arg_2D_0 = 0;
				if (0 != 0)
				{
				}
				this.DecodeData();
				return this.eval_k;
			}
			set
			{
				switch ((1825371977 == 1825371977) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				int arg_2D_0 = 0;
				if (0 != 0)
				{
				}
				this.eval_k = value;
				this.eval_a("PurpleTeamInfo");
			}
		}
		private void eval_a(string A_0)
		{
			int num = 0;
			while (true)
			{
				switch (num)
				{
				case 0:
					IL_09:
					switch (0)
					{
					case 0:
						goto IL_25;
					}
					continue;
				case 1:
					if (1 != 0)
					{
					}
					this.eval_a(this, new PropertyChangedEventArgs(A_0));
					num = 2;
					continue;
				case 2:
					goto IL_6B;
				}
				IL_25:
				int arg_29_0 = 0;
				if (this.eval_a != null)
				{
					num = 1;
					continue;
				}
				IL_6B:
				switch ((31204308 == 31204308) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_09;
				}
				break;
			}
			if (0 != 0)
			{
			}
		}
		public bool DecodeData()
		{
			switch (0)
			{
			case 0:
			{
				IL_0F:
				int num = 0;
				while (true)
				{
					int arg_27_0 = 0;
					if (1 != 0)
					{
					}
					switch (num)
					{
					case 0:
						switch (0)
						{
						case 0:
							goto IL_4C;
						}
						continue;
					case 1:
						goto IL_3CA;
					case 2:
						goto IL_61;
					}
					IL_4C:
					if (this.eval_l)
					{
						num = 2;
					}
					else
					{
						this.eval_l = true;
						System.IO.Stream stream = new System.IO.MemoryStream(this.eval_b);
						ASObject aSObject = null;
						AMFReader aMFReader = new AMFReader(stream);
						num = 1;
					}
				}
				IL_61:
				IL_395:
				return !this.Broken;
				IL_3CA:
				try
				{
					try
					{
						switch (0)
						{
						case 0:
							goto IL_A7;
						}
						ASObject aSObject;
						ArrayCollection arrayCollection;
						int num3;
						ArrayCollection arrayCollection2;
						while (true)
						{
							IL_70:
							switch (num)
							{
							case 0:
								goto IL_305;
							case 1:
							{
								ASObject aSObject2 = aSObject["myTeamInfo"] as ASObject;
								this.BlueTeamInfo = string.Format("[{0}]{1}", aSObject2["tag"], aSObject2["name"]);
								aSObject2 = (aSObject["otherTeamInfo"] as ASObject);
								this.PurpleTeamInfo = string.Format("[{0}]{1}", aSObject2["tag"], aSObject2["name"]);
								num = 0;
								continue;
							}
							case 2:
								if (aSObject["myTeamInfo"] != null)
								{
									num = 1;
									continue;
								}
								goto IL_305;
							case 3:
								goto IL_312;
							case 4:
							{
								int num2;
								if (num2 >= arrayCollection.Count)
								{
									num = 10;
									continue;
								}
								this.Players.Add(new PlayerStats(arrayCollection[num2] as ASObject));
								num2++;
								num = 8;
								continue;
							}
							case 5:
								goto IL_17D;
							case 6:
								goto IL_2D9;
							case 7:
								if (num3 >= arrayCollection2.Count)
								{
									num = 11;
									continue;
								}
								this.Players.Add(new PlayerStats(arrayCollection2[num3] as ASObject));
								num3++;
								num = 9;
								continue;
							case 8:
								goto IL_17D;
							case 9:
								goto IL_2D9;
							case 10:
								num = 2;
								continue;
							case 11:
							{
								int num2 = 0;
								num = 5;
								continue;
							}
							}
							goto IL_A7;
							IL_17D:
							num = 4;
							continue;
							IL_2D9:
							num = 7;
							continue;
							IL_305:
							num = 3;
						}
						IL_312:
						goto IL_324;
						IL_A7:
						AMFReader aMFReader;
						aSObject = (ASObject)aMFReader.ReadAMF3Data();
						this.Ranked = (bool)aSObject["ranked"];
						this.GameType = (aSObject["gameType"] as string);
						this.GameLength = uint.Parse(aSObject["gameLength"].ToString());
						this.GameMode = (aSObject["gameMode"] as string);
						this.GameId = ulong.Parse(aSObject["gameId"].ToString());
						arrayCollection2 = (aSObject["teamPlayerParticipantStats"] as ArrayCollection);
						arrayCollection = (aSObject["otherTeamPlayerParticipantStats"] as ArrayCollection);
						int arg_154_0 = arrayCollection2.Count;
						int arg_15C_0 = arrayCollection.Count;
						this.Players = new System.Collections.Generic.List<PlayerStats>();
						num3 = 0;
						num = 6;
						goto IL_70;
					}
					catch (System.Exception)
					{
						this.eval_c = true;
						return false;
					}
					IL_324:
					return true;
				}
				finally
				{
					num = 0;
					while (true)
					{
						AMFReader aMFReader;
						switch (num)
						{
						case 0:
							IL_330:
							switch (0)
							{
							case 0:
								goto IL_34D;
							}
							continue;
						case 1:
							((System.IDisposable)aMFReader).Dispose();
							num = 2;
							continue;
						case 2:
							goto IL_36E;
						}
						IL_34D:
						if (aMFReader != null)
						{
							num = 1;
							continue;
						}
						IL_36E:
						switch ((881487772 == 881487772) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_330;
						}
						break;
					}
					if (0 != 0)
					{
					}
				}
				goto IL_395;
			}
			}
			goto IL_0F;
		}
		public EndOfGameStats()
		{
			this.eval_c = false;
		}
		public EndOfGameStats(byte[] statBytes)
		{
			this.eval_c = false;
			this.eval_b = statBytes;
		}
	}
}
