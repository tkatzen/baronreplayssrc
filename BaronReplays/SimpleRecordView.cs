using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
namespace BaronReplays
{
	public class SimpleRecordView : UserControl, IComponentConnector
	{
		public MainWindow.RecordDelegate WatchClickEvent;
		internal TextBlock FileNameTextBox;
		internal TriStateButton RenameButton;
		internal TriStateButton DeleteButton;
		internal TriStateButton WatchButton;
		private bool _contentLoaded;
		public SimpleRecordView()
		{
			this.InitializeComponent();
		}
		private void eval_c(object A_0, RoutedEventArgs A_1)
		{
			int arg_04_0 = 0;
			switch ((1020038224 == 1020038224) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			SimpleLoLRecord simpleLoLRecord = base.DataContext as SimpleLoLRecord;
			simpleLoLRecord.PlayRecord();
		}
		private void eval_b(object A_0, RoutedEventArgs A_1)
		{
			int arg_04_0 = 0;
			int num = 1;
			while (true)
			{
				switch (num)
				{
				case 0:
					return;
				case 1:
					switch (0)
					{
					case 0:
						goto IL_31;
					}
					continue;
				case 2:
					switch ((1945017621 == 1945017621) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					case 1:
					{
						IL_97:
						if (0 != 0)
						{
						}
						SimpleLoLRecord simpleLoLRecord = base.DataContext as SimpleLoLRecord;
						simpleLoLRecord.DeleteRecord();
						num = 0;
						continue;
					}
					}
					goto IL_97;
				}
				IL_31:
				if (1 != 0)
				{
				}
				if (MessageBox.Show(string.Format(Utilities.GetString("SureDelete"), (base.DataContext as SimpleLoLRecord).FileNameShort), Utilities.GetString("BR"), MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
				{
					break;
				}
				num = 2;
			}
		}
		private void eval_a(object A_0, RoutedEventArgs A_1)
		{
			int arg_04_0 = 0;
			switch ((587722671 == 587722671) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			SimpleLoLRecord simpleLoLRecord = base.DataContext as SimpleLoLRecord;
			simpleLoLRecord.ShowRenameDialog();
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), System.Diagnostics.DebuggerNonUserCode]
		public void InitializeComponent()
		{
			while (this._contentLoaded)
			{
				int arg_0C_0 = 0;
				switch ((442573655 == 442573655) ? 1 : 0)
				{
				case 0:
				case 2:
					continue;
				case 1:
					IL_35:
					if (1 != 0)
					{
					}
					if (0 != 0)
					{
					}
					return;
				}
				goto IL_35;
			}
			this._contentLoaded = true;
			Uri resourceLocator = new Uri("/BaronReplays;component/simplerecordview.xaml", UriKind.Relative);
			Application.LoadComponent(this, resourceLocator);
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), System.Diagnostics.DebuggerNonUserCode]
		internal System.Delegate eval_a(System.Type A_0, string A_1)
		{
			int arg_04_0 = 0;
			switch ((1635760965 == 1635760965) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			return System.Delegate.CreateDelegate(A_0, this, A_1);
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), System.Diagnostics.DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			int num;
			switch ((268065049 == 268065049) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_7A:
				num = 1;
				goto IL_3C;
			case 1:
			{
				IL_1F:
				int arg_23_0 = 0;
				if (0 != 0)
				{
				}
				switch (0)
				{
				case 0:
					goto IL_4E;
				}
				goto IL_3C;
			}
			}
			goto IL_1F;
			while (true)
			{
				IL_3C:
				switch (num)
				{
				case 0:
					goto IL_7A;
				case 1:
					goto IL_9F;
				case 2:
					switch (connectionId)
					{
					case 1:
						goto IL_AE;
					case 2:
						goto IL_89;
					case 3:
						goto IL_7C;
					case 4:
						goto IL_A1;
					default:
						num = 0;
						continue;
					}
					break;
				}
				goto IL_4E;
			}
			IL_7C:
			this.DeleteButton = (TriStateButton)target;
			return;
			IL_89:
			this.RenameButton = (TriStateButton)target;
			return;
			IL_9F:
			if (1 != 0)
			{
			}
			this._contentLoaded = true;
			return;
			IL_A1:
			this.WatchButton = (TriStateButton)target;
			return;
			IL_AE:
			this.FileNameTextBox = (TextBlock)target;
			return;
			IL_4E:
			num = 2;
			goto IL_3C;
		}
	}
}
