using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Data;
using System.Windows.Media.Imaging;
namespace BaronReplays
{
	public class SpellNumberToSpellIcon : IValueConverter
	{
		public static string SpellDir;
		public static string SpellDirFullPath;
		public static object Lock;
		public static System.Collections.Generic.Dictionary<string, BitmapImage> SpellNumberToBitmapImage;
		public static void CheckSpellDir()
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_34;
				}
				int num2;
				string[] array;
				while (true)
				{
					IL_19:
					string text;
					switch (num)
					{
					case 0:
						goto IL_CA;
					case 1:
						if (num2 >= array.Length)
						{
							num = 4;
							continue;
						}
						goto IL_B7;
					case 2:
						try
						{
							SpellNumberToSpellIcon.SpellNumberToBitmapImage.Add(text.Substring(text.LastIndexOf('\\') + 1), Utilities.GetBitmapImage(text));
							goto IL_74;
						}
						catch
						{
							if (System.IO.File.Exists(text))
							{
								System.IO.File.Delete(text);
							}
							goto IL_74;
						}
						return;
						IL_74:
						switch ((460153027 == 460153027) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_B7;
						case 1:
							IL_93:
							if (0 != 0)
							{
							}
							if (1 != 0)
							{
							}
							num2++;
							num = 3;
							continue;
						}
						goto IL_93;
					case 3:
						goto IL_CA;
					case 4:
						return;
					}
					goto IL_34;
					IL_B7:
					text = array[num2];
					num = 2;
					continue;
					IL_CA:
					num = 1;
				}
				return;
				IL_34:
				Utilities.IfDirectoryNotExitThenCreate(SpellNumberToSpellIcon.SpellDirFullPath);
				string[] files = System.IO.Directory.GetFiles(SpellNumberToSpellIcon.SpellDirFullPath, "*.png");
				SpellNumberToSpellIcon.SpellNumberToBitmapImage = new System.Collections.Generic.Dictionary<string, BitmapImage>();
				array = files;
				num2 = 0;
				int arg_60_0 = 0;
				num = 0;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (1 != 0)
			{
			}
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_19:
				string text = (int)value + ".png";
				bool flag = false;
				object result;
				try
				{
					switch (0)
					{
					case 0:
						goto IL_68;
					}
					while (true)
					{
						IL_55:
						switch (num)
						{
						case 0:
							if (!SpellNumberToSpellIcon.SpellNumberToBitmapImage.ContainsKey(text))
							{
								num = 1;
								continue;
							}
							goto IL_17B;
						case 1:
							try
							{
								WebClient webClient = new WebClient();
								try
								{
									webClient.DownloadFile("https://sites.google.com/site/ahritw/" + text, SpellNumberToSpellIcon.SpellDir + text);
								}
								finally
								{
									num = 0;
									while (true)
									{
										switch (num)
										{
										case 0:
											switch (0)
											{
											case 0:
												goto IL_EA;
											}
											continue;
										case 1:
											((System.IDisposable)webClient).Dispose();
											num = 2;
											continue;
										case 2:
											goto IL_109;
										}
										IL_EA:
										if (webClient == null)
										{
											break;
										}
										num = 1;
									}
									IL_109:;
								}
							}
							catch
							{
								result = null;
								return result;
							}
							try
							{
								SpellNumberToSpellIcon.SpellNumberToBitmapImage.Add(text, Utilities.GetBitmapImage(SpellNumberToSpellIcon.SpellDirFullPath + text));
							}
							catch
							{
								string path = SpellNumberToSpellIcon.SpellDirFullPath + text;
								if (!System.IO.File.Exists(path))
								{
									goto IL_153;
								}
								IL_149:
								System.IO.File.Delete(path);
								IL_153:
								switch ((615283232 == 615283232) ? 1 : 0)
								{
								case 0:
								case 2:
									goto IL_149;
								case 1:
									IL_172:
									if (0 != 0)
									{
									}
									goto IL_17B;
								}
								goto IL_172;
							}
							goto IL_17B;
						case 2:
							goto IL_188;
						}
						goto IL_68;
						IL_17B:
						num = 2;
					}
					IL_188:
					goto IL_33;
					IL_68:
					object @lock;
					System.Threading.Monitor.Enter(@lock = SpellNumberToSpellIcon.Lock, ref flag);
					num = 0;
					goto IL_55;
				}
				finally
				{
					num = 1;
					while (true)
					{
						switch (num)
						{
						case 0:
							goto IL_1D4;
						case 1:
							switch (0)
							{
							case 0:
								goto IL_1B4;
							}
							continue;
						case 2:
						{
							object @lock;
							System.Threading.Monitor.Exit(@lock);
							num = 0;
							continue;
						}
						}
						IL_1B4:
						if (!flag)
						{
							break;
						}
						num = 2;
					}
					IL_1D4:;
				}
				return result;
				IL_33:
				int arg_37_0 = 0;
				return SpellNumberToSpellIcon.SpellNumberToBitmapImage[text];
			}
			}
			goto IL_19;
		}
		public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			switch ((1457484783 == 1457484783) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (0 != 0)
			{
			}
			int arg_2A_0 = 0;
			if (1 != 0)
			{
			}
			throw new System.NotImplementedException();
		}
		static SpellNumberToSpellIcon()
		{
			// Note: this type is marked as 'beforefieldinit'.
			switch ((1684473790 == 1684473790) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			int arg_2D_0 = 0;
			if (0 != 0)
			{
			}
			SpellNumberToSpellIcon.SpellDir = "Spells\\";
			SpellNumberToSpellIcon.SpellDirFullPath = System.AppDomain.CurrentDomain.BaseDirectory + SpellNumberToSpellIcon.SpellDir;
			SpellNumberToSpellIcon.Lock = new object();
		}
	}
}
