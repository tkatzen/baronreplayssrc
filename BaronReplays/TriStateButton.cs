using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
namespace BaronReplays
{
	public class TriStateButton : UserControl, INotifyPropertyChanged, IComponentConnector
	{
		public static readonly DependencyProperty ContentStringProperty;
		internal Button MainButton;
		private bool _contentLoaded;
		public event RoutedEventHandler Click
		{
			add
			{
				switch (0)
				{
				case 0:
					goto IL_1C;
				}
				int num;
				RoutedEventHandler routedEventHandler;
				while (true)
				{
					IL_0A:
					RoutedEventHandler routedEventHandler2;
					switch (num)
					{
					case 0:
						goto IL_96;
					case 1:
						if (routedEventHandler == routedEventHandler2)
						{
							num = 0;
							continue;
						}
						goto IL_54;
					case 2:
						switch ((1052000825 == 1052000825) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_1C;
						case 1:
							IL_4B:
							if (0 != 0)
							{
							}
							goto IL_54;
						}
						goto IL_4B;
					}
					goto IL_1C;
					IL_54:
					routedEventHandler2 = routedEventHandler;
					RoutedEventHandler value2 = (RoutedEventHandler)System.Delegate.Combine(routedEventHandler2, value);
					routedEventHandler = System.Threading.Interlocked.CompareExchange<RoutedEventHandler>(ref this.Click, value2, routedEventHandler2);
					int arg_75_0 = 0;
					num = 1;
				}
				IL_96:
				if (1 != 0)
				{
				}
				return;
				IL_1C:
				routedEventHandler = this.Click;
				num = 2;
				goto IL_0A;
			}
			remove
			{
				switch (0)
				{
				case 0:
					goto IL_1C;
				}
				int num;
				RoutedEventHandler routedEventHandler;
				while (true)
				{
					IL_0A:
					RoutedEventHandler routedEventHandler2;
					switch (num)
					{
					case 0:
						return;
					case 1:
						if (routedEventHandler == routedEventHandler2)
						{
							num = 0;
							continue;
						}
						goto IL_5E;
					case 2:
						switch ((54337636 == 54337636) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_1C;
						case 1:
							IL_4B:
							if (1 != 0)
							{
							}
							if (0 != 0)
							{
							}
							goto IL_5E;
						}
						goto IL_4B;
					}
					goto IL_1C;
					IL_5E:
					int arg_62_0 = 0;
					routedEventHandler2 = routedEventHandler;
					RoutedEventHandler value2 = (RoutedEventHandler)System.Delegate.Remove(routedEventHandler2, value);
					routedEventHandler = System.Threading.Interlocked.CompareExchange<RoutedEventHandler>(ref this.Click, value2, routedEventHandler2);
					num = 1;
				}
				return;
				IL_1C:
				routedEventHandler = this.Click;
				num = 2;
				goto IL_0A;
			}
		}
		public event PropertyChangedEventHandler PropertyChanged
		{
			add
			{
				switch (0)
				{
				case 0:
					goto IL_1C;
				}
				int num;
				PropertyChangedEventHandler propertyChangedEventHandler;
				while (true)
				{
					IL_0A:
					PropertyChangedEventHandler propertyChangedEventHandler2;
					switch (num)
					{
					case 0:
						switch ((702240335 == 702240335) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_1C;
						case 1:
							IL_4B:
							if (1 != 0)
							{
							}
							if (0 != 0)
							{
							}
							goto IL_5E;
						}
						goto IL_4B;
					case 1:
						if (propertyChangedEventHandler == propertyChangedEventHandler2)
						{
							num = 2;
							continue;
						}
						goto IL_5E;
					case 2:
						return;
					}
					goto IL_1C;
					IL_5E:
					int arg_62_0 = 0;
					propertyChangedEventHandler2 = propertyChangedEventHandler;
					PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)System.Delegate.Combine(propertyChangedEventHandler2, value);
					propertyChangedEventHandler = System.Threading.Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.PropertyChanged, value2, propertyChangedEventHandler2);
					num = 1;
				}
				return;
				IL_1C:
				propertyChangedEventHandler = this.PropertyChanged;
				num = 0;
				goto IL_0A;
			}
			remove
			{
				switch (0)
				{
				case 0:
					goto IL_1C;
				}
				int num;
				PropertyChangedEventHandler propertyChangedEventHandler;
				while (true)
				{
					IL_0A:
					PropertyChangedEventHandler propertyChangedEventHandler2;
					switch (num)
					{
					case 0:
						switch ((72089793 == 72089793) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_1C;
						case 1:
							IL_4B:
							if (0 != 0)
							{
							}
							goto IL_54;
						}
						goto IL_4B;
					case 1:
						return;
					case 2:
						if (1 != 0)
						{
						}
						if (propertyChangedEventHandler == propertyChangedEventHandler2)
						{
							num = 1;
							continue;
						}
						goto IL_54;
					}
					goto IL_1C;
					IL_54:
					propertyChangedEventHandler2 = propertyChangedEventHandler;
					PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)System.Delegate.Remove(propertyChangedEventHandler2, value);
					propertyChangedEventHandler = System.Threading.Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.PropertyChanged, value2, propertyChangedEventHandler2);
					int arg_75_0 = 0;
					num = 2;
				}
				return;
				IL_1C:
				propertyChangedEventHandler = this.PropertyChanged;
				num = 0;
				goto IL_0A;
			}
		}
		public string ContentString
		{
			get
			{
				switch ((1178782185 == 1178782185) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				int arg_23_0 = 0;
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				return (string)base.GetValue(TriStateButton.ContentStringProperty);
			}
			set
			{
				switch ((480332258 == 480332258) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				int arg_23_0 = 0;
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				base.SetValue(TriStateButton.ContentStringProperty, value);
			}
		}
		private void eval_a(string A_0 = "")
		{
			int num = 2;
			while (true)
			{
				int arg_47_0 = 0;
				switch (num)
				{
				case 0:
					return;
				case 1:
					goto IL_74;
				case 2:
					switch ((348989623 == 348989623) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_74;
					case 1:
						IL_28:
						if (0 != 0)
						{
						}
						if (1 != 0)
						{
						}
						switch (0)
						{
						case 0:
							goto IL_61;
						}
						continue;
					}
					goto IL_28;
				}
				IL_61:
				if (this.PropertyChanged != null)
				{
					num = 1;
					continue;
				}
				break;
				IL_74:
				this.PropertyChanged(this, new PropertyChangedEventArgs(A_0));
				num = 0;
			}
		}
		static TriStateButton()
		{
			switch ((1756779054 == 1756779054) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			int arg_23_0 = 0;
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			TriStateButton.ContentStringProperty = DependencyProperty.Register("ContentString", typeof(string), typeof(TriStateButton));
		}
		public TriStateButton()
		{
			base.DataContext = this;
			this.InitializeComponent();
		}
		private void eval_a(object A_0, RoutedEventArgs A_1)
		{
			int num = 0;
			while (true)
			{
				switch (num)
				{
				case 0:
					switch ((616129646 == 616129646) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_74;
					case 1:
						IL_28:
						if (0 != 0)
						{
						}
						switch (0)
						{
						case 0:
							goto IL_4B;
						}
						continue;
					}
					goto IL_28;
				case 1:
					return;
				case 2:
					goto IL_74;
				}
				IL_4B:
				int arg_4F_0 = 0;
				if (this.Click != null)
				{
					if (1 != 0)
					{
					}
					num = 2;
					continue;
				}
				break;
				IL_74:
				this.Click(this, A_1);
				num = 1;
			}
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), System.Diagnostics.DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				switch ((1675747704 == 1675747704) ? 1 : 0)
				{
				case 0:
				case 2:
					return;
				case 1:
				{
					IL_27:
					int arg_2B_0 = 0;
					if (0 != 0)
					{
					}
					if (1 != 0)
					{
					}
					return;
				}
				}
				goto IL_27;
			}
			this._contentLoaded = true;
			Uri resourceLocator = new Uri("/BaronReplays;component/tristatebutton.xaml", UriKind.Relative);
			Application.LoadComponent(this, resourceLocator);
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), System.Diagnostics.DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				switch ((1073779699 == 1073779699) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_44;
				case 1:
				{
					IL_25:
					int arg_29_0 = 0;
					if (0 != 0)
					{
					}
					if (1 != 0)
					{
					}
					goto IL_44;
				}
				}
				goto IL_25;
				IL_44:
				this.MainButton = (Button)target;
				this.MainButton.Click += new RoutedEventHandler(this.eval_a);
				return;
			}
			this._contentLoaded = true;
		}
	}
}
