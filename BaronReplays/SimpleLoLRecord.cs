using System;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows;
namespace BaronReplays
{
	public class SimpleLoLRecord : INotifyPropertyChanged
	{
		private PropertyChangedEventHandler eval_a;
		public LoLRecord RecoringRecord;
		private string[] eval_b;
		private string[] eval_c;
		private string eval_d;
		private MainWindow.RecordDelegate eval_e;
		private MainWindow.RecordDelegate eval_f;
		public event PropertyChangedEventHandler PropertyChanged
		{
			add
			{
				int arg_04_0 = 0;
				switch (0)
				{
				case 0:
					goto IL_28;
				}
				int num;
				PropertyChangedEventHandler propertyChangedEventHandler;
				while (true)
				{
					IL_16:
					PropertyChangedEventHandler propertyChangedEventHandler2;
					switch (num)
					{
					case 0:
						goto IL_3A;
					case 1:
						switch ((283743821 == 283743821) ? 1 : 0)
						{
						case 0:
						case 2:
							return;
						case 1:
							IL_7F:
							if (0 != 0)
							{
							}
							if (propertyChangedEventHandler == propertyChangedEventHandler2)
							{
								if (1 != 0)
								{
								}
								num = 2;
								continue;
							}
							goto IL_3A;
						}
						goto IL_7F;
					case 2:
						return;
					}
					goto IL_28;
					IL_3A:
					propertyChangedEventHandler2 = propertyChangedEventHandler;
					PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)System.Delegate.Combine(propertyChangedEventHandler2, value);
					propertyChangedEventHandler = System.Threading.Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.eval_a, value2, propertyChangedEventHandler2);
					num = 1;
				}
				return;
				IL_28:
				propertyChangedEventHandler = this.eval_a;
				num = 0;
				goto IL_16;
			}
			remove
			{
				switch (0)
				{
				case 0:
					goto IL_1C;
				}
				int num;
				PropertyChangedEventHandler propertyChangedEventHandler;
				while (true)
				{
					IL_0A:
					PropertyChangedEventHandler propertyChangedEventHandler2;
					switch (num)
					{
					case 0:
						return;
					case 1:
						if (propertyChangedEventHandler == propertyChangedEventHandler2)
						{
							num = 0;
							continue;
						}
						goto IL_5E;
					case 2:
						switch ((2078907622 == 2078907622) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_1C;
						case 1:
							IL_4B:
							if (1 != 0)
							{
							}
							if (0 != 0)
							{
							}
							goto IL_5E;
						}
						goto IL_4B;
					}
					goto IL_1C;
					IL_5E:
					int arg_62_0 = 0;
					propertyChangedEventHandler2 = propertyChangedEventHandler;
					PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)System.Delegate.Remove(propertyChangedEventHandler2, value);
					propertyChangedEventHandler = System.Threading.Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.eval_a, value2, propertyChangedEventHandler2);
					num = 1;
				}
				return;
				IL_1C:
				propertyChangedEventHandler = this.eval_a;
				num = 2;
				goto IL_0A;
			}
		}
		public event MainWindow.RecordDelegate WatchClickEvent
		{
			add
			{
				switch (0)
				{
				case 0:
					goto IL_1C;
				}
				int num;
				MainWindow.RecordDelegate recordDelegate;
				while (true)
				{
					IL_0A:
					MainWindow.RecordDelegate recordDelegate2;
					switch (num)
					{
					case 0:
						if (recordDelegate == recordDelegate2)
						{
							num = 1;
							continue;
						}
						goto IL_54;
					case 1:
						goto IL_96;
					case 2:
						switch ((494446539 == 494446539) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_1C;
						case 1:
							IL_4B:
							if (0 != 0)
							{
							}
							goto IL_54;
						}
						goto IL_4B;
					}
					goto IL_1C;
					IL_54:
					recordDelegate2 = recordDelegate;
					MainWindow.RecordDelegate value2 = (MainWindow.RecordDelegate)System.Delegate.Combine(recordDelegate2, value);
					recordDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.RecordDelegate>(ref this.eval_e, value2, recordDelegate2);
					int arg_75_0 = 0;
					num = 0;
				}
				IL_96:
				if (1 != 0)
				{
				}
				return;
				IL_1C:
				recordDelegate = this.eval_e;
				num = 2;
				goto IL_0A;
			}
			remove
			{
				switch (0)
				{
				case 0:
					goto IL_26;
				}
				int num;
				MainWindow.RecordDelegate recordDelegate;
				while (true)
				{
					IL_0A:
					if (1 != 0)
					{
					}
					MainWindow.RecordDelegate recordDelegate2;
					switch (num)
					{
					case 0:
						if (recordDelegate == recordDelegate2)
						{
							num = 2;
							continue;
						}
						goto IL_5E;
					case 1:
						switch ((1784663366 == 1784663366) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_26;
						case 1:
							IL_55:
							if (0 != 0)
							{
							}
							goto IL_5E;
						}
						goto IL_55;
					case 2:
						return;
					}
					goto IL_26;
					IL_5E:
					int arg_62_0 = 0;
					recordDelegate2 = recordDelegate;
					MainWindow.RecordDelegate value2 = (MainWindow.RecordDelegate)System.Delegate.Remove(recordDelegate2, value);
					recordDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.RecordDelegate>(ref this.eval_e, value2, recordDelegate2);
					num = 0;
				}
				return;
				IL_26:
				recordDelegate = this.eval_e;
				num = 1;
				goto IL_0A;
			}
		}
		public event MainWindow.RecordDelegate DeleteClickEvent
		{
			add
			{
				if (1 != 0)
				{
				}
				switch (0)
				{
				case 0:
					goto IL_26;
				}
				int num;
				MainWindow.RecordDelegate recordDelegate;
				while (true)
				{
					IL_14:
					MainWindow.RecordDelegate recordDelegate2;
					switch (num)
					{
					case 0:
						if (recordDelegate == recordDelegate2)
						{
							num = 1;
							continue;
						}
						goto IL_5E;
					case 1:
						return;
					case 2:
						switch ((1925577524 == 1925577524) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_26;
						case 1:
							IL_55:
							if (0 != 0)
							{
							}
							goto IL_5E;
						}
						goto IL_55;
					}
					goto IL_26;
					IL_5E:
					int arg_62_0 = 0;
					recordDelegate2 = recordDelegate;
					MainWindow.RecordDelegate value2 = (MainWindow.RecordDelegate)System.Delegate.Combine(recordDelegate2, value);
					recordDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.RecordDelegate>(ref this.eval_f, value2, recordDelegate2);
					num = 0;
				}
				return;
				IL_26:
				recordDelegate = this.eval_f;
				num = 2;
				goto IL_14;
			}
			remove
			{
				if (1 != 0)
				{
				}
				switch (0)
				{
				case 0:
					goto IL_26;
				}
				int num;
				MainWindow.RecordDelegate recordDelegate;
				while (true)
				{
					IL_14:
					MainWindow.RecordDelegate recordDelegate2;
					switch (num)
					{
					case 0:
						switch ((363092859 == 363092859) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_26;
						case 1:
							IL_55:
							if (0 != 0)
							{
							}
							goto IL_5E;
						}
						goto IL_55;
					case 1:
						if (recordDelegate == recordDelegate2)
						{
							num = 2;
							continue;
						}
						goto IL_5E;
					case 2:
						return;
					}
					goto IL_26;
					IL_5E:
					int arg_62_0 = 0;
					recordDelegate2 = recordDelegate;
					MainWindow.RecordDelegate value2 = (MainWindow.RecordDelegate)System.Delegate.Remove(recordDelegate2, value);
					recordDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.RecordDelegate>(ref this.eval_f, value2, recordDelegate2);
					num = 1;
				}
				return;
				IL_26:
				recordDelegate = this.eval_f;
				num = 0;
				goto IL_14;
			}
		}
		public string[] Champions
		{
			get
			{
				if (1 != 0)
				{
				}
				switch ((150991047 == 150991047) ? 1 : 0)
				{
				case 0:
				case 2:
				{
					IL_28:
					int arg_2C_0 = 0;
					goto IL_35;
				}
				case 1:
					goto IL_35;
				}
				goto IL_28;
				IL_35:
				if (0 != 0)
				{
				}
				return this.eval_b;
			}
			set
			{
				switch ((119504112 == 119504112) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				int arg_23_0 = 0;
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				this.eval_b = value;
				this.eval_b("Champions");
			}
		}
		public string[] SummonerName
		{
			get
			{
				if (1 != 0)
				{
				}
				switch ((1098857881 == 1098857881) ? 1 : 0)
				{
				case 0:
				case 2:
				{
					IL_28:
					int arg_2C_0 = 0;
					goto IL_35;
				}
				case 1:
					goto IL_35;
				}
				goto IL_28;
				IL_35:
				if (0 != 0)
				{
				}
				return this.eval_c;
			}
			set
			{
				switch ((1837655670 == 1837655670) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				int arg_23_0 = 0;
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				this.eval_c = value;
				this.eval_b("SummonerName");
			}
		}
		public string FileName
		{
			get
			{
				switch ((1433413794 == 1433413794) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				int arg_23_0 = 0;
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				return this.eval_d;
			}
			set
			{
				switch ((465128362 == 465128362) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				int arg_23_0 = 0;
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				this.eval_d = value;
				this.eval_b("FileName");
				this.eval_b("FileNameShort");
			}
		}
		public string FileNameShort
		{
			get
			{
				if (this.FileName == null)
				{
					switch ((387861437 == 387861437) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_3C;
					case 1:
					{
						IL_27:
						int arg_2B_0 = 0;
						if (0 != 0)
						{
						}
						goto IL_3C;
					}
					}
					goto IL_27;
					IL_3C:
					return "錄製中...";
				}
				if (1 != 0)
				{
				}
				string[] array = this.FileName.Split(new char[]
				{
					'\\'
				});
				return array[array.Length - 1];
			}
		}
		private void eval_b(string A_0)
		{
			int num = 0;
			while (true)
			{
				if (1 != 0)
				{
				}
				int arg_47_0 = 0;
				switch (num)
				{
				case 0:
					switch ((1995258410 == 1995258410) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_74;
					case 1:
						IL_28:
						if (0 != 0)
						{
						}
						switch (0)
						{
						case 0:
							goto IL_61;
						}
						continue;
					}
					goto IL_28;
				case 1:
					goto IL_74;
				case 2:
					return;
				}
				IL_61:
				if (this.eval_a != null)
				{
					num = 1;
					continue;
				}
				break;
				IL_74:
				this.eval_a(this, new PropertyChangedEventArgs(A_0));
				num = 2;
			}
		}
		public void DeleteRecord()
		{
			int num = 0;
			while (true)
			{
				int arg_47_0 = 0;
				switch (num)
				{
				case 0:
					if (1 != 0)
					{
					}
					switch ((676187730 == 676187730) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_74;
					case 1:
						IL_32:
						if (0 != 0)
						{
						}
						switch (0)
						{
						case 0:
							goto IL_61;
						}
						continue;
					}
					goto IL_32;
				case 1:
					goto IL_74;
				case 2:
					return;
				}
				IL_61:
				if (this.eval_f != null)
				{
					num = 1;
					continue;
				}
				break;
				IL_74:
				this.eval_f(this);
				num = 2;
			}
		}
		public void PlayRecord()
		{
			int num = 1;
			while (true)
			{
				if (1 != 0)
				{
				}
				int arg_47_0 = 0;
				switch (num)
				{
				case 0:
					goto IL_74;
				case 1:
					switch ((34700657 == 34700657) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_74;
					case 1:
						IL_28:
						if (0 != 0)
						{
						}
						switch (0)
						{
						case 0:
							goto IL_61;
						}
						continue;
					}
					goto IL_28;
				case 2:
					return;
				}
				IL_61:
				if (this.eval_e != null)
				{
					num = 0;
					continue;
				}
				break;
				IL_74:
				this.eval_e(this);
				num = 2;
			}
		}
		public void ShowRenameDialog()
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_40;
				}
				string text;
				OneLineWindow oneLineWindow;
				while (true)
				{
					IL_19:
					switch (num)
					{
					case 0:
						goto IL_152;
					case 1:
						goto IL_13C;
					case 2:
						if (text.Substring(text.Length - 4).CompareTo(".lpr") != 0)
						{
							num = 5;
							continue;
						}
						goto IL_16A;
					case 3:
						num = 2;
						continue;
					case 4:
						return;
					case 5:
						text += ".lpr";
						num = 7;
						continue;
					case 6:
						if (!oneLineWindow.OkayClick)
						{
							switch ((1225643593 == 1225643593) ? 1 : 0)
							{
							case 0:
							case 2:
								goto IL_152;
							case 1:
								IL_AE:
								if (1 != 0)
								{
								}
								if (0 != 0)
								{
								}
								num = 4;
								continue;
							}
							goto IL_AE;
						}
						text = oneLineWindow.DesireString;
						num = 0;
						continue;
					case 7:
						goto IL_E7;
					}
					goto IL_40;
					IL_152:
					if (text.Length > 3)
					{
						num = 3;
					}
					else
					{
						text += ".lpr";
						num = 1;
					}
				}
				return;
				IL_E7:
				IL_13C:
				IL_16A:
				string arg = this.FileName.Remove(this.FileName.LastIndexOf('\\'));
				string a_ = arg + '\\' + text;
				this.eval_a(a_);
				return;
				IL_40:
				oneLineWindow = new OneLineWindow();
				oneLineWindow.eval_a.Text = this.FileNameShort;
				oneLineWindow.Owner = Application.Current.MainWindow;
				oneLineWindow.ShowDialog();
				int arg_72_0 = 0;
				num = 6;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		private void eval_a(string A_0)
		{
			try
			{
				if (1 != 0)
				{
				}
				switch ((2062871373 == 2062871373) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_28:
					break;
				case 1:
					goto IL_29;
				}
				goto IL_28;
				IL_29:
				if (0 != 0)
				{
				}
				System.IO.File.Move(this.FileName, A_0);
				this.FileName = A_0;
			}
			catch (System.Exception)
			{
			}
			int arg_4C_0 = 0;
		}
	}
}
