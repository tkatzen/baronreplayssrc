using BaronReplays.JsonData;
using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel;
using System.Threading;
namespace BaronReplays
{
	public class FeaturedGameJson : INotifyPropertyChanged
	{
		private PropertyChangedEventHandler eval_a;
		public ulong gameId;
		public int mapId;
		public string gameMode;
		public string gameType;
		public int gameQueueConfigId;
		public FeaturedGameParticipant[] participants;
		private FeaturedGameParticipant[] eval_b;
		private FeaturedGameParticipant[] eval_c;
		public JContainer observers;
		public string platformId;
		public int gameTypeConfigId;
		public object bannedChampions;
		public ulong gameStartTime;
		public uint gameLength;
		public event PropertyChangedEventHandler PropertyChanged
		{
			add
			{
				switch (0)
				{
				case 0:
					goto IL_42;
				}
				int num;
				PropertyChangedEventHandler propertyChangedEventHandler;
				while (true)
				{
					IL_0A:
					PropertyChangedEventHandler propertyChangedEventHandler2;
					switch ((2012460035 == 2012460035) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_5E;
					case 1:
						IL_29:
						if (0 != 0)
						{
						}
						switch (num)
						{
						case 0:
							goto IL_94;
						case 1:
							goto IL_5E;
						case 2:
							if (propertyChangedEventHandler == propertyChangedEventHandler2)
							{
								num = 0;
								continue;
							}
							goto IL_5E;
						}
						goto IL_42;
					}
					goto IL_29;
					IL_5E:
					propertyChangedEventHandler2 = propertyChangedEventHandler;
					PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)System.Delegate.Combine(propertyChangedEventHandler2, value);
					propertyChangedEventHandler = System.Threading.Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.eval_a, value2, propertyChangedEventHandler2);
					num = 2;
				}
				IL_94:
				int arg_9A_0 = 0;
				return;
				IL_42:
				propertyChangedEventHandler = this.eval_a;
				if (1 != 0)
				{
				}
				num = 1;
				goto IL_0A;
			}
			remove
			{
				switch (0)
				{
				case 0:
					goto IL_4C;
				}
				int num;
				PropertyChangedEventHandler propertyChangedEventHandler;
				while (true)
				{
					IL_0A:
					PropertyChangedEventHandler propertyChangedEventHandler2;
					switch ((1856456265 == 1856456265) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_5E;
					case 1:
						IL_29:
						if (1 != 0)
						{
						}
						if (0 != 0)
						{
						}
						switch (num)
						{
						case 0:
							if (propertyChangedEventHandler == propertyChangedEventHandler2)
							{
								num = 2;
								continue;
							}
							goto IL_5E;
						case 1:
							goto IL_5E;
						case 2:
							goto IL_94;
						}
						goto IL_4C;
					}
					goto IL_29;
					IL_5E:
					propertyChangedEventHandler2 = propertyChangedEventHandler;
					PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)System.Delegate.Remove(propertyChangedEventHandler2, value);
					propertyChangedEventHandler = System.Threading.Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.eval_a, value2, propertyChangedEventHandler2);
					num = 0;
				}
				IL_94:
				int arg_9A_0 = 0;
				return;
				IL_4C:
				propertyChangedEventHandler = this.eval_a;
				num = 1;
				goto IL_0A;
			}
		}
		public int MapId
		{
			get
			{
				if (1 != 0)
				{
				}
				switch ((569445353 == 569445353) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_28:
					break;
				case 1:
					goto IL_29;
				}
				goto IL_28;
				IL_29:
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return this.mapId;
			}
		}
		public string GameMode
		{
			get
			{
				switch ((1676918376 == 1676918376) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return this.gameMode;
			}
		}
		public string GameType
		{
			get
			{
				if (1 != 0)
				{
				}
				switch ((582956107 == 582956107) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_28:
					break;
				case 1:
					goto IL_29;
				}
				goto IL_28;
				IL_29:
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return this.gameType;
			}
		}
		public FeaturedGameParticipant[] PurpleTeamParticipants
		{
			get
			{
				int num = 1;
				while (true)
				{
					switch (num)
					{
					case 0:
						goto IL_83;
					case 1:
					{
						int arg_0D_0 = 0;
						switch (0)
						{
						case 0:
							goto IL_31;
						}
						continue;
					}
					case 2:
						this.eval_a();
						goto IL_7A;
					}
					IL_31:
					if (this.eval_b == null)
					{
						switch ((1177917751 == 1177917751) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_7A;
						case 1:
							IL_58:
							if (1 != 0)
							{
							}
							if (0 != 0)
							{
							}
							num = 2;
							continue;
						}
						goto IL_58;
					}
					break;
					IL_7A:
					num = 0;
				}
				IL_83:
				return this.eval_b;
			}
		}
		public FeaturedGameParticipant[] BlueTeamParticipants
		{
			get
			{
				int num = 0;
				while (true)
				{
					switch (num)
					{
					case 0:
					{
						int arg_0D_0 = 0;
						switch (0)
						{
						case 0:
							goto IL_31;
						}
						continue;
					}
					case 1:
						goto IL_83;
					case 2:
						this.eval_a();
						goto IL_7A;
					}
					IL_31:
					if (1 != 0)
					{
					}
					if (this.eval_c == null)
					{
						switch ((611701622 == 611701622) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_7A;
						case 1:
							IL_62:
							if (0 != 0)
							{
							}
							num = 2;
							continue;
						}
						goto IL_62;
					}
					break;
					IL_7A:
					num = 1;
				}
				IL_83:
				return this.eval_c;
			}
		}
		public string PlatformId
		{
			get
			{
				switch ((1757107204 == 1757107204) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				return this.platformId;
			}
		}
		public uint GameLength
		{
			get
			{
				switch ((356244041 == 356244041) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				System.DateTime d = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
				d = d.AddMilliseconds(this.gameStartTime);
				return (uint)(System.DateTime.UtcNow - d).TotalSeconds - 180u;
			}
			set
			{
				switch ((424588389 == 424588389) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this.gameLength = value;
				this.eval_a("GameLength");
			}
		}
		private void eval_a(string A_0)
		{
			int num = 0;
			while (true)
			{
				switch (num)
				{
				case 0:
				{
					int arg_0D_0 = 0;
					switch (0)
					{
					case 0:
						goto IL_31;
					}
					continue;
				}
				case 1:
					return;
				case 2:
					this.eval_a(this, new PropertyChangedEventArgs(A_0));
					goto IL_86;
				}
				IL_31:
				if (this.eval_a != null)
				{
					switch ((1895031171 == 1895031171) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_86;
					case 1:
						IL_58:
						if (1 != 0)
						{
						}
						if (0 != 0)
						{
						}
						num = 2;
						continue;
					}
					goto IL_58;
				}
				break;
				IL_86:
				num = 1;
			}
		}
		private void eval_a()
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_40;
				}
				int num2;
				int num3;
				int num4;
				FeaturedGameParticipant[] array;
				while (true)
				{
					IL_19:
					switch (num)
					{
					case 0:
					{
						FeaturedGameParticipant featuredGameParticipant;
						this.eval_c[num2++] = featuredGameParticipant;
						num = 4;
						continue;
					}
					case 1:
						switch ((1176376879 == 1176376879) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_F1;
						case 1:
							IL_8F:
							if (0 != 0)
							{
							}
							goto IL_100;
						}
						goto IL_8F;
					case 2:
						goto IL_98;
					case 3:
						goto IL_121;
					case 4:
						goto IL_98;
					case 5:
					{
						FeaturedGameParticipant featuredGameParticipant;
						if (featuredGameParticipant.TeamId == 100u)
						{
							goto IL_F1;
						}
						this.eval_b[num3++] = featuredGameParticipant;
						num = 2;
						continue;
					}
					case 6:
					{
						if (num4 >= array.Length)
						{
							num = 3;
							continue;
						}
						FeaturedGameParticipant featuredGameParticipant = array[num4];
						num = 5;
						continue;
					}
					case 7:
						goto IL_100;
					}
					goto IL_40;
					IL_98:
					num4++;
					int arg_A2_0 = 0;
					num = 7;
					continue;
					IL_F1:
					num = 0;
					continue;
					IL_100:
					num = 6;
				}
				IL_121:
				if (1 != 0)
				{
				}
				return;
				IL_40:
				this.eval_c = new FeaturedGameParticipant[5];
				this.eval_b = new FeaturedGameParticipant[5];
				num2 = 0;
				num3 = 0;
				array = this.participants;
				num4 = 0;
				num = 1;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
	}
}
