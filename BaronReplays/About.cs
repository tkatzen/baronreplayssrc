using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;
namespace BaronReplays
{
	public class About : Window, IComponentConnector
	{
		internal TriStateButton eval_a;
		private bool eval_b;
		public string VersionString
		{
			get
			{
				switch ((45318981 == 45318981) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				int arg_2D_0 = 0;
				if (0 != 0)
				{
				}
				return string.Format(Utilities.GetString("AboutVersion"), Utilities.Version / 100 + "." + (Utilities.Version % 100).ToString().PadLeft(2, '0'));
			}
		}
		public About()
		{
			this.InitializeComponent();
			base.DataContext = this;
		}
		private void eval_a(object A_0, RoutedEventArgs A_1)
		{
			switch ((65020210 == 65020210) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			int arg_2D_0 = 0;
			if (0 != 0)
			{
			}
			base.Close();
		}
		private void eval_a(object A_0, KeyEventArgs A_1)
		{
			while (true)
			{
				IL_00:
				int num = 0;
				while (true)
				{
					switch (num)
					{
					case 0:
						switch (0)
						{
						case 0:
							goto IL_25;
						}
						continue;
					case 1:
						base.Close();
						switch ((1274579713 == 1274579713) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_00;
						case 1:
							IL_5E:
							if (0 != 0)
							{
							}
							if (1 != 0)
							{
							}
							num = 2;
							continue;
						}
						goto IL_5E;
					case 2:
						goto IL_78;
					}
					IL_25:
					if (A_1.Key != Key.Return)
					{
						goto IL_7A;
					}
					num = 1;
				}
			}
			IL_78:
			IL_7A:
			int arg_7E_0 = 0;
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), System.Diagnostics.DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (this.eval_b)
			{
				switch ((1347784724 == 1347784724) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_3B;
				case 1:
					IL_29:
					if (0 != 0)
					{
					}
					if (1 != 0)
					{
					}
					return;
				}
				goto IL_29;
			}
			IL_3B:
			int arg_3F_0 = 0;
			this.eval_b = true;
			Uri resourceLocator = new Uri("/BaronReplays;component/about.xaml", UriKind.Relative);
			Application.LoadComponent(this, resourceLocator);
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), System.Diagnostics.DebuggerNonUserCode]
		internal System.Delegate eval_a(System.Type A_0, string A_1)
		{
			switch ((72237340 == 72237340) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (0 != 0)
			{
			}
			int arg_2A_0 = 0;
			if (1 != 0)
			{
			}
			return System.Delegate.CreateDelegate(A_0, this, A_1);
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), System.Diagnostics.DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch ((453225180 == 453225180) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_5A:
				int num = 0;
				goto IL_3A;
			}
			case 1:
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				switch (0)
				{
				case 0:
					goto IL_58;
				}
				goto IL_3A;
			}
			goto IL_1F;
			while (true)
			{
				IL_3A:
				int arg_3E_0 = 0;
				int num;
				switch (num)
				{
				case 0:
					switch (connectionId)
					{
					case 1:
						goto IL_8B;
					case 2:
						goto IL_7E;
					default:
						num = 1;
						continue;
					}
					break;
				case 1:
					num = 2;
					continue;
				case 2:
					goto IL_AC;
				}
				goto IL_58;
			}
			IL_7E:
			this.eval_a = (TriStateButton)target;
			return;
			IL_8B:
			((About)target).KeyDown += new KeyEventHandler(this.eval_a);
			return;
			IL_AC:
			this.eval_b = true;
			return;
			IL_58:
			goto IL_5A;
		}
	}
}
