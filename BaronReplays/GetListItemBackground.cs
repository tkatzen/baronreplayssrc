using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
namespace BaronReplays
{
	public class GetListItemBackground : IMultiValueConverter
	{
		public static SolidColorBrush[] Backgrounds;
		public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			int arg_04_0 = 0;
			switch ((1976230078 == 1976230078) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			ListBoxItem container = values[0] as ListBoxItem;
			ListBox listBox = values[1] as ListBox;
			int num = listBox.ItemContainerGenerator.IndexFromContainer(container);
			return GetListItemBackground.Backgrounds[num % GetListItemBackground.Backgrounds.Length];
		}
		public object[] ConvertBack(object value, System.Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
		{
			int arg_04_0 = 0;
			switch ((1153988856 == 1153988856) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			return null;
		}
		static GetListItemBackground()
		{
			// Note: this type is marked as 'beforefieldinit'.
			int arg_04_0 = 0;
			switch ((833098151 == 833098151) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			GetListItemBackground.Backgrounds = new SolidColorBrush[]
			{
				new SolidColorBrush(Color.FromRgb(255, 255, 255)),
				new SolidColorBrush(Color.FromRgb(255, 255, 255))
			};
		}
	}
}
