using BaronReplays.Properties;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Shapes;
using System.Windows.Threading;
namespace BaronReplays
{
	public class FileNameFormat : UserControl, IComponentConnector
	{
		private static System.Collections.Generic.List<string> TagTypes;
		private static System.Collections.Generic.List<string> SymbolTypes;
		private System.Collections.Generic.Dictionary<TriStateButton, string> _tagToType;
		private System.Collections.Generic.Dictionary<string, string> _namesToExample;
		private System.DateTime _mouseDownTime;
		private DispatcherTimer _clickDragTimer;
		private TriStateButton _draggingItem;
		private static char[] _unAcceptChars;
		internal WrapPanel SourcePanel;
		internal StackPanel NamePanel;
		internal TextBlock FileNameExample;
		private bool _contentLoaded;
		public FileNameFormat()
		{
			this.InitializeComponent();
			this._clickDragTimer = new DispatcherTimer();
			this._clickDragTimer.Interval = System.TimeSpan.FromMilliseconds(200.0);
			this._clickDragTimer.Tick += new System.EventHandler(this.eval_a);
			this.eval_e();
			this.eval_d();
		}
		private void eval_e()
		{
			switch (0)
			{
			case 0:
			{
				IL_0F:
				this._tagToType = new System.Collections.Generic.Dictionary<TriStateButton, string>();
				using (System.Collections.Generic.List<string>.Enumerator enumerator = FileNameFormat.TagTypes.GetEnumerator())
				{
					int num = 1;
					while (true)
					{
						switch (num)
						{
						case 0:
							num = 2;
							continue;
						case 1:
							switch (0)
							{
							case 0:
								goto IL_FD;
							}
							continue;
						case 2:
							goto IL_16E;
						case 3:
						{
							if (!enumerator.MoveNext())
							{
								num = 0;
								continue;
							}
							string current = enumerator.Current;
							this.eval_a(current, false);
							num = 4;
							continue;
						}
						}
						IL_FF:
						switch ((727881186 == 727881186) ? 1 : 0)
						{
						case 0:
						case 2:
							continue;
						case 1:
							IL_11E:
							if (0 != 0)
							{
							}
							num = 3;
							continue;
						}
						goto IL_11E;
						IL_FD:
						goto IL_FF;
					}
					IL_16E:
					goto IL_BE;
				}
				goto IL_181;
				while (true)
				{
					IL_BE:
					System.Collections.Generic.List<string>.Enumerator enumerator2 = FileNameFormat.SymbolTypes.GetEnumerator();
					try
					{
						int num = 4;
						while (true)
						{
							switch (num)
							{
							case 1:
							{
								if (!enumerator2.MoveNext())
								{
									num = 3;
									continue;
								}
								string current2 = enumerator2.Current;
								this.eval_a(current2, true);
								num = 0;
								continue;
							}
							case 2:
								goto IL_A1;
							case 3:
								num = 2;
								continue;
							case 4:
								switch (0)
								{
								case 0:
									goto IL_59;
								}
								continue;
							}
							IL_5B:
							num = 1;
							continue;
							IL_59:
							goto IL_5B;
						}
						IL_A1:
						break;
					}
					finally
					{
						if (1 != 0)
						{
						}
						((System.IDisposable)enumerator2).Dispose();
					}
				}
				IL_181:
				int arg_185_0 = 0;
				return;
			}
			}
			goto IL_0F;
		}
		private TriStateButton eval_a(string A_0, bool A_1)
		{
			switch (0)
			{
			case 0:
				goto IL_20;
			}
			int num;
			TriStateButton triStateButton;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					if (!A_1)
					{
						num = 1;
						continue;
					}
					triStateButton.ContentString = A_0;
					triStateButton.PreviewMouseRightButtonUp += new MouseButtonEventHandler(this.eval_b);
					num = 3;
					continue;
				case 1:
					triStateButton.ContentString = Utilities.GetString(A_0);
					triStateButton.PreviewMouseRightButtonUp += new MouseButtonEventHandler(this.eval_a);
					this._tagToType.Add(triStateButton, A_0);
					num = 2;
					continue;
				case 2:
					goto IL_D7;
				case 3:
					goto IL_9E;
				}
				goto IL_20;
			}
			IL_9E:
			IL_D7:
			triStateButton.Margin = new Thickness(5.0);
			triStateButton.VerticalAlignment = VerticalAlignment.Center;
			triStateButton.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(this.eval_e);
			triStateButton.PreviewMouseLeftButtonUp += new MouseButtonEventHandler(this.eval_d);
			this.SourcePanel.Children.Add(triStateButton);
			return triStateButton;
			while (true)
			{
				IL_20:
				triStateButton = new TriStateButton();
				switch ((877534076 == 877534076) ? 1 : 0)
				{
				case 0:
				case 2:
					continue;
				}
				break;
			}
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			int arg_5A_0 = 0;
			num = 0;
			goto IL_0A;
		}
		private void eval_e(object A_0, MouseButtonEventArgs A_1)
		{
			int arg_04_0 = 0;
			switch ((1547373124 == 1547373124) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this._mouseDownTime = System.DateTime.Now;
			this._draggingItem = (A_0 as TriStateButton);
			this._clickDragTimer.Start();
		}
		private void eval_d(object A_0, MouseButtonEventArgs A_1)
		{
			switch (0)
			{
			case 0:
				goto IL_1C;
			}
			int num;
			System.TimeSpan timeSpan;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					if (1 != 0)
					{
					}
					this.eval_b(A_0 as TriStateButton);
					this._clickDragTimer.Stop();
					num = 2;
					continue;
				case 1:
					switch ((120571360 == 120571360) ? 1 : 0)
					{
					case 0:
					case 2:
						return;
					case 1:
						IL_61:
						if (0 != 0)
						{
						}
						if (timeSpan.TotalMilliseconds < 200.0)
						{
							num = 0;
							continue;
						}
						return;
					}
					goto IL_61;
				case 2:
					return;
				}
				goto IL_1C;
			}
			return;
			IL_1C:
			timeSpan = System.DateTime.Now - this._mouseDownTime;
			int arg_31_0 = 0;
			num = 1;
			goto IL_0A;
		}
		private void eval_a(object A_0, System.EventArgs A_1)
		{
			int arg_04_0 = 0;
			switch ((576461883 == 576461883) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this._clickDragTimer.Stop();
			this.eval_c(this._draggingItem, null);
		}
		private void eval_b(TriStateButton A_0)
		{
			int num = 1;
			while (true)
			{
				switch (num)
				{
				case 0:
					if (!FileNameFormat.SymbolTypes.Contains(A_0.ContentString))
					{
						num = 7;
						continue;
					}
					goto IL_161;
				case 1:
					switch (0)
					{
					case 0:
						goto IL_39;
					}
					continue;
				case 2:
					this.NamePanel.Children.Remove(A_0);
					num = 0;
					continue;
				case 3:
					if (FileNameFormat.SymbolTypes.Contains(A_0.ContentString))
					{
						num = 4;
						continue;
					}
					goto IL_161;
				case 4:
				{
					int arg_B5_0 = 0;
					switch ((460888971 == 460888971) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_11D;
					case 1:
						IL_13E:
						if (0 != 0)
						{
						}
						this.eval_a(A_0.ContentString, true);
						num = 5;
						continue;
					}
					goto IL_13E;
				}
				case 5:
					goto IL_15F;
				case 6:
					goto IL_DD;
				case 7:
					goto IL_11D;
				}
				IL_39:
				if (this.NamePanel.Children.Contains(A_0))
				{
					if (1 != 0)
					{
					}
					num = 2;
					continue;
				}
				this.SourcePanel.Children.Remove(A_0);
				this.NamePanel.Children.Add(A_0);
				num = 3;
				continue;
				IL_11D:
				this.SourcePanel.Children.Add(A_0);
				num = 6;
			}
			IL_DD:
			IL_15F:
			IL_161:
			this.eval_c();
		}
		private void eval_c(object A_0, MouseButtonEventArgs A_1)
		{
			int arg_04_0 = 0;
			switch ((1187692287 == 1187692287) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			DataObject dataObject = new DataObject();
			dataObject.SetData("Object", A_0);
			DragDrop.DoDragDrop(this, dataObject, DragDropEffects.All);
		}
		private void eval_d()
		{
			int arg_04_0 = 0;
			switch ((2100703303 == 2100703303) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			System.DateTime now = System.DateTime.Now;
			this._namesToExample = new System.Collections.Generic.Dictionary<string, string>();
			this._namesToExample.Add("SummonerName", Utilities.GetString("SummonerName"));
			this._namesToExample.Add("Champion", "Ahri");
			this._namesToExample.Add("Year", now.Year.ToString());
			this._namesToExample.Add("Month", now.Month.ToString());
			this._namesToExample.Add("Day", now.Day.ToString());
			this._namesToExample.Add("Hour", now.Hour.ToString());
			this._namesToExample.Add("Minute", now.Minute.ToString());
			this._namesToExample.Add("Second", now.Second.ToString());
			this._namesToExample.Add("Kill", "8");
			this._namesToExample.Add("Death", "3");
			this._namesToExample.Add("Assist", "2");
			this._namesToExample.Add("KDA", "3.3");
			this._namesToExample.Add("WinLose", Utilities.GetString("Win"));
			this._namesToExample.Add("Map", Utilities.GetString("CLASSIC"));
		}
		private void eval_c()
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_30;
				}
				System.Text.StringBuilder stringBuilder;
				while (true)
				{
					IL_19:
					switch (num)
					{
					case 0:
						return;
					case 1:
						goto IL_C1;
					case 2:
						try
						{
							num = 0;
							while (true)
							{
								switch (num)
								{
								case 0:
									switch (0)
									{
									case 0:
										goto IL_101;
									}
									continue;
								case 1:
								{
									TriStateButton triStateButton;
									if (this._tagToType.ContainsKey(triStateButton))
									{
										num = 7;
										continue;
									}
									stringBuilder.Append(triStateButton.ContentString);
									num = 5;
									continue;
								}
								case 3:
								{
									System.Collections.IEnumerator enumerator;
									if (!enumerator.MoveNext())
									{
										num = 4;
										continue;
									}
									object current = enumerator.Current;
									TriStateButton triStateButton = current as TriStateButton;
									num = 1;
									continue;
								}
								case 4:
									num = 6;
									continue;
								case 6:
									goto IL_1AD;
								case 7:
								{
									TriStateButton triStateButton;
									string key = this._tagToType[triStateButton];
									stringBuilder.Append(this._namesToExample[key]);
									num = 2;
									continue;
								}
								}
								IL_136:
								num = 3;
								continue;
								IL_101:
								goto IL_136;
							}
							IL_1AD:
							goto IL_86;
						}
						finally
						{
							switch (0)
							{
							case 0:
								goto IL_1CF;
							}
							System.IDisposable disposable;
							while (true)
							{
								IL_1BC:
								switch (num)
								{
								case 0:
									if (disposable != null)
									{
										num = 2;
										continue;
									}
									goto IL_205;
								case 1:
									goto IL_203;
								case 2:
									disposable.Dispose();
									num = 1;
									continue;
								}
								goto IL_1CF;
							}
							IL_203:
							IL_205:
							goto EndFinally_7;
							IL_1CF:
							System.Collections.IEnumerator enumerator;
							disposable = (enumerator as System.IDisposable);
							num = 0;
							goto IL_1BC;
							EndFinally_7:;
						}
						goto IL_206;
						IL_86:
						stringBuilder.Append(".lpr");
						num = 3;
						continue;
					case 3:
						if (this.FileNameExample != null)
						{
							if (1 != 0)
							{
							}
							num = 1;
							continue;
						}
						return;
					}
					goto IL_30;
				}
				IL_C1:
				goto IL_206;
				IL_30:
				int arg_34_0 = 0;
				switch ((118452558 == 118452558) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_206:
					this.FileNameExample.Text = stringBuilder.ToString();
					num = 0;
					goto IL_19;
				case 1:
				{
					IL_5B:
					if (0 != 0)
					{
					}
					stringBuilder = new System.Text.StringBuilder();
					System.Collections.IEnumerator enumerator = this.NamePanel.Children.GetEnumerator();
					num = 2;
					goto IL_19;
				}
				}
				goto IL_5B;
			}
			}
			goto IL_0F;
		}
		private void eval_b()
		{
			switch ((1620848684 == 1620848684) ? 1 : 0)
			{
			case 0:
			case 2:
				goto IL_196;
			case 1:
				break;
			default:
			{
				int arg_22_0 = 0;
				break;
			}
			}
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int num = 0;
			System.Text.StringBuilder stringBuilder;
			switch (num)
			{
			case 0:
			{
				IL_4B:
				stringBuilder = new System.Text.StringBuilder();
				System.Collections.IEnumerator enumerator = this.NamePanel.Children.GetEnumerator();
				try
				{
					num = 4;
					while (true)
					{
						switch (num)
						{
						case 0:
						{
							if (!enumerator.MoveNext())
							{
								num = 3;
								continue;
							}
							object current = enumerator.Current;
							TriStateButton triStateButton = current as TriStateButton;
							num = 5;
							continue;
						}
						case 1:
							goto IL_141;
						case 2:
						{
							TriStateButton triStateButton;
							stringBuilder.AppendFormat("<{0}>", this._tagToType[triStateButton]);
							num = 7;
							continue;
						}
						case 3:
							num = 1;
							continue;
						case 4:
							switch (0)
							{
							case 0:
								goto IL_9F;
							}
							continue;
						case 5:
						{
							TriStateButton triStateButton;
							if (this._tagToType.ContainsKey(triStateButton))
							{
								num = 2;
								continue;
							}
							stringBuilder.Append(triStateButton.ContentString);
							num = 6;
							continue;
						}
						}
						IL_D3:
						num = 0;
						continue;
						IL_9F:
						goto IL_D3;
					}
					IL_141:;
				}
				finally
				{
					switch (0)
					{
					case 0:
						goto IL_160;
					}
					System.IDisposable disposable;
					while (true)
					{
						IL_14D:
						switch (num)
						{
						case 0:
							goto IL_193;
						case 1:
							disposable.Dispose();
							num = 0;
							continue;
						case 2:
							if (disposable != null)
							{
								num = 1;
								continue;
							}
							goto IL_195;
						}
						goto IL_160;
					}
					IL_193:
					IL_195:
					goto EndFinally_7;
					IL_160:
					disposable = (enumerator as System.IDisposable);
					num = 2;
					goto IL_14D;
					EndFinally_7:;
				}
				goto IL_196;
			}
			}
			goto IL_4B;
			IL_196:
			stringBuilder.Append(".lpr");
			BaronReplays.Properties.Settings.Default.FileNameFormat = stringBuilder.ToString();
		}
		private void eval_a(object A_0, DragEventArgs A_1)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_64;
				}
				TriStateButton triStateButton;
				int num2;
				double num3;
				double x;
				while (true)
				{
					IL_19:
					switch (num)
					{
					case 0:
						goto IL_280;
					case 1:
						goto IL_2D4;
					case 2:
						if (this.NamePanel.Children.Count > 0)
						{
							num = 7;
							continue;
						}
						goto IL_2D4;
					case 3:
						this.SourcePanel.Children.Remove(triStateButton);
						this.NamePanel.Children.Insert(num2, triStateButton);
						num = 10;
						continue;
					case 4:
						if (num2 > this.NamePanel.Children.IndexOf(triStateButton))
						{
							num = 11;
							continue;
						}
						goto IL_250;
					case 5:
						goto IL_285;
					case 6:
						if (this.SourcePanel.Children.Contains(triStateButton))
						{
							num = 3;
							continue;
						}
						num = 4;
						continue;
					case 7:
						switch ((1776846507 == 1776846507) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_1D6;
						case 1:
							IL_21A:
							if (0 != 0)
							{
							}
							num = 5;
							continue;
						}
						goto IL_21A;
					case 8:
						goto IL_250;
					case 9:
						goto IL_285;
					case 10:
						if (FileNameFormat.SymbolTypes.Contains(triStateButton.ContentString))
						{
							int arg_150_0 = 0;
							num = 16;
							continue;
						}
						goto IL_309;
					case 11:
						num2--;
						num = 8;
						continue;
					case 12:
						if (num3 - (this.NamePanel.Children[num2] as TriStateButton).ActualWidth / 2.0 - 5.0 <= x)
						{
							goto IL_1D6;
						}
						goto IL_2D4;
					case 13:
						num2++;
						num = 9;
						continue;
					case 14:
						goto IL_24B;
					case 15:
						if (num2 >= this.NamePanel.Children.Count)
						{
							if (1 != 0)
							{
							}
							num = 1;
							continue;
						}
						num3 += (this.NamePanel.Children[num2] as TriStateButton).ActualWidth + 10.0;
						num = 12;
						continue;
					case 16:
						this.eval_a(triStateButton.ContentString, true);
						num = 14;
						continue;
					}
					goto IL_64;
					IL_1D6:
					num = 13;
					continue;
					IL_250:
					this.NamePanel.Children.Remove(triStateButton);
					this.NamePanel.Children.Insert(num2, triStateButton);
					num = 0;
					continue;
					IL_285:
					num = 15;
					continue;
					IL_2D4:
					num = 6;
				}
				IL_24B:
				IL_280:
				IL_309:
				this.eval_c();
				return;
				IL_64:
				triStateButton = (A_1.Data.GetData("Object") as TriStateButton);
				x = A_1.GetPosition(this.NamePanel).X;
				num3 = 0.0;
				num2 = 0;
				num = 2;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		private void eval_a(TriStateButton A_0)
		{
			int num = 2;
			while (true)
			{
				switch (num)
				{
				case 0:
					switch ((1521891307 == 1521891307) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_25;
					}
					goto IL_AA;
				case 1:
				{
					int arg_4F_0 = 0;
					this.NamePanel.Children.Remove(A_0);
					this.SourcePanel.Children.Add(A_0);
					this.eval_c();
					num = 0;
					continue;
				}
				case 2:
					switch (0)
					{
					case 0:
						goto IL_25;
					}
					continue;
				}
				IL_25:
				if (1 != 0)
				{
				}
				if (!this.NamePanel.Children.Contains(A_0))
				{
					return;
				}
				num = 1;
			}
			IL_AA:
			if (0 != 0)
			{
			}
		}
		private void eval_b(object A_0, MouseButtonEventArgs A_1)
		{
			if (1 != 0)
			{
			}
			switch (0)
			{
			case 0:
				goto IL_26;
			}
			int num;
			TriStateButton element;
			while (true)
			{
				IL_14:
				switch (num)
				{
				case 0:
					return;
				case 1:
					switch ((1177915820 == 1177915820) ? 1 : 0)
					{
					case 0:
					case 2:
						return;
					case 1:
						IL_61:
						if (0 != 0)
						{
						}
						if (this.NamePanel.Children.Contains(element))
						{
							num = 2;
							continue;
						}
						return;
					}
					goto IL_61;
				case 2:
					this.NamePanel.Children.Remove(element);
					this.eval_c();
					num = 0;
					continue;
				}
				goto IL_26;
			}
			return;
			IL_26:
			int arg_2A_0 = 0;
			element = (A_0 as TriStateButton);
			num = 1;
			goto IL_14;
		}
		private void eval_a(object A_0, MouseButtonEventArgs A_1)
		{
			int arg_04_0 = 0;
			switch ((127362360 == 127362360) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.eval_a(A_0 as TriStateButton);
		}
		private void eval_a(object A_0, TextChangedEventArgs A_1)
		{
			int arg_04_0 = 0;
			switch ((1082861513 == 1082861513) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.eval_c();
		}
		private void eval_a()
		{
			int arg_04_0 = 0;
			switch ((922342213 == 922342213) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			Settings element = new Settings();
			Grid grid = (Grid)base.Parent;
			grid.Children.Clear();
			grid.Children.Add(element);
		}
		private void eval_b(object A_0, RoutedEventArgs A_1)
		{
			int arg_04_0 = 0;
			switch ((1013368888 == 1013368888) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			this.eval_b();
			this.eval_a();
		}
		private void eval_a(object A_0, RoutedEventArgs A_1)
		{
			int arg_04_0 = 0;
			if (1 != 0)
			{
			}
			switch ((83958454 == 83958454) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_34:
				break;
			case 1:
				goto IL_35;
			}
			goto IL_34;
			IL_35:
			if (0 != 0)
			{
			}
			this.eval_a();
		}
		private void eval_a(object A_0, TextCompositionEventArgs A_1)
		{
			switch (0)
			{
			case 0:
				goto IL_2C;
			}
			int num;
			int num2;
			char[] unAcceptChars;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					return;
				case 1:
					goto IL_89;
				case 2:
				{
					char value;
					if (A_1.Text.Contains(value))
					{
						num = 5;
						continue;
					}
					num2++;
					num = 1;
					continue;
				}
				case 3:
				{
					if (num2 >= unAcceptChars.Length)
					{
						num = 0;
						continue;
					}
					char value = unAcceptChars[num2];
					num = 2;
					continue;
				}
				case 4:
					goto IL_89;
				case 5:
					A_1.Handled = true;
					goto IL_55;
				case 6:
					return;
				}
				goto IL_2C;
				IL_55:
				num = 6;
				continue;
				IL_89:
				switch ((95780853 == 95780853) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_55;
				case 1:
				{
					IL_A8:
					int arg_AC_0 = 0;
					if (1 != 0)
					{
					}
					if (0 != 0)
					{
					}
					num = 3;
					continue;
				}
				}
				goto IL_A8;
			}
			return;
			IL_2C:
			unAcceptChars = FileNameFormat._unAcceptChars;
			num2 = 0;
			num = 4;
			goto IL_0A;
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), System.Diagnostics.DebuggerNonUserCode]
		public void InitializeComponent()
		{
			switch ((1242194601 == 1242194601) ? 1 : 0)
			{
			case 0:
			case 2:
				goto IL_3A;
			case 1:
			{
				IL_1F:
				int arg_23_0 = 0;
				if (0 != 0)
				{
				}
				if (this._contentLoaded)
				{
					goto IL_3A;
				}
				this._contentLoaded = true;
				Uri resourceLocator = new Uri("/BaronReplays;component/filenameformat.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
				return;
			}
			}
			goto IL_1F;
			IL_3A:
			if (1 != 0)
			{
			}
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), System.Diagnostics.DebuggerNonUserCode]
		internal System.Delegate eval_a(System.Type A_0, string A_1)
		{
			int arg_04_0 = 0;
			switch ((1043060555 == 1043060555) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			return System.Delegate.CreateDelegate(A_0, this, A_1);
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), System.Diagnostics.DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (0)
			{
			case 0:
				goto IL_1C;
			}
			int num;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					goto IL_AD;
				case 1:
					switch ((1642868191 == 1642868191) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_4B;
					case 1:
						IL_73:
						if (0 != 0)
						{
						}
						num = 0;
						continue;
					}
					goto IL_73;
				case 2:
				{
					int arg_2B_0 = 0;
					switch (connectionId)
					{
					case 1:
						goto IL_C6;
					case 2:
						goto IL_89;
					case 3:
						goto IL_7C;
					case 4:
						goto IL_AF;
					}
					goto IL_4B;
				}
				}
				goto IL_1C;
				IL_4B:
				num = 1;
			}
			IL_7C:
			this.NamePanel = (StackPanel)target;
			return;
			IL_89:
			((Rectangle)target).Drop += new DragEventHandler(this.eval_a);
			return;
			IL_AD:
			this._contentLoaded = true;
			return;
			IL_AF:
			if (1 != 0)
			{
			}
			this.FileNameExample = (TextBlock)target;
			return;
			IL_C6:
			this.SourcePanel = (WrapPanel)target;
			return;
			IL_1C:
			num = 2;
			goto IL_0A;
		}
		static FileNameFormat()
		{
			// Note: this type is marked as 'beforefieldinit'.
			int arg_04_0 = 0;
			switch ((1755198819 == 1755198819) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			FileNameFormat.TagTypes = new System.Collections.Generic.List<string>
			{
				"SummonerName",
				"Champion",
				"Year",
				"Month",
				"Day",
				"Hour",
				"Minute",
				"Second",
				"Kill",
				"Death",
				"Assist",
				"KDA",
				"WinLose",
				"Map"
			};
			FileNameFormat.SymbolTypes = new System.Collections.Generic.List<string>
			{
				" ",
				"-",
				"~"
			};
			FileNameFormat._unAcceptChars = new char[]
			{
				'\\',
				'/',
				':',
				'*',
				'?',
				'"',
				'<',
				'>',
				'|'
			};
		}
	}
}
