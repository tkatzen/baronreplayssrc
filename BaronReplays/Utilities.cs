using BaronReplays.Properties;
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;
namespace BaronReplays
{
	public static class Utilities
	{
		public static System.IO.StreamWriter LogWriter;
		private static MainWindow.BackGroundInitDoneDelegate eval_a;
		public static bool HasNewVersion;
		public static System.Collections.Generic.Dictionary<int, string> IdToChampion;
		public static int Version;
		public static System.Collections.Generic.Dictionary<string, string> LoLObserveServersIpMapping;
		public static System.Collections.Generic.List<string> LoLObservePlatformList;
		public static System.Collections.Generic.List<string> LoLObserveServerList;
		public static event MainWindow.BackGroundInitDoneDelegate StartupDone
		{
			add
			{
				switch (0)
				{
				case 0:
					goto IL_1C;
				}
				int num;
				MainWindow.BackGroundInitDoneDelegate backGroundInitDoneDelegate;
				while (true)
				{
					IL_0A:
					MainWindow.BackGroundInitDoneDelegate backGroundInitDoneDelegate2;
					switch (num)
					{
					case 0:
						if (1 != 0)
						{
						}
						switch ((991602846 == 991602846) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_1C;
						case 1:
							IL_54:
							if (0 != 0)
							{
							}
							goto IL_5D;
						}
						goto IL_54;
					case 1:
						if (backGroundInitDoneDelegate == backGroundInitDoneDelegate2)
						{
							num = 2;
							continue;
						}
						goto IL_5D;
					case 2:
						return;
					}
					goto IL_1C;
					IL_5D:
					int arg_61_0 = 0;
					backGroundInitDoneDelegate2 = backGroundInitDoneDelegate;
					MainWindow.BackGroundInitDoneDelegate value2 = (MainWindow.BackGroundInitDoneDelegate)System.Delegate.Combine(backGroundInitDoneDelegate2, value);
					backGroundInitDoneDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.BackGroundInitDoneDelegate>(ref Utilities.eval_a, value2, backGroundInitDoneDelegate2);
					num = 1;
				}
				return;
				IL_1C:
				backGroundInitDoneDelegate = Utilities.eval_a;
				num = 0;
				goto IL_0A;
			}
			remove
			{
				switch (0)
				{
				case 0:
					goto IL_1C;
				}
				int num;
				MainWindow.BackGroundInitDoneDelegate backGroundInitDoneDelegate;
				while (true)
				{
					IL_0A:
					MainWindow.BackGroundInitDoneDelegate backGroundInitDoneDelegate2;
					switch (num)
					{
					case 0:
						if (1 != 0)
						{
						}
						if (backGroundInitDoneDelegate == backGroundInitDoneDelegate2)
						{
							num = 2;
							continue;
						}
						goto IL_53;
					case 1:
						switch ((1812650019 == 1812650019) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_1C;
						case 1:
							IL_4A:
							if (0 != 0)
							{
							}
							goto IL_53;
						}
						goto IL_4A;
					case 2:
						return;
					}
					goto IL_1C;
					IL_53:
					backGroundInitDoneDelegate2 = backGroundInitDoneDelegate;
					MainWindow.BackGroundInitDoneDelegate value2 = (MainWindow.BackGroundInitDoneDelegate)System.Delegate.Remove(backGroundInitDoneDelegate2, value);
					backGroundInitDoneDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.BackGroundInitDoneDelegate>(ref Utilities.eval_a, value2, backGroundInitDoneDelegate2);
					int arg_73_0 = 0;
					num = 0;
				}
				return;
				IL_1C:
				backGroundInitDoneDelegate = Utilities.eval_a;
				num = 1;
				goto IL_0A;
			}
		}
		public static string GetSimpleVersionNumber(string s)
		{
			if (1 != 0)
			{
			}
			switch ((370374239 == 370374239) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_28:
				break;
			case 1:
				goto IL_29;
			}
			goto IL_28;
			IL_29:
			int arg_2D_0 = 0;
			if (0 != 0)
			{
			}
			string[] array = s.Split(new char[]
			{
				'.'
			});
			return string.Format("{0}.{1}", array[0], array[1]);
		}
		public static int CompareVersion(string v1, string v2)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_40;
				}
				int num2;
				string[] array;
				string[] array2;
				int num3;
				while (true)
				{
					IL_19:
					switch (num)
					{
					case 0:
						if (num2 == 0)
						{
							num = 7;
							continue;
						}
						goto IL_155;
					case 1:
						num = 0;
						continue;
					case 2:
						goto IL_11F;
					case 3:
						if (1 != 0)
						{
						}
						if (uint.Parse(array[0]) > uint.Parse(array2[0]))
						{
							num = 5;
							continue;
						}
						num = 6;
						continue;
					case 4:
						if (num3 != 0)
						{
							num = 1;
							continue;
						}
						return 0;
					case 5:
						return 1;
					case 6:
						if (uint.Parse(array[0]) < uint.Parse(array2[0]))
						{
							num = 2;
							continue;
						}
						num3 = v1.IndexOf('.');
						num2 = v2.IndexOf('.');
						num = 4;
						continue;
					case 7:
						return 0;
					}
					goto IL_40;
				}
				return 1;
				IL_11F:
				switch ((1822374166 == 1822374166) ? 1 : 0)
				{
				case 0:
				case 2:
					return 0;
				case 1:
					IL_E6:
					if (0 != 0)
					{
					}
					return -1;
				}
				goto IL_E6;
				IL_155:
				int arg_159_0 = 0;
				return Utilities.CompareVersion(v1.Substring(num3 + 1), v2.Substring(num2 + 1));
				IL_40:
				array = v1.Split(new char[]
				{
					'.'
				}, 2);
				array2 = v2.Split(new char[]
				{
					'.'
				}, 2);
				num = 3;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		public static void GetNAServerInfo()
		{
			switch (0)
			{
			case 0:
				goto IL_1C;
			}
			int num;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					goto IL_98;
				case 1:
					Utilities.eval_a();
					num = 0;
					continue;
				case 2:
					if (Utilities.eval_a != null)
					{
						num = 1;
						continue;
					}
					return;
				}
				goto IL_1C;
			}
			IL_98:
			if (1 != 0)
			{
			}
			return;
			IL_1C:
			Utilities.HasNewVersion = Utilities.IsUpdateAvailable();
			Utilities.ReadChampionTxt();
			Utilities.CheckNAServerVersion();
			Utilities.GetChampionNumber();
			switch ((930772014 == 930772014) ? 1 : 0)
			{
			case 0:
			case 2:
				goto IL_0A;
			case 1:
				break;
			default:
			{
				int arg_57_0 = 0;
				break;
			}
			}
			if (0 != 0)
			{
			}
			num = 2;
			goto IL_0A;
		}
		public static void CheckNAServerVersion()
		{
			string value;
			int num;
			try
			{
				switch (0)
				{
				case 0:
				{
					IL_0F:
					WebClient webClient = new WebClient();
					try
					{
						string input = webClient.DownloadString("http://gameinfo.na.leagueoflegends.com/assets/js/na.js");
						Regex regex = new Regex("\"v\":\"(?<V>[\\.0-9]+)\",", RegexOptions.IgnoreCase);
						Match match = regex.Match(input);
						value = match.Groups["V"].Value;
					}
					finally
					{
						num = 0;
						while (true)
						{
							switch (num)
							{
							case 0:
								switch (0)
								{
								case 0:
									goto IL_78;
								}
								continue;
							case 1:
								((System.IDisposable)webClient).Dispose();
								num = 2;
								continue;
							case 2:
								goto IL_97;
							}
							IL_78:
							if (webClient == null)
							{
								break;
							}
							num = 1;
						}
						IL_97:;
					}
					goto IL_112;
				}
				}
				goto IL_0F;
			}
			catch (System.Exception)
			{
				return;
			}
			goto IL_A2;
			while (true)
			{
				IL_AC:
				switch (num)
				{
				case 0:
					goto IL_13B;
				case 1:
					if (string.Compare(BaronReplays.Properties.Settings.Default.NAVersion, value) != 0)
					{
						num = 0;
						continue;
					}
					return;
				case 2:
					goto IL_106;
				}
				goto IL_BF;
			}
			IL_106:
			if (1 != 0)
			{
			}
			return;
			IL_13B:
			IL_A2:
			switch (0)
			{
			case 0:
				goto IL_BF;
			}
			goto IL_AC;
			IL_BF:
			switch ((1990486455 == 1990486455) ? 1 : 0)
			{
			case 0:
			case 2:
				goto IL_AC;
			case 1:
			{
				IL_DE:
				if (0 != 0)
				{
				}
				BaronReplays.Properties.Settings.Default.NAVersion = value;
				int arg_F4_0 = 0;
				num = 2;
				goto IL_AC;
			}
			}
			goto IL_DE;
			IL_112:
			num = 1;
			goto IL_AC;
		}
		public static void ReadChampionTxt()
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_38;
				}
				System.IO.StreamReader streamReader;
				while (true)
				{
					IL_19:
					switch (num)
					{
					case 0:
						goto IL_100;
					case 1:
					{
						if (!System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "Champions\\Champions.txt"))
						{
							num = 3;
							continue;
						}
						char[] separator = new char[]
						{
							','
						};
						streamReader = new System.IO.StreamReader("Champions\\Champions.txt");
						num = 4;
						continue;
					}
					case 2:
						goto IL_12E;
					case 3:
						return;
					case 4:
						goto IL_100;
					case 5:
					{
						if (streamReader.EndOfStream)
						{
							goto IL_115;
						}
						char[] separator;
						string[] array = streamReader.ReadLine().Split(separator);
						Utilities.IdToChampion.Add(int.Parse(array[0]), array[1]);
						num = 0;
						continue;
					}
					}
					goto IL_38;
					IL_100:
					num = 5;
				}
				return;
				IL_12E:
				streamReader.Close();
				return;
				IL_38:
				Utilities.IdToChampion = new System.Collections.Generic.Dictionary<int, string>();
				switch ((1681761114 == 1681761114) ? 1 : 0)
				{
				case 0:
				case 2:
				{
					IL_115:
					int arg_119_0 = 0;
					num = 2;
					goto IL_19;
				}
				case 1:
					IL_61:
					if (1 != 0)
					{
					}
					if (0 != 0)
					{
					}
					num = 1;
					goto IL_19;
				}
				goto IL_61;
			}
			}
			goto IL_0F;
		}
		public static void GetChampionNumber()
		{
			int num = 0;
			switch (num)
			{
			case 0:
				while (true)
				{
					IL_0F:
					switch (0)
					{
					case 0:
						goto IL_52;
					}
					WebClient webClient;
					MatchCollection matchCollection;
					while (true)
					{
						IL_19:
						switch ((380409776 == 380409776) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_0F;
						case 1:
						{
							IL_38:
							if (0 != 0)
							{
							}
							System.IO.StreamWriter streamWriter;
							switch (num)
							{
							case 0:
							{
								int arg_68_0 = 0;
								try
								{
									string input = webClient.DownloadString(string.Format("http://ddragon.leagueoflegends.com/cdn/{0}/data/en_US/champion.jsonp", BaronReplays.Properties.Settings.Default.NAVersion));
									Regex regex = new Regex("\"id\":\"(?<NAME>[a-zA-z]+)\",\"key\":\"(?<ID>[0-9]+)\",", RegexOptions.IgnoreCase);
									matchCollection = regex.Matches(input);
									goto IL_72;
								}
								catch
								{
									goto IL_274;
								}
								goto IL_CA;
								IL_72:
								streamWriter = new System.IO.StreamWriter("Champions\\Champions.txt", false);
								System.Collections.IEnumerator enumerator = matchCollection.GetEnumerator();
								num = 1;
								continue;
							}
							case 1:
								try
								{
									num = 4;
									while (true)
									{
										Match match;
										int num2;
										switch (num)
										{
										case 0:
											goto IL_21B;
										case 1:
											goto IL_1CE;
										case 2:
										{
											System.Collections.IEnumerator enumerator;
											if (!enumerator.MoveNext())
											{
												num = 6;
												continue;
											}
											match = (Match)enumerator.Current;
											num2 = int.Parse(match.Groups["ID"].Value);
											num = 7;
											continue;
										}
										case 3:
											Utilities.IdToChampion.Add(num2, match.Groups["NAME"].Value);
											num = 1;
											continue;
										case 4:
											switch (0)
											{
											case 0:
												goto IL_11E;
											}
											continue;
										case 6:
											num = 0;
											continue;
										case 7:
											if (!Utilities.IdToChampion.ContainsKey(num2))
											{
												num = 3;
												continue;
											}
											goto IL_1CE;
										}
										IL_123:
										num = 2;
										continue;
										IL_11E:
										goto IL_123;
										IL_1CE:
										streamWriter.WriteLine(string.Format("{0},{1}", num2, match.Groups["NAME"].Value));
										num = 5;
									}
									IL_21B:
									goto IL_CA;
								}
								finally
								{
									switch (0)
									{
									case 0:
										goto IL_23D;
									}
									System.IDisposable disposable;
									while (true)
									{
										IL_22A:
										switch (num)
										{
										case 0:
											goto IL_271;
										case 1:
											if (disposable != null)
											{
												num = 2;
												continue;
											}
											goto IL_273;
										case 2:
											disposable.Dispose();
											num = 0;
											continue;
										}
										goto IL_23D;
									}
									IL_271:
									IL_273:
									goto EndFinally_8;
									IL_23D:
									System.Collections.IEnumerator enumerator;
									disposable = (enumerator as System.IDisposable);
									num = 1;
									goto IL_22A;
									EndFinally_8:;
								}
								goto IL_274;
							case 2:
								goto IL_DE;
							}
							goto IL_52;
							IL_CA:
							streamWriter.Close();
							num = 2;
							continue;
						}
						}
						goto IL_38;
					}
					IL_52:
					matchCollection = null;
					webClient = new WebClient();
					num = 0;
					goto IL_19;
				}
				IL_DE:
				IL_274:
				if (1 != 0)
				{
				}
				return;
			}
			goto IL_0F;
		}
		public static Process GetProcess(string pName)
		{
			Process[] processesByName;
			while (true)
			{
				processesByName = Process.GetProcessesByName(pName);
				if (processesByName.Length == 0)
				{
					if (1 != 0)
					{
					}
					switch ((654077335 == 654077335) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					}
					break;
				}
				goto IL_4C;
			}
			if (0 != 0)
			{
			}
			int arg_42_0 = 0;
			return null;
			IL_4C:
			return processesByName[0];
		}
		public static int GetProcessCount(string pName)
		{
			if (1 != 0)
			{
			}
			switch ((1190544261 == 1190544261) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_28:
				break;
			case 1:
				goto IL_29;
			}
			goto IL_28;
			IL_29:
			int arg_2D_0 = 0;
			if (0 != 0)
			{
			}
			Process[] processesByName = Process.GetProcessesByName(pName);
			return processesByName.Length;
		}
		public static void UnhandledExceptonHandler(object sender, System.UnhandledExceptionEventArgs args)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_6C;
				}
				System.IO.StreamWriter streamWriter;
				System.Exception ex;
				string text;
				while (true)
				{
					IL_19:
					switch (num)
					{
					case 0:
						streamWriter.WriteLine("Inner exception message: {0}", ex.InnerException.Message);
						num = 16;
						continue;
					case 1:
						goto IL_21D;
					case 2:
						streamWriter.WriteLine("Inner exception");
						num = 14;
						continue;
					case 3:
						goto IL_13A;
					case 4:
					{
						eval_d eval_d = new eval_d();
						eval_d.eval_b(text);
						num = 13;
						continue;
					}
					case 5:
						goto IL_353;
					case 6:
						goto IL_289;
					case 7:
						if (ex.Message != null)
						{
							num = 17;
							continue;
						}
						goto IL_21D;
					case 8:
						goto IL_326;
					case 9:
						if (ex.InnerException != null)
						{
							num = 2;
							continue;
						}
						goto IL_13A;
					case 10:
						if (ex.InnerException.StackTrace != null)
						{
							switch ((1755025139 == 1755025139) ? 1 : 0)
							{
							case 0:
							case 2:
								goto IL_26C;
							case 1:
								IL_1C9:
								if (0 != 0)
								{
								}
								if (1 != 0)
								{
								}
								num = 18;
								continue;
							}
							goto IL_1C9;
						}
						goto IL_13A;
					case 11:
						if (ex.StackTrace != null)
						{
							num = 15;
							continue;
						}
						goto IL_353;
					case 12:
						if (MessageBox.Show(string.Format(Utilities.GetString("AskSendReport"), text), Utilities.GetString("BR"), MessageBoxButton.OKCancel) != MessageBoxResult.Cancel)
						{
							num = 4;
							continue;
						}
						return;
					case 13:
					{
						eval_d eval_d;
						if (eval_d.eval_a())
						{
							num = 8;
							continue;
						}
						goto IL_26C;
					}
					case 14:
						if (ex.InnerException.Message != null)
						{
							num = 0;
							continue;
						}
						goto IL_190;
					case 15:
						streamWriter.WriteLine("Stack trace: {0}", ex.StackTrace);
						num = 5;
						continue;
					case 16:
						goto IL_190;
					case 17:
					{
						streamWriter.WriteLine("Exception message: {0}", ex.Message);
						int arg_2DA_0 = 0;
						num = 1;
						continue;
					}
					case 18:
						streamWriter.WriteLine("Inner exception stack trace: {0}", ex.InnerException.StackTrace);
						num = 3;
						continue;
					}
					goto IL_6C;
					IL_13A:
					streamWriter.WriteLine("End of error log");
					streamWriter.Close();
					num = 12;
					continue;
					IL_190:
					num = 10;
					continue;
					IL_21D:
					num = 11;
					continue;
					IL_26C:
					MessageBox.Show(Utilities.GetString("SendFailed"));
					num = 6;
					continue;
					IL_353:
					num = 9;
				}
				IL_289:
				return;
				IL_326:
				MessageBox.Show(Utilities.GetString("SendSuccess"));
				return;
				IL_6C:
				Utilities.LogWriter.Close();
				ex = (System.Exception)args.ExceptionObject;
				text = System.DateTime.Now.ToString("yyyy-MM-ddTHH-mm-ss") + "CrashLog.txt";
				streamWriter = new System.IO.StreamWriter(text);
				streamWriter.WriteLine("Start of error log:");
				streamWriter.WriteLine("Operating system: {0}", System.Environment.OSVersion.VersionString);
				streamWriter.WriteLine("64-bit operating system: {0}", System.Environment.Is64BitOperatingSystem);
				streamWriter.WriteLine("Processor count: {0}", System.Environment.ProcessorCount);
				streamWriter.WriteLine("BR version: {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
				num = 7;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		public static bool IsUpdateAvailable()
		{
			switch (0)
			{
			case 0:
				goto IL_1C;
			}
			int num;
			WebClient webClient;
			int num2;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					if (1 != 0)
					{
					}
					try
					{
						try
						{
							num2 = int.Parse(webClient.DownloadString("http://ahri.tw/BaronReplays/Version.php"));
						}
						catch
						{
						}
						goto IL_6B;
					}
					finally
					{
						num = 0;
						while (true)
						{
							switch (num)
							{
							case 0:
								switch (0)
								{
								case 0:
									goto IL_C4;
								}
								continue;
							case 1:
								((System.IDisposable)webClient).Dispose();
								num = 2;
								continue;
							case 2:
								goto IL_E1;
							}
							IL_C4:
							if (webClient == null)
							{
								break;
							}
							num = 1;
						}
						IL_E1:;
					}
					return true;
					IL_6B:
					num = 1;
					continue;
				case 1:
					if (Utilities.Version < num2)
					{
						num = 2;
						continue;
					}
					return false;
				case 2:
					return true;
				}
				goto IL_1C;
			}
			return true;
			IL_1C:
			num2 = 0;
			webClient = new WebClient();
			switch ((1141580796 == 1141580796) ? 1 : 0)
			{
			case 0:
			case 2:
				goto IL_0A;
			case 1:
				break;
			default:
			{
				int arg_46_0 = 0;
				break;
			}
			}
			if (0 != 0)
			{
			}
			num = 0;
			goto IL_0A;
		}
		public static string SearchFileInAncestor(string fullPath, string fName)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_48;
				}
				int num3;
				System.Text.StringBuilder stringBuilder;
				string[] array;
				while (true)
				{
					IL_19:
					switch (num)
					{
					case 0:
					{
						int num2;
						if (num2 >= num3)
						{
							num = 4;
							continue;
						}
						stringBuilder.Append(array[num2]);
						stringBuilder.Append('\\');
						num2++;
						int arg_CD_0 = 0;
						num = 7;
						continue;
					}
					case 1:
						switch ((1875498159 == 1875498159) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_90;
						}
						goto IL_17D;
					case 2:
						goto IL_B0;
					case 3:
					{
						if (num3 < 0)
						{
							num = 1;
							continue;
						}
						stringBuilder = new System.Text.StringBuilder();
						int num2 = 0;
						num = 9;
						continue;
					}
					case 4:
						num = 6;
						continue;
					case 5:
						goto IL_E4;
					case 6:
						goto IL_90;
					case 7:
						goto IL_12E;
					case 8:
						goto IL_E4;
					case 9:
						goto IL_12E;
					}
					goto IL_48;
					IL_90:
					if (System.IO.File.Exists(stringBuilder.ToString() + fName))
					{
						num = 2;
						continue;
					}
					num3--;
					num = 8;
					continue;
					IL_E4:
					num = 3;
					continue;
					IL_12E:
					if (1 != 0)
					{
					}
					num = 0;
				}
				IL_B0:
				return stringBuilder.Remove(stringBuilder.Length - 1, 1).ToString();
				IL_17D:
				if (0 != 0)
				{
				}
				return null;
				IL_48:
				array = fullPath.Split(new char[]
				{
					'\\'
				});
				num3 = array.Length - 1;
				num = 5;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		public static bool SelectLoLExePath()
		{
			OpenFileDialog openFileDialog;
			while (true)
			{
				openFileDialog = new OpenFileDialog();
				openFileDialog.Filter = "League of Legends.exe|League of Legends.exe";
				openFileDialog.Title = Utilities.GetString("PickLoLExe");
				openFileDialog.InitialDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyComputer);
				openFileDialog.ShowDialog();
				if (openFileDialog.FileName == string.Empty)
				{
					switch ((1176653566 == 1176653566) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					}
					break;
				}
				goto IL_87;
			}
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			int arg_7D_0 = 0;
			return false;
			IL_87:
			BaronReplays.Properties.Settings.Default.LoLGameExe = openFileDialog.FileName;
			BaronReplays.Properties.Settings.Default.Save();
			return true;
		}
		public static void IfDirectoryNotExitThenCreate(string path)
		{
			int num = 1;
			while (true)
			{
				switch ((112492327 == 112492327) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_09;
				case 1:
				{
					IL_32:
					if (0 != 0)
					{
					}
					if (1 != 0)
					{
					}
					int arg_47_0 = 0;
					switch (num)
					{
					case 0:
						System.IO.Directory.CreateDirectory(path);
						num = 2;
						continue;
					case 1:
						goto IL_09;
					case 2:
						return;
					}
					goto IL_61;
				}
				}
				goto IL_32;
				IL_09:
				switch (0)
				{
				case 0:
					IL_61:
					if (System.IO.Directory.Exists(path))
					{
						return;
					}
					num = 0;
					break;
				}
			}
		}
		public static BitmapImage GetBitmapImage(string s)
		{
			BitmapImage bitmapImage;
			switch ((2038050122 == 2038050122) ? 1 : 0)
			{
			case 0:
			case 2:
				goto IL_78;
			case 1:
			{
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
				System.IO.FileStream fileStream = new System.IO.FileStream(s, System.IO.FileMode.Open, System.IO.FileAccess.Read);
				try
				{
					memoryStream.SetLength(fileStream.Length);
					fileStream.Read(memoryStream.GetBuffer(), 0, (int)fileStream.Length);
					memoryStream.Flush();
					fileStream.Close();
					goto IL_78;
				}
				finally
				{
					int num = 1;
					while (true)
					{
						switch (num)
						{
						case 0:
							goto IL_FD;
						case 1:
							switch (0)
							{
							case 0:
								goto IL_E0;
							}
							continue;
						case 2:
							((System.IDisposable)fileStream).Dispose();
							num = 0;
							continue;
						}
						IL_E0:
						if (fileStream == null)
						{
							break;
						}
						num = 2;
					}
					IL_FD:;
				}
				return bitmapImage;
			}
			}
			goto IL_1F;
			while (true)
			{
				IL_78:
				int arg_7C_0 = 0;
				bitmapImage = new BitmapImage();
				try
				{
					bitmapImage.BeginInit();
					bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
					System.IO.MemoryStream memoryStream;
					bitmapImage.StreamSource = memoryStream;
					bitmapImage.EndInit();
					bitmapImage.Freeze();
					break;
				}
				catch
				{
					throw new System.Exception(string.Format("Error loading file: {0}", s));
				}
			}
			return bitmapImage;
		}
		public static string GetString(string name)
		{
			string result;
			try
			{
				result = (Application.Current.FindResource(name) as string);
			}
			catch
			{
				return name;
			}
			if (1 != 0)
			{
			}
			switch ((1821734298 == 1821734298) ? 1 : 0)
			{
			case 0:
			case 2:
				return name;
			case 1:
				break;
			default:
			{
				int arg_44_0 = 0;
				break;
			}
			}
			if (0 != 0)
			{
			}
			return result;
		}
		static Utilities()
		{
			// Note: this type is marked as 'beforefieldinit'.
			switch ((1592828600 == 1592828600) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			int arg_2D_0 = 0;
			if (0 != 0)
			{
			}
			Utilities.LogWriter = new System.IO.StreamWriter("Log.txt", true);
			Utilities.Version = 120;
			Utilities.LoLObserveServersIpMapping = new System.Collections.Generic.Dictionary<string, string>
			{

				{
					"TW",
					"112.121.84.194:8088"
				},

				{
					"NA1",
					"spectator.na.lol.riotgames.com:8088"
				},

				{
					"EUW1",
					"spectator.eu.lol.riotgames.com:8088"
				},

				{
					"EUN1",
					"spectator.eu.lol.riotgames.com:8088"
				},

				{
					"EU",
					"spectator.eu.lol.riotgames.com:8088"
				},

				{
					"BR1",
					"spectator.br.lol.riotgames.com:80"
				},

				{
					"LA1",
					"spectator.br.lol.riotgames.com:80"
				},

				{
					"LA2",
					"spectator.br.lol.riotgames.com:80"
				},

				{
					"BRLA",
					"spectator.br.lol.riotgames.com:80"
				},

				{
					"TR1",
					"spectator.tr.lol.riotgames.com:80"
				},

				{
					"RU",
					"spectator.tr.lol.riotgames.com:80"
				},

				{
					"TRRU",
					"spectator.tr.lol.riotgames.com:80"
				},

				{
					"PBE1",
					"spectator.pbe1.lol.riotgames.com:8088"
				},

				{
					"SG",
					"203.116.112.222:8088"
				},

				{
					"KR",
					"110.45.191.11:80"
				},

				{
					"OC1",
					"192.64.169.29:8088"
				}
			};
			Utilities.LoLObservePlatformList = new System.Collections.Generic.List<string>
			{
				"TW",
				"NA1",
				"EUW1",
				"EUN1",
				"BR1",
				"LA1",
				"LA2",
				"TR1",
				"RU",
				"SG",
				"KR",
				"OC1"
			};
			Utilities.LoLObserveServerList = new System.Collections.Generic.List<string>
			{
				"TW",
				"NA1",
				"EU",
				"BRLA",
				"TRRU",
				"SG",
				"KR",
				"OC1"
			};
		}
	}
}
