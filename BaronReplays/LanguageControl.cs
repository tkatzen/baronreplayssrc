using BaronReplays.Properties;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
namespace BaronReplays
{
	public class LanguageControl
	{
		public static void ChangeLanguage(string lang)
		{
			switch (0)
			{
			case 0:
				goto IL_34;
			}
			ResourceDictionary resourceDictionary;
			while (true)
			{
				IL_0A:
				int num;
				switch (num)
				{
				case 0:
					if (resourceDictionary != null)
					{
						if (1 != 0)
						{
						}
						num = 6;
						continue;
					}
					return;
				case 1:
					Application.Current.Resources.MergedDictionaries.Clear();
					num = 4;
					continue;
				case 2:
					BaronReplays.Properties.Settings.Default.Language = lang;
					BaronReplays.Properties.Settings.Default.Save();
					num = 5;
					continue;
				case 3:
					try
					{
						resourceDictionary = (Application.LoadComponent(new Uri("Lang/" + lang + ".xaml", UriKind.Relative)) as ResourceDictionary);
						goto IL_179;
					}
					catch (System.Exception)
					{
						resourceDictionary = (Application.LoadComponent(new Uri("Lang/zh-tw.xaml", UriKind.Relative)) as ResourceDictionary);
						lang = "zh-tw";
						goto IL_179;
					}
					goto IL_135;
					IL_179:
					num = 0;
					continue;
				case 4:
					goto IL_87;
				case 5:
					goto IL_E1;
				case 6:
					num = 8;
					continue;
				case 7:
					if (string.Compare(BaronReplays.Properties.Settings.Default.Language, lang) != 0)
					{
						num = 2;
						continue;
					}
					return;
				case 8:
					if (Application.Current.Resources.MergedDictionaries.Count > 0)
					{
						num = 1;
						continue;
					}
					goto IL_135;
				}
				goto IL_34;
			}
			IL_87:
			goto IL_135;
			IL_E1:
			int arg_E5_0 = 0;
			return;
			IL_34:
			resourceDictionary = null;
			switch ((865263206 == 865263206) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_135:
				Application.Current.Resources.MergedDictionaries.Add(resourceDictionary);
				int num = 7;
				goto IL_0A;
			}
			case 1:
			{
				IL_55:
				if (0 != 0)
				{
				}
				int num = 3;
				goto IL_0A;
			}
			}
			goto IL_55;
		}
		public static void LoadLanguage()
		{
			int num = 4;
			while (true)
			{
				int arg_17_0 = 0;
				switch (num)
				{
				case 0:
					goto IL_55;
				case 1:
					if (1 != 0)
					{
					}
					if (System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName.CompareTo("zh") == 0)
					{
						num = 2;
						continue;
					}
					goto IL_108;
				case 2:
					goto IL_B2;
				case 3:
					goto IL_106;
				case 4:
					switch (0)
					{
					case 0:
						goto IL_3D;
					}
					continue;
				case 5:
					if (BaronReplays.Properties.Settings.Default.Language.Length == 0)
					{
						switch ((782055059 == 782055059) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_FA;
						case 1:
							IL_76:
							if (0 != 0)
							{
							}
							num = 1;
							continue;
						}
						goto IL_76;
					}
					IL_FA:
					num = 3;
					continue;
				}
				IL_3D:
				if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
				{
					num = 0;
				}
				else
				{
					num = 5;
				}
			}
			IL_55:
			LanguageControl.ChangeLanguage("zh-TW");
			return;
			IL_B2:
			LanguageControl.ChangeLanguage("zh-TW");
			return;
			IL_106:
			LanguageControl.ChangeLanguage(BaronReplays.Properties.Settings.Default.Language);
			return;
			IL_108:
			LanguageControl.ChangeLanguage("en-US");
		}
	}
}
