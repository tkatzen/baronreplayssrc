using BaronReplays.Properties;
using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Resources;
using System.Windows.Threading;
namespace BaronReplays
{
	public class MainWindow : Window, IComponentConnector
	{
		public delegate void BackGroundInitDoneDelegate();
		public delegate void RecordDelegate(SimpleLoLRecord slr);
		public delegate void LoLLauncherDoneDelegate(bool isSuccess, string reason);
		public delegate void RecordDoneDelegate(LoLRecorder sender, bool isSuccess, string reason);
		public delegate void ShowRecordFailedMessageBoxDelegate(string reason);
		public delegate void RecordInfoDoneDelegate(LoLRecorder recoder);
		public delegate void StartNewRecodingDelegate(LoLRecorder r, bool playNow = false);
		public delegate void PlayGameDelegate(GameInfo info);
		public delegate void LoadRecordsDelegate();
		private DispatcherTimer eval_a;
		private bool eval_b;
		private System.Collections.Generic.Dictionary<LoLRecorder, System.Threading.Thread> eval_c;
		private NotifyIcon eval_d;
		private bool eval_e;
		private LoLLauncher eval_f;
		private System.Collections.Generic.List<SimpleLoLRecord> eval_g;
		private bool eval_h;
		private System.Threading.Thread eval_i;
		private eval_b eval_j;
		private bool eval_k;
		private object eval_l = new object();
		private FileSystemWatcher eval_m;
		private bool eval_n;
		internal System.Windows.Controls.MenuItem eval_o;
		internal System.Windows.Controls.MenuItem eval_p;
		internal System.Windows.Controls.MenuItem eval_q;
		internal System.Windows.Controls.MenuItem eval_r;
		internal System.Windows.Controls.MenuItem eval_s;
		internal System.Windows.Controls.MenuItem eval_t;
		internal System.Windows.Controls.MenuItem eval_u;
		internal System.Windows.Controls.MenuItem eval_v;
		internal System.Windows.Controls.MenuItem eval_w;
		internal System.Windows.Controls.MenuItem eval_x;
		internal System.Windows.Controls.MenuItem eval_y;
		internal Grid eval_z;
		internal System.Windows.Controls.ListBox eval_aa;
		internal Grid eval_ab;
		internal System.Windows.Controls.WebBrowser eval_ac;
		internal TextBlock eval_ad;
		internal TextBlock eval_ae;
		internal TextBlock eval_af;
		private bool eval_ag;
		[System.Runtime.CompilerServices.CompilerGenerated]
		private static Func<System.IO.FileInfo, System.DateTime> eval_ah;
		public System.Collections.Generic.List<SimpleLoLRecord> Records
		{
			get
			{
				switch ((1246473869 == 1246473869) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				int arg_2A_0 = 0;
				if (1 != 0)
				{
				}
				return this.eval_g;
			}
			set
			{
				if (1 != 0)
				{
				}
				switch ((65166476 == 65166476) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_28:
					break;
				case 1:
					goto IL_29;
				}
				goto IL_28;
				IL_29:
				int arg_2D_0 = 0;
				if (0 != 0)
				{
				}
				this.eval_g = value;
			}
		}
		public MainWindow()
		{
			this.eval_h = false;
			this.InitializeComponent();
		}
		private void eval_u()
		{
			switch (0)
			{
			case 0:
				goto IL_1C;
			}
			int num;
			StreamResourceInfo resourceStream;
			while (true)
			{
				IL_0A:
				System.IO.Stream stream;
				switch (num)
				{
				case 0:
					switch ((396617546 == 396617546) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_1C;
					case 1:
						IL_85:
						if (0 != 0)
						{
						}
						goto IL_E6;
					}
					goto IL_85;
				case 1:
					try
					{
						this.eval_d.Icon = new Icon(stream);
						goto IL_FB;
					}
					finally
					{
						num = 2;
						while (true)
						{
							switch (num)
							{
							case 0:
								goto IL_E3;
							case 1:
								((System.IDisposable)stream).Dispose();
								num = 0;
								continue;
							case 2:
								switch (0)
								{
								case 0:
									goto IL_C6;
								}
								continue;
							}
							IL_C6:
							if (stream == null)
							{
								break;
							}
							num = 1;
						}
						IL_E3:;
					}
					goto IL_E6;
				case 2:
					if (resourceStream != null)
					{
						num = 0;
						continue;
					}
					goto IL_FB;
				}
				goto IL_1C;
				IL_E6:
				stream = resourceStream.Stream;
				num = 1;
			}
			IL_FB:
			this.eval_d.DoubleClick += new System.EventHandler(this.eval_c);
			this.eval_d.ContextMenuStrip = new ContextMenuStrip();
			this.eval_d.ContextMenuStrip.Items.Add(Utilities.GetString("Exit"));
			this.eval_d.ContextMenuStrip.Items[0].Click += new System.EventHandler(this.eval_d);
			return;
			IL_1C:
			if (1 != 0)
			{
			}
			this.eval_d = new NotifyIcon();
			resourceStream = System.Windows.Application.GetResourceStream(new Uri("Icon.ico", UriKind.Relative));
			int arg_46_0 = 0;
			num = 2;
			goto IL_0A;
		}
		private void eval_l(object A_0, RoutedEventArgs A_1)
		{
			switch ((1765565877 == 1765565877) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			int arg_2D_0 = 0;
			if (0 != 0)
			{
			}
			this.Records = new System.Collections.Generic.List<SimpleLoLRecord>();
			this.eval_aa.ItemsSource = this.Records;
			this.eval_c = new System.Collections.Generic.Dictionary<LoLRecorder, System.Threading.Thread>();
			this.eval_s();
			this.eval_f();
			Utilities.StartupDone += new MainWindow.BackGroundInitDoneDelegate(this.eval_t);
			this.eval_u();
			this.eval_m();
			this.eval_ac.Navigate("http://ahri.tw/News/br.php");
			this.eval_r();
			this.eval_q();
			this.eval_i = new System.Threading.Thread(new System.Threading.ThreadStart(Utilities.GetNAServerInfo));
			this.eval_i.Start();
			this.eval_h = true;
			this.eval_a();
		}
		private void eval_t()
		{
			int num = 4;
			while (true)
			{
				IL_13:
				switch (num)
				{
				case 0:
					goto IL_75;
				case 1:
					if (Utilities.HasNewVersion)
					{
						num = 3;
						continue;
					}
					goto IL_D0;
				case 2:
					goto IL_8C;
				case 3:
					this.eval_af.Visibility = Visibility.Visible;
					num = 2;
					continue;
				case 4:
					switch (0)
					{
					case 0:
						goto IL_2D;
					}
					continue;
				}
				IL_2D:
				while (!base.Dispatcher.CheckAccess())
				{
					switch ((1084424171 == 1084424171) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					case 1:
					{
						IL_59:
						if (0 != 0)
						{
						}
						int arg_64_0 = 0;
						num = 0;
						goto IL_13;
					}
					}
					goto IL_59;
				}
				num = 1;
			}
			IL_75:
			Action method = new Action(this.eval_t);
			base.Dispatcher.Invoke(method, new object[0]);
			return;
			IL_8C:
			IL_D0:
			if (1 != 0)
			{
			}
		}
		private void eval_s()
		{
			while (true)
			{
				IL_00:
				int num = 2;
				while (true)
				{
					switch ((2132279751 == 2132279751) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_00;
					case 1:
						IL_32:
						if (1 != 0)
						{
						}
						if (0 != 0)
						{
						}
						switch (num)
						{
						case 0:
						{
							int arg_75_0 = 0;
							BaronReplays.Properties.Settings.Default.ReplayDir = System.AppDomain.CurrentDomain.BaseDirectory + "Replays";
							num = 1;
							continue;
						}
						case 1:
							goto IL_A7;
						case 2:
							switch (0)
							{
							case 0:
								goto IL_55;
							}
							continue;
						}
						IL_55:
						if (BaronReplays.Properties.Settings.Default.ReplayDir.Length == 0)
						{
							num = 0;
							continue;
						}
						goto IL_A9;
					}
					goto IL_32;
				}
			}
			IL_A7:
			IL_A9:
			Utilities.IfDirectoryNotExitThenCreate(BaronReplays.Properties.Settings.Default.ReplayDir);
		}
		private void eval_r()
		{
			switch (0)
			{
			case 0:
				goto IL_1C;
			}
			int num;
			string[] commandLineArgs;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					switch ((1356562899 == 1356562899) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_1C;
					case 1:
						IL_65:
						if (0 != 0)
						{
						}
						if (1 != 0)
						{
						}
						this.eval_f = new LoLLauncher(commandLineArgs[1]);
						this.eval_k();
						num = 1;
						continue;
					}
					goto IL_65;
				case 1:
					return;
				case 2:
				{
					int arg_2F_0 = 0;
					if (commandLineArgs.Length > 1)
					{
						num = 0;
						continue;
					}
					return;
				}
				}
				goto IL_1C;
			}
			return;
			IL_1C:
			commandLineArgs = System.Environment.GetCommandLineArgs();
			num = 2;
			goto IL_0A;
		}
		private void eval_q()
		{
			switch ((238479356 == 238479356) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			int arg_2D_0 = 0;
			if (0 != 0)
			{
			}
			this.eval_j = new eval_b();
			eval_b expr_4D = this.eval_j;
			expr_4D.eval_e = (MainWindow.RecordDelegate)System.Delegate.Combine(expr_4D.eval_e, new MainWindow.RecordDelegate(this.PlayRecord));
			new System.Threading.Thread(new System.Threading.ThreadStart(this.eval_j.eval_b)).Start();
		}
		private void eval_d(object A_0, System.EventArgs A_1)
		{
			switch ((2025800540 == 2025800540) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			int arg_2D_0 = 0;
			if (0 != 0)
			{
			}
			this.eval_n = true;
			base.Close();
		}
		private void eval_p()
		{
			switch ((108915662 == 108915662) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			int arg_2D_0 = 0;
			if (0 != 0)
			{
			}
			base.Visibility = Visibility.Visible;
			this.eval_d.Visible = false;
			base.Focus();
		}
		private void eval_o()
		{
			switch ((851797825 == 851797825) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (0 != 0)
			{
			}
			int arg_2A_0 = 0;
			if (1 != 0)
			{
			}
			base.Visibility = Visibility.Hidden;
			this.eval_d.Visible = true;
		}
		private void eval_c(object A_0, System.EventArgs A_1)
		{
			switch ((1518973660 == 1518973660) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (0 != 0)
			{
			}
			int arg_2A_0 = 0;
			if (1 != 0)
			{
			}
			this.eval_e = false;
			this.eval_p();
		}
		private void eval_n()
		{
			int num = 3;
			while (true)
			{
				switch (num)
				{
				case 0:
					goto IL_B3;
				case 1:
					this.eval_ad.Text = Utilities.GetString("AutoStart");
					num = 2;
					continue;
				case 2:
					goto IL_D6;
				case 3:
					switch (0)
					{
					case 0:
						goto IL_29;
					}
					continue;
				}
				IL_29:
				if (1 != 0)
				{
				}
				int arg_37_0 = 0;
				if (this.eval_c.Count == 0)
				{
					switch ((1011021358 == 1011021358) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_B3;
					case 1:
						IL_6B:
						if (0 != 0)
						{
						}
						num = 1;
						continue;
					}
					goto IL_6B;
				}
				this.eval_ad.Text = string.Format(Utilities.GetString("RemainGameStatus"), this.eval_c.Count);
				num = 0;
			}
			IL_B3:
			IL_D6:
			this.eval_d.Text = this.eval_ad.Text;
		}
		private void eval_m()
		{
			if (1 != 0)
			{
			}
			switch ((1811810065 == 1811810065) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_28:
				break;
			case 1:
				goto IL_29;
			}
			goto IL_28;
			IL_29:
			int arg_2D_0 = 0;
			if (0 != 0)
			{
			}
			this.eval_b = false;
			this.eval_a = new DispatcherTimer();
			this.eval_a.Interval = new System.TimeSpan(5000000L);
			this.eval_a.Tick += new System.EventHandler(this.eval_b);
			this.eval_a.Start();
		}
		public void DeleteRecord(SimpleLoLRecord slr)
		{
			switch (0)
			{
			case 0:
				goto IL_1C;
			}
			int num;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					switch ((1550753074 == 1550753074) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_1C;
					case 1:
						IL_7D:
						if (0 != 0)
						{
						}
						System.IO.File.Delete(slr.FileName);
						num = 1;
						continue;
					}
					goto IL_7D;
				case 1:
					goto IL_9D;
				case 2:
					if (System.IO.File.Exists(slr.FileName))
					{
						num = 0;
						continue;
					}
					goto IL_9F;
				}
				goto IL_1C;
			}
			IL_9D:
			IL_9F:
			this.UpdateRecordList();
			return;
			IL_1C:
			this.Records.Remove(slr);
			if (1 != 0)
			{
			}
			int arg_37_0 = 0;
			num = 2;
			goto IL_0A;
		}
		public void PlayRecord(SimpleLoLRecord slr)
		{
			int num = 5;
			while (true)
			{
				switch (num)
				{
				case 0:
					goto IL_4E;
				case 1:
					goto IL_B5;
				case 2:
					num = 6;
					continue;
				case 3:
					if (1 != 0)
					{
					}
					if (this.eval_f == null)
					{
						num = 2;
						continue;
					}
					System.Windows.MessageBox.Show(Utilities.GetString("AlreadyPlaying"), Utilities.GetString("BR"));
					num = 4;
					continue;
				case 4:
					return;
				case 5:
					switch (0)
					{
					case 0:
						goto IL_35;
					}
					continue;
				case 6:
					switch ((1132099359 == 1132099359) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_4E;
					case 1:
						IL_8B:
						if (0 != 0)
						{
						}
						if (slr.RecoringRecord == null)
						{
							int arg_A1_0 = 0;
							num = 1;
							continue;
						}
						return;
					}
					goto IL_8B;
				}
				IL_35:
				if (!base.Dispatcher.CheckAccess())
				{
					num = 0;
				}
				else
				{
					num = 3;
				}
			}
			IL_4E:
			MainWindow.RecordDelegate method = new MainWindow.RecordDelegate(this.PlayRecord);
			base.Dispatcher.Invoke(method, new object[]
			{
				slr
			});
			return;
			IL_B5:
			this.eval_f = new LoLLauncher(slr);
			this.eval_k();
		}
		public static string GetMainModuleFilepath(int processId)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				if (1 != 0)
				{
				}
				string queryString = "SELECT ProcessId, ExecutablePath FROM Win32_Process WHERE ProcessId = " + processId;
				ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher(queryString);
				string result;
				try
				{
					ManagementObjectCollection managementObjectCollection = managementObjectSearcher.Get();
					try
					{
						switch (0)
						{
						case 0:
							goto IL_6B;
						}
						ManagementObject managementObject;
						while (true)
						{
							IL_54:
							switch (num)
							{
							case 0:
								goto IL_BB;
							case 1:
								if (managementObject != null)
								{
									num = 2;
									continue;
								}
								num = 0;
								continue;
							case 2:
								result = (string)managementObject["ExecutablePath"];
								num = 3;
								continue;
							case 3:
								goto IL_AC;
							}
							goto IL_6B;
						}
						IL_AC:
						return result;
						IL_BB:
						goto IL_12C;
						IL_6B:
						managementObject = managementObjectCollection.Cast<ManagementObject>().FirstOrDefault<ManagementObject>();
						num = 1;
						goto IL_54;
					}
					finally
					{
						while (true)
						{
							IL_BD:
							num = 0;
							while (true)
							{
								switch ((1211545032 == 1211545032) ? 1 : 0)
								{
								case 0:
								case 2:
									goto IL_BD;
								case 1:
									IL_F0:
									if (0 != 0)
									{
									}
									switch (num)
									{
									case 0:
										switch (0)
										{
										case 0:
											goto IL_10A;
										}
										continue;
									case 1:
										goto IL_129;
									case 2:
										((System.IDisposable)managementObjectCollection).Dispose();
										num = 1;
										continue;
									}
									IL_10A:
									if (managementObjectCollection != null)
									{
										num = 2;
										continue;
									}
									goto IL_12B;
								}
								goto IL_F0;
							}
						}
						IL_129:
						IL_12B:;
					}
					IL_12C:
					goto IL_33;
				}
				finally
				{
					num = 2;
					while (true)
					{
						switch (num)
						{
						case 0:
							goto IL_177;
						case 1:
							((System.IDisposable)managementObjectSearcher).Dispose();
							num = 0;
							continue;
						case 2:
							switch (0)
							{
							case 0:
								goto IL_158;
							}
							continue;
						}
						IL_158:
						if (managementObjectSearcher == null)
						{
							break;
						}
						num = 1;
					}
					IL_177:;
				}
				return result;
				IL_33:
				int arg_37_0 = 0;
				return null;
			}
			}
			goto IL_0F;
		}
		private void eval_l()
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_4C;
				}
				string text3;
				while (true)
				{
					IL_19:
					string text;
					string text2;
					string baseDirectory;
					switch (num)
					{
					case 0:
						goto IL_189;
					case 1:
						System.IO.Directory.CreateDirectory(text);
						num = 4;
						continue;
					case 2:
						if (1 != 0)
						{
						}
						if (!System.IO.File.Exists(text2))
						{
							num = 8;
							continue;
						}
						return;
					case 3:
						return;
					case 4:
						switch ((2088088574 == 2088088574) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_189;
						case 1:
							IL_A1:
							if (0 != 0)
							{
							}
							goto IL_15B;
						}
						goto IL_A1;
					case 5:
						if (string.Compare(text3, BaronReplays.Properties.Settings.Default.LoLGameExe) != 0)
						{
							num = 0;
							continue;
						}
						goto IL_AD;
					case 6:
						if (text3 == null)
						{
							num = 9;
							continue;
						}
						baseDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
						num = 5;
						continue;
					case 7:
						if (!System.IO.Directory.Exists(text))
						{
							int arg_EE_0 = 0;
							num = 1;
							continue;
						}
						goto IL_15B;
					case 8:
						System.IO.File.Copy(text3, text2);
						num = 3;
						continue;
					case 9:
						return;
					case 10:
						goto IL_AD;
					}
					goto IL_4C;
					IL_AD:
					text = baseDirectory + "\\LoLExecutable\\" + Utilities.GetSimpleVersionNumber(FileVersionInfo.GetVersionInfo(text3).ProductVersion);
					text2 = text + "\\League of Legends.exe";
					num = 7;
					continue;
					IL_15B:
					num = 2;
					continue;
					IL_189:
					BaronReplays.Properties.Settings.Default.LoLGameExe = text3;
					BaronReplays.Properties.Settings.Default.Save();
					num = 10;
				}
				return;
				IL_4C:
				text3 = global::eval_a.eval_a();
				num = 6;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		public void LoLLauncherDone(bool isSuccess, string reason)
		{
			int num = 4;
			while (true)
			{
				switch (num)
				{
				case 0:
					goto IL_39;
				case 1:
					System.Windows.MessageBox.Show(reason, Utilities.GetString("CannotPlay"));
					num = 3;
					continue;
				case 2:
					switch ((409813051 == 409813051) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_39;
					case 1:
						break;
					default:
					{
						int arg_82_0 = 0;
						break;
					}
					}
					if (1 != 0)
					{
					}
					if (0 != 0)
					{
					}
					if (reason != null)
					{
						num = 1;
						continue;
					}
					goto IL_AD;
				case 3:
					goto IL_55;
				case 4:
					switch (0)
					{
					case 0:
						goto IL_2D;
					}
					continue;
				}
				IL_2D:
				if (!isSuccess)
				{
					num = 0;
					continue;
				}
				break;
				IL_39:
				num = 2;
			}
			IL_55:
			IL_AD:
			this.eval_f = null;
			this.eval_a.Start();
		}
		private void eval_k()
		{
			if (!global::eval_a.eval_b())
			{
				while (true)
				{
					switch ((719532589 == 719532589) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					}
					break;
				}
				int arg_2C_0 = 0;
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				this.eval_f = null;
				return;
			}
			this.eval_a.Stop();
			this.eval_f.Parent = this;
			LoLLauncher expr_6A = this.eval_f;
			expr_6A.AnyEvent = (MainWindow.LoLLauncherDoneDelegate)System.Delegate.Combine(expr_6A.AnyEvent, new MainWindow.LoLLauncherDoneDelegate(this.LoLLauncherDone));
			this.eval_f.StartPlaying();
			this.eval_b(null, null);
		}
		public void RecordDone(LoLRecorder sender, bool isSuccess, string reason)
		{
			switch (0)
			{
			case 0:
			{
				IL_0F:
				int num = 7;
				while (true)
				{
					string text;
					switch (num)
					{
					case 0:
						goto IL_19B;
					case 1:
						goto IL_98;
					case 2:
						this.eval_c.Remove(sender);
						num = 9;
						continue;
					case 3:
					{
						if (isSuccess)
						{
							num = 8;
							continue;
						}
						MainWindow.ShowRecordFailedMessageBoxDelegate method = new MainWindow.ShowRecordFailedMessageBoxDelegate(this.eval_a);
						base.Dispatcher.Invoke(method, new object[]
						{
							reason
						});
						num = 0;
						continue;
					}
					case 4:
						text = text.Insert(text.Length - 4, new System.Random().Next(9999).ToString());
						goto IL_1F5;
					case 5:
						goto IL_E4;
					case 6:
						if (System.IO.File.Exists(text))
						{
							num = 4;
							continue;
						}
						goto IL_98;
					case 7:
						switch (0)
						{
						case 0:
							goto IL_52;
						}
						continue;
					case 8:
					{
						int arg_1C1_0 = 0;
						string str = FileNameGenerater.GenerateFilename(sender);
						text = BaronReplays.Properties.Settings.Default.ReplayDir + "\\" + str;
						Utilities.IfDirectoryNotExitThenCreate(BaronReplays.Properties.Settings.Default.ReplayDir);
						if (1 != 0)
						{
						}
						num = 6;
						continue;
					}
					case 9:
						goto IL_19D;
					}
					IL_52:
					switch ((1894850440 == 1894850440) ? 1 : 0)
					{
					case 0:
					case 2:
						IL_1F5:
						num = 1;
						continue;
					case 1:
						IL_71:
						if (0 != 0)
						{
						}
						if (this.eval_c.ContainsKey(sender))
						{
							num = 2;
							continue;
						}
						goto IL_19D;
					}
					goto IL_71;
					IL_98:
					this.eval_k = true;
					sender.record.writeToFile(text);
					this.AddRecord(sender.record);
					Action method2 = new Action(this.eval_j);
					base.Dispatcher.Invoke(method2, new object[0]);
					num = 5;
					continue;
					IL_19D:
					num = 3;
				}
				IL_E4:
				IL_19B:
				Action method3 = new Action(this.eval_n);
				base.Dispatcher.Invoke(method3, new object[0]);
				return;
			}
			}
			goto IL_0F;
		}
		private void eval_j()
		{
			switch ((1444459333 == 1444459333) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			int arg_23_0 = 0;
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			this.eval_a(Utilities.GetString("RecordSuucssed"), true);
		}
		private void eval_a(string A_0)
		{
			switch ((1060240487 == 1060240487) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			int arg_23_0 = 0;
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.eval_a(string.Format(Utilities.GetString("RecordFailed"), A_0), false);
		}
		public void RecordInfoDone(LoLRecorder recoder)
		{
			int num = 0;
			while (true)
			{
				Process process;
				switch (num)
				{
				case 0:
					switch (0)
					{
					case 0:
						goto IL_35;
					}
					continue;
				case 1:
					if (BaronReplays.Properties.Settings.Default.KillProcess)
					{
						num = 3;
						continue;
					}
					goto IL_DD;
				case 2:
					goto IL_AA;
				case 3:
					num = 6;
					continue;
				case 4:
					process = Utilities.GetProcess("League of Legends");
					num = 1;
					continue;
				case 5:
					goto IL_8D;
				case 6:
					if (process != null)
					{
						num = 2;
						continue;
					}
					goto IL_DD;
				}
				IL_35:
				if (!recoder.selfGame)
				{
					switch ((974225823 == 974225823) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_AA;
					case 1:
					{
						IL_5F:
						if (0 != 0)
						{
						}
						int arg_6A_0 = 0;
						num = 4;
						continue;
					}
					}
					goto IL_5F;
				}
				break;
				IL_AA:
				process.CloseMainWindow();
				num = 5;
			}
			IL_8D:
			IL_DD:
			if (1 != 0)
			{
			}
		}
		private void eval_i()
		{
			int arg_04_0 = 0;
			if (1 != 0)
			{
			}
			switch ((1042919874 == 1042919874) ? 1 : 0)
			{
			case 0:
			case 2:
				return;
			case 1:
				IL_35:
				if (0 != 0)
				{
				}
				if (this.eval_f != null)
				{
					return;
				}
				this.eval_h();
				this.eval_l();
				return;
			}
			goto IL_35;
		}
		private void eval_h()
		{
			switch ((236497296 == 236497296) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			int arg_23_0 = 0;
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			LoLRecorder r = new LoLRecorder();
			this.StartNewRecoding(r, false);
		}
		public void StartNewRecoding(LoLRecorder r, bool playNow = false)
		{
			int num = 1;
			while (true)
			{
				if (1 != 0)
				{
				}
				switch (num)
				{
				case 0:
					num = 3;
					continue;
				case 1:
					switch (0)
					{
					case 0:
						goto IL_3F;
					}
					continue;
				case 2:
					return;
				case 3:
					if (!global::eval_a.eval_b())
					{
						num = 4;
						continue;
					}
					goto IL_54;
				case 4:
					switch ((589765783 == 589765783) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_A5;
					}
					goto IL_E4;
				case 5:
					if (playNow)
					{
						num = 6;
						continue;
					}
					return;
				case 6:
					this.eval_f = new LoLLauncher(r);
					this.eval_k();
					num = 2;
					continue;
				}
				IL_3F:
				if (!global::eval_a.eval_d())
				{
					num = 0;
					continue;
				}
				goto IL_54;
				IL_A5:
				num = 5;
				continue;
				IL_54:
				r.doneEvent += new MainWindow.RecordDoneDelegate(this.RecordDone);
				r.infoDoneEvent += new MainWindow.RecordInfoDoneDelegate(this.RecordInfoDone);
				System.Threading.ThreadStart start = new System.Threading.ThreadStart(r.startRecording);
				System.Threading.Thread thread = new System.Threading.Thread(start);
				this.eval_c.Add(r, thread);
				thread.Start();
				this.eval_n();
				goto IL_A5;
			}
			IL_E4:
			if (0 != 0)
			{
			}
			int arg_EF_0 = 0;
		}
		public void PlayGame(GameInfo info)
		{
			int num = 3;
			while (true)
			{
				switch (num)
				{
				case 0:
					goto IL_91;
				case 1:
					num = 2;
					continue;
				case 2:
					if (1 != 0)
					{
					}
					if (!global::eval_a.eval_b())
					{
						num = 0;
						continue;
					}
					goto IL_93;
				case 3:
					switch ((1504531534 == 1504531534) ? 1 : 0)
					{
					case 0:
					case 2:
						return;
					case 1:
					{
						IL_28:
						int arg_2C_0 = 0;
						if (0 != 0)
						{
						}
						switch (0)
						{
						case 0:
							goto IL_5B;
						}
						continue;
					}
					}
					goto IL_28;
				}
				IL_5B:
				if (global::eval_a.eval_d())
				{
					goto IL_93;
				}
				num = 1;
			}
			return;
			IL_91:
			return;
			IL_93:
			this.eval_f = new LoLLauncher(info);
			this.eval_k();
		}
		private void eval_g()
		{
			if (1 != 0)
			{
			}
			switch ((163950454 == 163950454) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_28:
				int arg_2C_0 = 0;
				goto IL_35;
			}
			case 1:
				goto IL_35;
			}
			goto IL_28;
			IL_35:
			if (0 != 0)
			{
			}
		}
		private void eval_b(object A_0, System.EventArgs A_1)
		{
			switch ((1424029514 == 1424029514) ? 1 : 0)
			{
			case 0:
			case 2:
				return;
			case 1:
			{
				IL_1F:
				int arg_23_0 = 0;
				if (0 != 0)
				{
				}
				bool flag = false;
				try
				{
					switch (0)
					{
					case 0:
						goto IL_92;
					}
					int num;
					while (true)
					{
						IL_40:
						switch (num)
						{
						case 0:
							num = 13;
							continue;
						case 1:
							this.eval_o();
							num = 6;
							continue;
						case 2:
							if (global::eval_a.eval_d())
							{
								num = 12;
								continue;
							}
							num = 4;
							continue;
						case 3:
							num = 7;
							continue;
						case 4:
							if (this.eval_b)
							{
								num = 0;
								continue;
							}
							goto IL_20F;
						case 5:
							if (!this.eval_b)
							{
								num = 14;
								continue;
							}
							goto IL_20F;
						case 6:
							goto IL_161;
						case 7:
							if (1 != 0)
							{
							}
							if (!this.eval_e)
							{
								num = 10;
								continue;
							}
							goto IL_143;
						case 8:
							goto IL_20F;
						case 9:
							goto IL_21B;
						case 10:
							this.eval_p();
							num = 18;
							continue;
						case 11:
							num = 15;
							continue;
						case 12:
							num = 5;
							continue;
						case 13:
							if (base.Visibility == Visibility.Hidden)
							{
								num = 3;
								continue;
							}
							goto IL_143;
						case 14:
							num = 17;
							continue;
						case 15:
							if (!this.eval_e)
							{
								num = 1;
								continue;
							}
							goto IL_161;
						case 16:
							goto IL_20F;
						case 17:
							if (base.Visibility == Visibility.Visible)
							{
								num = 11;
								continue;
							}
							goto IL_161;
						case 18:
							goto IL_143;
						}
						goto IL_92;
						IL_143:
						this.eval_g();
						this.eval_b = false;
						num = 16;
						continue;
						IL_161:
						this.eval_i();
						this.eval_b = true;
						num = 8;
						continue;
						IL_20F:
						num = 9;
					}
					IL_21B:
					return;
					IL_92:
					object obj;
					System.Threading.Monitor.Enter(obj = this.eval_l, ref flag);
					num = 2;
					goto IL_40;
				}
				finally
				{
					int num = 2;
					while (true)
					{
						switch (num)
						{
						case 0:
							goto IL_25F;
						case 1:
						{
							object obj;
							System.Threading.Monitor.Exit(obj);
							num = 0;
							continue;
						}
						case 2:
							switch (0)
							{
							case 0:
								goto IL_242;
							}
							continue;
						}
						IL_242:
						if (!flag)
						{
							break;
						}
						num = 1;
					}
					IL_25F:;
				}
				return;
			}
			}
			goto IL_1F;
		}
		private void eval_f()
		{
			switch ((1987177831 == 1987177831) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			int arg_23_0 = 0;
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			new System.Threading.Thread(new System.Threading.ThreadStart(this.eval_d)).Start();
		}
		private void eval_e()
		{
			switch ((1615452199 == 1615452199) ? 1 : 0)
			{
			case 0:
			case 2:
				goto IL_D7;
			case 1:
			{
				IL_1F:
				if (0 != 0)
				{
				}
				int num = 2;
				while (true)
				{
					switch (num)
					{
					case 0:
						goto IL_D7;
					case 1:
						this.eval_m = new FileSystemWatcher();
						this.eval_m.Filter = "*.lpr";
						this.eval_m.Renamed += new RenamedEventHandler(this.eval_a);
						this.eval_m.Deleted += new FileSystemEventHandler(this.eval_a);
						this.eval_m.Created += new FileSystemEventHandler(this.eval_b);
						num = 0;
						continue;
					case 2:
						switch (0)
						{
						case 0:
							goto IL_4B;
						}
						continue;
					}
					IL_4B:
					if (1 != 0)
					{
					}
					if (this.eval_m != null)
					{
						goto IL_E5;
					}
					num = 1;
				}
				break;
			}
			}
			goto IL_1F;
			IL_D7:
			int arg_DB_0 = 0;
			IL_E5:
			this.eval_m.Path = BaronReplays.Properties.Settings.Default.ReplayDir;
			this.eval_m.EnableRaisingEvents = true;
		}
		private void eval_b(object A_0, FileSystemEventArgs A_1)
		{
			int num = 1;
			while (true)
			{
				long length;
				long num2;
				switch (num)
				{
				case 0:
					goto IL_83;
				case 1:
					switch (0)
					{
					case 0:
						goto IL_31;
					}
					continue;
				case 2:
					goto IL_83;
				case 3:
					if (length != num2)
					{
						num = 5;
						continue;
					}
					goto IL_E5;
				case 4:
					goto IL_81;
				case 5:
					num2 = length;
					num = 2;
					continue;
				}
				IL_31:
				if (this.eval_k)
				{
					if (1 != 0)
					{
					}
					switch ((2081468949 == 2081468949) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_BF;
					case 1:
					{
						IL_65:
						int arg_69_0 = 0;
						if (0 != 0)
						{
						}
						num = 4;
						continue;
					}
					}
					goto IL_65;
				}
				num2 = 0L;
				IL_BF:
				num = 0;
				continue;
				IL_83:
				System.Threading.Thread.Sleep(500);
				length = new System.IO.FileInfo(A_1.FullPath).Length;
				num = 3;
			}
			IL_81:
			this.eval_k = false;
			return;
			IL_E5:
			this.AddRecord(A_1.FullPath, true);
		}
		private void eval_a(object A_0, FileSystemEventArgs A_1)
		{
			switch (0)
			{
			case 0:
				goto IL_46;
			}
			int num;
			SimpleLoLRecord simpleLoLRecord;
			System.Collections.Generic.List<SimpleLoLRecord>.Enumerator enumerator;
			while (true)
			{
				IL_0A:
				switch ((671653451 == 671653451) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_46;
				case 1:
				{
					IL_29:
					if (0 != 0)
					{
					}
					switch (num)
					{
					case 0:
						if (simpleLoLRecord != null)
						{
							num = 2;
							continue;
						}
						return;
					case 1:
						try
						{
							num = 5;
							while (true)
							{
								switch (num)
								{
								case 0:
								{
									SimpleLoLRecord current;
									if (A_1.FullPath.CompareTo(current.FileName) == 0)
									{
										num = 2;
										continue;
									}
									break;
								}
								case 1:
									goto IL_116;
								case 2:
								{
									SimpleLoLRecord current;
									simpleLoLRecord = current;
									num = 4;
									continue;
								}
								case 3:
								{
									if (!enumerator.MoveNext())
									{
										num = 1;
										continue;
									}
									SimpleLoLRecord current = enumerator.Current;
									num = 0;
									continue;
								}
								case 4:
									goto IL_116;
								case 5:
									switch (0)
									{
									case 0:
										goto IL_BB;
									}
									continue;
								case 6:
									goto IL_122;
								}
								IL_EC:
								num = 3;
								continue;
								IL_BB:
								goto IL_EC;
								IL_116:
								num = 6;
							}
							IL_122:
							goto IL_5F;
						}
						finally
						{
							((System.IDisposable)enumerator).Dispose();
						}
						goto IL_135;
						IL_5F:
						if (1 != 0)
						{
						}
						num = 0;
						continue;
					case 2:
						goto IL_135;
					case 3:
						return;
					}
					goto IL_46;
					IL_135:
					int arg_139_0 = 0;
					this.Records.Remove(simpleLoLRecord);
					this.UpdateRecordList();
					num = 3;
					continue;
				}
				}
				goto IL_29;
			}
			return;
			IL_46:
			simpleLoLRecord = null;
			enumerator = this.Records.GetEnumerator();
			num = 1;
			goto IL_0A;
		}
		private void eval_a(object A_0, RenamedEventArgs A_1)
		{
			switch ((1271384017 == 1271384017) ? 1 : 0)
			{
			case 0:
			case 2:
				return;
			case 1:
			{
				IL_1F:
				int arg_23_0 = 0;
				if (0 != 0)
				{
				}
				using (System.Collections.Generic.List<SimpleLoLRecord>.Enumerator enumerator = this.Records.GetEnumerator())
				{
					int num = 4;
					while (true)
					{
						switch (num)
						{
						case 0:
							goto IL_E7;
						case 1:
							goto IL_F3;
						case 2:
							goto IL_E7;
						case 3:
						{
							SimpleLoLRecord current;
							if (A_1.OldFullPath.CompareTo(current.FileName) == 0)
							{
								num = 5;
								continue;
							}
							break;
						}
						case 4:
							switch (0)
							{
							case 0:
								goto IL_75;
							}
							continue;
						case 5:
						{
							SimpleLoLRecord current;
							current.FileName = A_1.FullPath;
							num = 2;
							continue;
						}
						case 6:
						{
							if (!enumerator.MoveNext())
							{
								num = 0;
								continue;
							}
							SimpleLoLRecord current = enumerator.Current;
							num = 3;
							continue;
						}
						}
						IL_A6:
						if (1 != 0)
						{
						}
						num = 6;
						continue;
						IL_75:
						goto IL_A6;
						IL_E7:
						num = 1;
					}
					IL_F3:;
				}
				return;
			}
			}
			goto IL_1F;
		}
		private void eval_d()
		{
			switch (0)
			{
			case 0:
				goto IL_28;
			}
			int num;
			System.IO.DirectoryInfo directoryInfo;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
				{
					int num2;
					if (num2 < 0)
					{
						num = 3;
						continue;
					}
					System.IO.FileInfo[] array;
					this.AddRecord(array[num2].FullName, false);
					num2--;
					num = 4;
					continue;
				}
				case 1:
					switch ((1757207690 == 1757207690) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_F4;
					case 1:
					{
						IL_71:
						if (0 != 0)
						{
						}
						int arg_7C_0 = 0;
						System.IO.FileInfo[] array;
						try
						{
							System.Collections.Generic.IEnumerable<System.IO.FileInfo> arg_E4_0 = directoryInfo.GetFiles("*.lpr");
							if (MainWindow.eval_ah == null)
							{
								MainWindow.eval_ah = new Func<System.IO.FileInfo, System.DateTime>(MainWindow.eval_a);
							}
							array = arg_E4_0.OrderByDescending(MainWindow.eval_ah).ToArray<System.IO.FileInfo>();
							goto IL_A4;
						}
						catch (System.Exception)
						{
							return;
						}
						goto IL_F4;
						IL_A4:
						int num2 = array.Length - 1;
						num = 5;
						continue;
					}
					}
					goto IL_71;
				case 2:
					return;
				case 3:
					goto IL_F4;
				case 4:
					if (1 != 0)
					{
					}
					goto IL_86;
				case 5:
					goto IL_86;
				}
				goto IL_28;
				IL_86:
				num = 0;
				continue;
				IL_F4:
				this.UpdateRecordList();
				num = 2;
			}
			return;
			IL_28:
			this.eval_e();
			this.Records.Clear();
			directoryInfo = new System.IO.DirectoryInfo(BaronReplays.Properties.Settings.Default.ReplayDir);
			num = 1;
			goto IL_0A;
		}
		public void AddRecord(LoLRecord record)
		{
			while (true)
			{
				IL_00:
				if (1 != 0)
				{
				}
				switch (0)
				{
				case 0:
					goto IL_4C;
				}
				int num;
				SimpleLoLRecord simpleLoLRecord;
				while (true)
				{
					IL_14:
					switch ((534154468 == 534154468) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_00;
					case 1:
						IL_33:
						if (0 != 0)
						{
						}
						switch (num)
						{
						case 0:
							goto IL_B3;
						case 1:
							if (simpleLoLRecord != null)
							{
								int arg_63_0 = 0;
								num = 2;
								continue;
							}
							goto IL_B5;
						case 2:
							simpleLoLRecord.WatchClickEvent += new MainWindow.RecordDelegate(this.PlayRecord);
							simpleLoLRecord.DeleteClickEvent += new MainWindow.RecordDelegate(this.DeleteRecord);
							this.Records.Insert(0, simpleLoLRecord);
							num = 0;
							continue;
						}
						goto IL_4C;
					}
					goto IL_33;
				}
				IL_4C:
				simpleLoLRecord = LoLRecord.GetSimpleLoLRecord(record);
				num = 1;
				goto IL_14;
			}
			IL_B3:
			IL_B5:
			this.UpdateRecordList();
		}
		public void AddRecord(string path, bool refreshList)
		{
			switch (0)
			{
			case 0:
				goto IL_28;
			}
			int num;
			SimpleLoLRecord simpleLoLRecord;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					if (simpleLoLRecord != null)
					{
						goto IL_3B;
					}
					goto IL_63;
				case 1:
					switch ((224920578 == 224920578) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_3B;
					case 1:
						IL_D6:
						if (0 != 0)
						{
						}
						goto IL_63;
					}
					goto IL_D6;
				case 2:
				{
					this.UpdateRecordList();
					int arg_50_0 = 0;
					num = 5;
					continue;
				}
				case 3:
					simpleLoLRecord.WatchClickEvent += new MainWindow.RecordDelegate(this.PlayRecord);
					simpleLoLRecord.DeleteClickEvent += new MainWindow.RecordDelegate(this.DeleteRecord);
					this.Records.Insert(0, simpleLoLRecord);
					num = 1;
					continue;
				case 4:
					if (refreshList)
					{
						num = 2;
						continue;
					}
					goto IL_DF;
				case 5:
					goto IL_61;
				}
				goto IL_28;
				IL_3B:
				num = 3;
				continue;
				IL_63:
				num = 4;
			}
			IL_61:
			IL_DF:
			if (1 != 0)
			{
			}
			return;
			IL_28:
			simpleLoLRecord = LoLRecord.GetSimpleLoLRecord(path);
			num = 0;
			goto IL_0A;
		}
		public void UpdateRecordList()
		{
			int arg_04_0 = 0;
			switch ((597336943 == 597336943) ? 1 : 0)
			{
			case 0:
			case 2:
				goto IL_49;
			case 1:
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				if (!base.Dispatcher.CheckAccess())
				{
					goto IL_49;
				}
				this.eval_aa.Items.Refresh();
				return;
			}
			goto IL_2B;
			IL_49:
			Action method = new Action(this.eval_aa.Items.Refresh);
			base.Dispatcher.Invoke(method, new object[0]);
		}
		private void eval_a(object A_0, CancelEventArgs A_1)
		{
			int num = 2;
			while (true)
			{
				switch (num)
				{
				case 0:
					goto IL_178;
				case 1:
					goto IL_173;
				case 2:
					switch (0)
					{
					case 0:
						goto IL_59;
					}
					continue;
				case 3:
				{
					int arg_1C3_0 = 0;
					A_1.Cancel = true;
					num = 7;
					continue;
				}
				case 4:
					if (this.eval_j != null)
					{
						num = 8;
						continue;
					}
					goto IL_1D8;
				case 5:
					if (!A_1.Cancel)
					{
						num = 10;
						continue;
					}
					goto IL_1D8;
				case 6:
					if (!this.CheckRamainRecorder())
					{
						num = 3;
						continue;
					}
					goto IL_F0;
				case 7:
					goto IL_F0;
				case 8:
					this.eval_j.eval_c();
					num = 9;
					continue;
				case 9:
					goto IL_152;
				case 10:
					this.eval_d.Visible = false;
					num = 4;
					continue;
				case 11:
				{
					int num2;
					if (num2 == 1)
					{
						num = 1;
						continue;
					}
					goto IL_19C;
				}
				case 12:
					if (!this.eval_n)
					{
						num = 0;
						continue;
					}
					goto IL_19C;
				case 13:
				{
					int num2;
					if (num2 == 0)
					{
						num = 15;
						continue;
					}
					num = 11;
					continue;
				}
				case 14:
					return;
				case 15:
					goto IL_19A;
				}
				IL_59:
				if (!this.eval_h)
				{
					num = 14;
					continue;
				}
				num = 12;
				continue;
				IL_F0:
				switch ((395613672 == 395613672) ? 1 : 0)
				{
				case 0:
				case 2:
				{
					IL_178:
					int num2 = this.eval_c();
					num = 13;
					continue;
				}
				case 1:
					IL_10F:
					if (0 != 0)
					{
					}
					num = 5;
					continue;
				}
				goto IL_10F;
				IL_19C:
				num = 6;
			}
			return;
			IL_152:
			goto IL_1D8;
			IL_173:
			A_1.Cancel = true;
			this.eval_e = true;
			this.eval_o();
			return;
			IL_19A:
			A_1.Cancel = true;
			return;
			IL_1D8:
			if (1 != 0)
			{
			}
			this.eval_n = false;
		}
		private int eval_c()
		{
			int num = 3;
			while (true)
			{
				MessageBoxResult messageBoxResult;
				switch (num)
				{
				case 0:
					if (messageBoxResult == MessageBoxResult.Cancel)
					{
						num = 4;
						continue;
					}
					return 2;
				case 1:
					return 1;
				case 2:
					if (messageBoxResult == MessageBoxResult.Yes)
					{
						num = 1;
						continue;
					}
					num = 0;
					continue;
				case 3:
					if (1 != 0)
					{
					}
					switch (0)
					{
					case 0:
						goto IL_3B;
					}
					continue;
				case 4:
					return 0;
				case 5:
					return 1;
				}
				IL_3B:
				if (BaronReplays.Properties.Settings.Default.AlwaysTaskbar)
				{
					switch ((1444998083 == 1444998083) ? 1 : 0)
					{
					case 0:
					case 2:
						return 0;
					case 1:
					{
						IL_66:
						int arg_6A_0 = 0;
						if (0 != 0)
						{
						}
						num = 5;
						continue;
					}
					}
					goto IL_66;
				}
				messageBoxResult = System.Windows.MessageBox.Show(Utilities.GetString("ShrinkToTaskBarQuestion"), Utilities.GetString("BR"), MessageBoxButton.YesNoCancel);
				num = 2;
			}
			return 1;
		}
		public bool CheckRamainRecorder()
		{
			int num = 4;
			while (true)
			{
				System.Collections.Generic.Dictionary<LoLRecorder, System.Threading.Thread>.Enumerator enumerator;
				switch (num)
				{
				case 0:
					try
					{
						num = 2;
						while (true)
						{
							switch (num)
							{
							case 1:
								goto IL_C2;
							case 2:
								switch (0)
								{
								case 0:
									goto IL_7B;
								}
								continue;
							case 3:
								num = 1;
								continue;
							case 4:
							{
								if (!enumerator.MoveNext())
								{
									num = 3;
									continue;
								}
								System.Collections.Generic.KeyValuePair<LoLRecorder, System.Threading.Thread> current = enumerator.Current;
								current.Value.Abort();
								num = 0;
								continue;
							}
							}
							IL_9C:
							num = 4;
							continue;
							IL_7B:
							goto IL_9C;
						}
						IL_C2:
						return true;
					}
					finally
					{
						((System.IDisposable)enumerator).Dispose();
					}
					goto IL_D2;
				case 1:
					goto IL_1A3;
				case 2:
					if (System.Windows.MessageBox.Show(string.Format(Utilities.GetString("RemainGame"), this.eval_c.Count), Utilities.GetString("Sure"), MessageBoxButton.OKCancel) != MessageBoxResult.Cancel)
					{
						switch ((952064905 == 952064905) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_1A3;
						case 1:
						{
							IL_184:
							if (0 != 0)
							{
							}
							int arg_18F_0 = 0;
							num = 1;
							continue;
						}
						}
						goto IL_184;
					}
					return false;
				case 3:
					return true;
				case 4:
					switch (0)
					{
					case 0:
						goto IL_35;
					}
					continue;
				case 5:
					return true;
				case 6:
					if (1 != 0)
					{
					}
					if (this.eval_c.Count == 0)
					{
						num = 3;
						continue;
					}
					num = 2;
					continue;
				}
				IL_35:
				if (this.eval_c == null)
				{
					num = 5;
					continue;
				}
				num = 6;
				continue;
				IL_D2:
				enumerator = this.eval_c.GetEnumerator();
				num = 0;
				continue;
				IL_1A3:
				goto IL_D2;
			}
			return true;
		}
		private void eval_a(object A_0, MouseButtonEventArgs A_1)
		{
			if (1 != 0)
			{
			}
			switch ((1876708246 == 1876708246) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_28:
				break;
			case 1:
				goto IL_29;
			}
			goto IL_28;
			IL_29:
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			this.eval_e(this, null);
		}
		private void eval_a(UIElement A_0)
		{
			switch (0)
			{
			case 0:
				goto IL_42;
			}
			int num;
			System.Type type;
			while (true)
			{
				IL_0A:
				switch ((1032665134 == 1032665134) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_92;
				case 1:
					IL_29:
					if (0 != 0)
					{
					}
					switch (num)
					{
					case 0:
						return;
					case 1:
						if (type.Name.CompareTo("RecordDetail") != 0)
						{
							num = 2;
							continue;
						}
						return;
					case 2:
						goto IL_92;
					}
					goto IL_42;
				}
				goto IL_29;
				IL_92:
				if (1 != 0)
				{
				}
				this.eval_aa.SelectedItem = null;
				int arg_AE_0 = 0;
				num = 0;
			}
			return;
			IL_42:
			this.eval_ab.Children.Clear();
			this.eval_ab.Children.Add(A_0);
			type = A_0.GetType();
			num = 1;
			goto IL_0A;
		}
		private void eval_a(object A_0, SelectionChangedEventArgs A_1)
		{
			if (this.eval_aa.SelectedItem != null)
			{
				switch ((1023700334 == 1023700334) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_1B;
				case 1:
				{
					IL_45:
					if (0 != 0)
					{
					}
					LoLRecord loLRecord = new LoLRecord();
					loLRecord.readFromFile((this.eval_aa.SelectedItem as SimpleLoLRecord).FileName, false);
					this.eval_a(new RecordDetail
					{
						DataContext = loLRecord.gameStats
					});
					return;
				}
				}
				goto IL_45;
			}
			int arg_11_0 = 0;
			IL_1B:
			if (1 != 0)
			{
			}
		}
		private void eval_k(object A_0, RoutedEventArgs A_1)
		{
			if (1 != 0)
			{
			}
			switch ((1805163553 == 1805163553) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_28:
				break;
			case 1:
				goto IL_29;
			}
			goto IL_28;
			IL_29:
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			Settings settings = new Settings();
			settings.ReplayPathChanged += new MainWindow.LoadRecordsDelegate(this.eval_f);
			this.eval_a(settings);
		}
		private void eval_j(object A_0, RoutedEventArgs A_1)
		{
			switch ((1248954616 == 1248954616) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			FeaturedGamesView featuredGamesView = new FeaturedGamesView();
			featuredGamesView.FeatureGameClicked += new MainWindow.StartNewRecodingDelegate(this.StartNewRecoding);
			featuredGamesView.FeatureGamePlayOnlyClicked += new MainWindow.PlayGameDelegate(this.PlayGame);
			this.eval_a(featuredGamesView);
		}
		private void eval_i(object A_0, RoutedEventArgs A_1)
		{
			switch ((226240057 == 226240057) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			int arg_34_0 = 0;
			this.eval_ac.Navigate("http://ahri.tw/News/br.php");
			this.eval_a(this.eval_ac);
		}
		private void eval_h(object A_0, RoutedEventArgs A_1)
		{
			switch ((270981224 == 270981224) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			Process.Start("http://ahri.tw/");
		}
		private void eval_g(object A_0, RoutedEventArgs A_1)
		{
			switch (0)
			{
			case 0:
				goto IL_28;
			}
			int num;
			bool? flag;
			Microsoft.Win32.OpenFileDialog openFileDialog;
			while (true)
			{
				IL_0A:
				bool arg_C6_0;
				switch (num)
				{
				case 0:
					arg_C6_0 = flag.HasValue;
					goto IL_C6;
				case 1:
					return;
				case 2:
					goto IL_78;
				case 3:
					this.eval_f = new LoLLauncher(openFileDialog.FileName);
					this.eval_k();
					num = 1;
					continue;
				case 4:
					num = 0;
					continue;
				case 5:
					switch ((460729241 == 460729241) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_78;
					case 1:
						IL_101:
						if (0 != 0)
						{
						}
						arg_C6_0 = false;
						goto IL_C6;
					}
					goto IL_101;
				}
				goto IL_28;
				IL_78:
				if (flag.GetValueOrDefault())
				{
					num = 4;
					continue;
				}
				num = 5;
				continue;
				IL_C6:
				if (!arg_C6_0)
				{
					break;
				}
				num = 3;
			}
			return;
			IL_28:
			openFileDialog = new Microsoft.Win32.OpenFileDialog();
			openFileDialog.InitialDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
			openFileDialog.DefaultExt = ".lpr";
			openFileDialog.Filter = "BaronReplays Files (*.lpr)|*.lpr";
			bool? flag2 = openFileDialog.ShowDialog();
			flag = flag2;
			int arg_5D_0 = 0;
			if (1 != 0)
			{
			}
			num = 2;
			goto IL_0A;
		}
		private void eval_f(object A_0, RoutedEventArgs A_1)
		{
			switch ((128038747 == 128038747) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			int arg_34_0 = 0;
			new About
			{
				Owner = this
			}.ShowDialog();
		}
		private void eval_e(object A_0, RoutedEventArgs A_1)
		{
			int num = 7;
			while (true)
			{
				switch (num)
				{
				case 0:
					return;
				case 1:
				{
					bool? isSuccess;
					if (isSuccess.Value)
					{
						num = 2;
						continue;
					}
					return;
				}
				case 2:
					goto IL_CA;
				case 3:
				{
					DownloadProgress downloadProgress = new DownloadProgress("http://Ahri.tw/BaronReplays/BaronReplays_Auto.exe", "BaronReplays_Auto.exe");
					downloadProgress.Owner = this;
					downloadProgress.ShowDialog();
					bool? isSuccess2 = downloadProgress.IsSuccess;
					num = 5;
					continue;
				}
				case 4:
				{
					DownloadProgress downloadProgress;
					bool? isSuccess = downloadProgress.IsSuccess;
					num = 1;
					continue;
				}
				case 5:
				{
					bool? isSuccess2;
					if (isSuccess2.HasValue)
					{
						num = 4;
						continue;
					}
					return;
				}
				case 6:
					if (System.Windows.MessageBox.Show(this, Utilities.GetString("AskUpdate"), Utilities.GetString("BR"), MessageBoxButton.OKCancel) != MessageBoxResult.Cancel)
					{
						num = 3;
						continue;
					}
					return;
				case 7:
					switch (0)
					{
					case 0:
						goto IL_3D;
					}
					continue;
				case 8:
					num = 6;
					continue;
				}
				IL_3D:
				if (Utilities.IsUpdateAvailable())
				{
					int arg_4B_0 = 0;
					num = 8;
				}
				else
				{
					System.Windows.MessageBox.Show(this, Utilities.GetString("NewestVersion"), Utilities.GetString("BR"));
					num = 0;
				}
			}
			while (true)
			{
				IL_CA:
				if (1 != 0)
				{
				}
				switch ((1895448593 == 1895448593) ? 1 : 0)
				{
				case 0:
				case 2:
					continue;
				}
				break;
			}
			if (0 != 0)
			{
			}
			Process.Start(System.AppDomain.CurrentDomain.BaseDirectory + "\\BaronReplays_AutoUpdate.exe");
		}
		private void eval_a(string A_0, bool A_1 = false)
		{
			int num = 1;
			while (true)
			{
				switch (num)
				{
				case 0:
					goto IL_43;
				case 1:
					goto IL_09;
				case 2:
					if (!A_1)
					{
						int arg_7C_0 = 0;
						num = 4;
						continue;
					}
					return;
				case 3:
					return;
				case 4:
					switch ((1195998096 == 1195998096) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_09;
					case 1:
						IL_AC:
						if (0 != 0)
						{
						}
						System.Windows.MessageBox.Show(this, A_0, Utilities.GetString("BR"));
						if (1 != 0)
						{
						}
						num = 3;
						continue;
					}
					goto IL_AC;
				}
				goto IL_2D;
				IL_09:
				switch (0)
				{
				case 0:
					IL_2D:
					if (this.eval_d.Visible)
					{
						num = 0;
					}
					else
					{
						num = 2;
					}
					break;
				}
			}
			IL_43:
			this.eval_d.ShowBalloonTip(10000, Utilities.GetString("BR"), A_0, ToolTipIcon.None);
		}
		private void eval_b()
		{
			switch ((338084955 == 338084955) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			int arg_34_0 = 0;
			this.eval_v.IsChecked = false;
			this.eval_u.IsChecked = false;
		}
		private void eval_d(object A_0, RoutedEventArgs A_1)
		{
			switch ((1393629775 == 1393629775) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			LanguageControl.ChangeLanguage("zh-TW");
			this.eval_a();
		}
		private void eval_c(object A_0, RoutedEventArgs A_1)
		{
			switch ((1949498183 == 1949498183) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			LanguageControl.ChangeLanguage("en-US");
			this.eval_a();
		}
		private void eval_a()
		{
			switch (0)
			{
			case 0:
				goto IL_4A;
			}
			int num;
			string strA;
			while (true)
			{
				IL_0A:
				switch ((1152409636 == 1152409636) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_D3;
				case 1:
					IL_29:
					if (0 != 0)
					{
					}
					switch (num)
					{
					case 0:
						if (string.Compare(strA, "en-US") == 0)
						{
							num = 1;
							continue;
						}
						return;
					case 1:
						this.eval_v.IsChecked = true;
						num = 4;
						continue;
					case 2:
						if (string.Compare(strA, "zh-TW") == 0)
						{
							num = 3;
							continue;
						}
						num = 0;
						continue;
					case 3:
						goto IL_84;
					case 4:
						goto IL_9E;
					}
					goto IL_4A;
				}
				goto IL_29;
			}
			IL_84:
			goto IL_D3;
			IL_9E:
			int arg_A2_0 = 0;
			return;
			IL_D3:
			this.eval_u.IsChecked = true;
			return;
			IL_4A:
			if (1 != 0)
			{
			}
			this.eval_b();
			strA = BaronReplays.Properties.Settings.Default.Language;
			num = 2;
			goto IL_0A;
		}
		private void eval_b(object A_0, RoutedEventArgs A_1)
		{
			switch (0)
			{
			case 0:
				goto IL_34;
			}
			int num;
			LoLCommandAnalyzer loLCommandAnalyzer;
			OneLineWindow oneLineWindow;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
				{
					MessageBoxResult messageBoxResult;
					if (messageBoxResult == MessageBoxResult.No)
					{
						num = 5;
						continue;
					}
					return;
				}
				case 1:
					goto IL_131;
				case 2:
					if (loLCommandAnalyzer.IsSuccess)
					{
						num = 7;
						continue;
					}
					System.Windows.MessageBox.Show(this, Utilities.GetString("UselessCommand"), Utilities.GetString("BR"));
					num = 1;
					continue;
				case 3:
				{
					MessageBoxResult messageBoxResult;
					if (messageBoxResult == MessageBoxResult.Yes)
					{
						num = 8;
						continue;
					}
					goto IL_16F;
				}
				case 4:
					if (1 != 0)
					{
					}
					loLCommandAnalyzer = new LoLCommandAnalyzer(oneLineWindow.DesireString);
					num = 2;
					continue;
				case 5:
					goto IL_18B;
				case 6:
					if (oneLineWindow.DesireString != null)
					{
						num = 4;
						continue;
					}
					return;
				case 7:
				{
					MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(Utilities.GetString("WatchNow"), Utilities.GetString("BR"), MessageBoxButton.YesNo);
					num = 3;
					continue;
				}
				case 8:
					goto IL_16A;
				}
				goto IL_34;
			}
			IL_131:
			return;
			IL_16A:
			this.StartNewRecoding(loLCommandAnalyzer.GetLoLRecoder(), true);
			return;
			IL_18B:
			this.StartNewRecoding(loLCommandAnalyzer.GetLoLRecoder(), false);
			return;
			IL_34:
			switch ((1569875370 == 1569875370) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_16F:
				num = 0;
				goto IL_0A;
			case 1:
				break;
			default:
			{
				int arg_56_0 = 0;
				break;
			}
			}
			if (0 != 0)
			{
			}
			oneLineWindow = new OneLineWindow();
			oneLineWindow.Title = Utilities.GetString("InputCommand");
			oneLineWindow.Owner = this;
			oneLineWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
			oneLineWindow.ShowDialog();
			num = 6;
			goto IL_0A;
		}
		private void eval_a(object A_0, RoutedEventArgs A_1)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_5A;
				}
				LoLCommandAnalyzer loLCommandAnalyzer;
				while (true)
				{
					IL_19:
					if (1 != 0)
					{
					}
					int arg_27_0 = 0;
					switch (num)
					{
					case 0:
					{
						Microsoft.Win32.OpenFileDialog openFileDialog;
						System.IO.StreamReader streamReader = new System.IO.StreamReader(openFileDialog.FileName);
						loLCommandAnalyzer = new LoLCommandAnalyzer(streamReader.ReadToEnd());
						num = 8;
						continue;
					}
					case 1:
						goto IL_165;
					case 2:
					{
						MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(Utilities.GetString("WatchNow"), Utilities.GetString("BR"), MessageBoxButton.YesNo);
						num = 6;
						continue;
					}
					case 3:
					{
						MessageBoxResult messageBoxResult;
						if (messageBoxResult == MessageBoxResult.No)
						{
							num = 7;
							continue;
						}
						return;
					}
					case 4:
					{
						Microsoft.Win32.OpenFileDialog openFileDialog;
						if (openFileDialog.FileName != string.Empty)
						{
							num = 0;
							continue;
						}
						return;
					}
					case 5:
						goto IL_1A0;
					case 6:
					{
						MessageBoxResult messageBoxResult;
						if (messageBoxResult == MessageBoxResult.Yes)
						{
							num = 5;
							continue;
						}
						num = 3;
						continue;
					}
					case 7:
						goto IL_1C3;
					case 8:
						if (loLCommandAnalyzer.IsSuccess)
						{
							num = 2;
							continue;
						}
						goto IL_13D;
					}
					goto IL_5A;
				}
				IL_165:
				return;
				IL_1A0:
				this.StartNewRecoding(loLCommandAnalyzer.GetLoLRecoder(), true);
				return;
				IL_1C3:
				this.StartNewRecoding(loLCommandAnalyzer.GetLoLRecoder(), false);
				return;
				IL_5A:
				switch ((722646003 == 722646003) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_13D:
					System.Windows.MessageBox.Show(this, Utilities.GetString("UselessCommand"), Utilities.GetString("BR"));
					num = 1;
					goto IL_19;
				case 1:
				{
					IL_79:
					if (0 != 0)
					{
					}
					Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
					openFileDialog.Filter = "All files (*.*)|*.*";
					openFileDialog.Title = Utilities.GetString("SelectFile");
					openFileDialog.InitialDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
					openFileDialog.ShowDialog();
					num = 4;
					goto IL_19;
				}
				}
				goto IL_79;
			}
			}
			goto IL_0F;
		}
		private void eval_a(object A_0, System.EventArgs A_1)
		{
			switch ((847926875 == 847926875) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			int arg_34_0 = 0;
			Utilities.LogWriter.Close();
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), System.Diagnostics.DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!this.eval_ag)
			{
				if (1 != 0)
				{
				}
				switch ((1542513968 == 1542513968) ? 1 : 0)
				{
				case 0:
				case 2:
					return;
				case 1:
				{
					IL_40:
					if (0 != 0)
					{
					}
					this.eval_ag = true;
					Uri resourceLocator = new Uri("/BaronReplays;component/mainwindow.xaml", UriKind.Relative);
					System.Windows.Application.LoadComponent(this, resourceLocator);
					return;
				}
				}
				goto IL_40;
			}
			int arg_0C_0 = 0;
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), System.Diagnostics.DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (0)
			{
			case 0:
				goto IL_1C;
			}
			int num;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					goto IL_228;
				case 1:
					if (1 != 0)
					{
					}
					num = 0;
					continue;
				case 2:
					switch ((1999515012 == 1999515012) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					case 1:
						break;
					default:
					{
						int arg_49_0 = 0;
						break;
					}
					}
					if (0 != 0)
					{
					}
					switch (connectionId)
					{
					case 1:
						goto IL_251;
					case 2:
						goto IL_137;
					case 3:
						goto IL_106;
					case 4:
						goto IL_199;
					case 5:
						goto IL_22D;
					case 6:
						goto IL_2EC;
					case 7:
						goto IL_1EE;
					case 8:
						goto IL_297;
					case 9:
						goto IL_175;
					case 10:
						goto IL_E2;
					case 11:
						goto IL_310;
					case 12:
						goto IL_2C8;
					case 13:
						goto IL_15B;
					case 14:
						goto IL_BE;
					case 15:
						goto IL_2BB;
					case 16:
						goto IL_12A;
					case 17:
						goto IL_1BD;
					case 18:
						goto IL_168;
					case 19:
						goto IL_1CA;
					default:
						num = 1;
						continue;
					}
					break;
				}
				goto IL_1C;
			}
			IL_BE:
			this.eval_aa = (System.Windows.Controls.ListBox)target;
			this.eval_aa.SelectionChanged += new SelectionChangedEventHandler(this.eval_a);
			return;
			IL_E2:
			this.eval_w = (System.Windows.Controls.MenuItem)target;
			this.eval_w.Click += new RoutedEventHandler(this.eval_h);
			return;
			IL_106:
			this.eval_p = (System.Windows.Controls.MenuItem)target;
			this.eval_p.Click += new RoutedEventHandler(this.eval_a);
			return;
			IL_12A:
			this.eval_ac = (System.Windows.Controls.WebBrowser)target;
			return;
			IL_137:
			this.eval_o = (System.Windows.Controls.MenuItem)target;
			this.eval_o.Click += new RoutedEventHandler(this.eval_g);
			return;
			IL_15B:
			this.eval_z = (Grid)target;
			return;
			IL_168:
			this.eval_ae = (TextBlock)target;
			return;
			IL_175:
			this.eval_v = (System.Windows.Controls.MenuItem)target;
			this.eval_v.Click += new RoutedEventHandler(this.eval_c);
			return;
			IL_199:
			this.eval_q = (System.Windows.Controls.MenuItem)target;
			this.eval_q.Click += new RoutedEventHandler(this.eval_b);
			return;
			IL_1BD:
			this.eval_ad = (TextBlock)target;
			return;
			IL_1CA:
			this.eval_af = (TextBlock)target;
			this.eval_af.MouseLeftButtonDown += new MouseButtonEventHandler(this.eval_a);
			return;
			IL_1EE:
			this.eval_t = (System.Windows.Controls.MenuItem)target;
			this.eval_t.Click += new RoutedEventHandler(this.eval_e);
			return;
			IL_228:
			this.eval_ag = true;
			return;
			IL_22D:
			this.eval_r = (System.Windows.Controls.MenuItem)target;
			this.eval_r.Click += new RoutedEventHandler(this.eval_j);
			return;
			IL_251:
			((MainWindow)target).Closing += new CancelEventHandler(this.eval_a);
			((MainWindow)target).Loaded += new RoutedEventHandler(this.eval_l);
			((MainWindow)target).Closed += new System.EventHandler(this.eval_a);
			return;
			IL_297:
			this.eval_u = (System.Windows.Controls.MenuItem)target;
			this.eval_u.Click += new RoutedEventHandler(this.eval_d);
			return;
			IL_2BB:
			this.eval_ab = (Grid)target;
			return;
			IL_2C8:
			this.eval_y = (System.Windows.Controls.MenuItem)target;
			this.eval_y.Click += new RoutedEventHandler(this.eval_f);
			return;
			IL_2EC:
			this.eval_s = (System.Windows.Controls.MenuItem)target;
			this.eval_s.Click += new RoutedEventHandler(this.eval_k);
			return;
			IL_310:
			this.eval_x = (System.Windows.Controls.MenuItem)target;
			this.eval_x.Click += new RoutedEventHandler(this.eval_i);
			return;
			IL_1C:
			num = 2;
			goto IL_0A;
		}
		[System.Runtime.CompilerServices.CompilerGenerated]
		private static System.DateTime eval_a(System.IO.FileInfo A_0)
		{
			switch ((74528902 == 74528902) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			return A_0.LastWriteTime;
		}
	}
}
