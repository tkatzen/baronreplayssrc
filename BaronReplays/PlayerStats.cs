using FluorineFx;
using FluorineFx.AMF3;
using System;
namespace BaronReplays
{
	public class PlayerStats
	{
		protected string _skinName;
		protected bool _botPlayer;
		protected ulong _userId;
		protected ulong _gameId;
		protected string _summonerName;
		protected bool _leaver;
		protected uint _teamId;
		protected int _spell1Id;
		protected int _spell2Id;
		private DetailStats eval_a;
		public string SkinName
		{
			get
			{
				switch ((830726958 == 830726958) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return this._skinName;
			}
			set
			{
				switch ((936936560 == 936936560) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this._skinName = value;
			}
		}
		public bool BotPlayer
		{
			get
			{
				switch ((392328408 == 392328408) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return this._botPlayer;
			}
			set
			{
				switch ((502026530 == 502026530) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				this._botPlayer = value;
			}
		}
		public ulong UserId
		{
			get
			{
				switch ((611699856 == 611699856) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return this._userId;
			}
			set
			{
				switch ((784963436 == 784963436) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this._userId = value;
			}
		}
		public ulong GameId
		{
			get
			{
				switch ((688500635 == 688500635) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				return this._gameId;
			}
			set
			{
				switch ((483773023 == 483773023) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this._gameId = value;
			}
		}
		public string SummonerName
		{
			get
			{
				switch ((1232496642 == 1232496642) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return this._summonerName;
			}
			set
			{
				if (1 != 0)
				{
				}
				switch ((1269818904 == 1269818904) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_28:
					break;
				case 1:
					goto IL_29;
				}
				goto IL_28;
				IL_29:
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this._summonerName = value;
			}
		}
		public bool Leaver
		{
			get
			{
				switch ((253263240 == 253263240) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return this._leaver;
			}
			set
			{
				if (1 != 0)
				{
				}
				switch ((651905629 == 651905629) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_28:
					break;
				case 1:
					goto IL_29;
				}
				goto IL_28;
				IL_29:
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this._leaver = value;
			}
		}
		public uint TeamId
		{
			get
			{
				switch ((148513487 == 148513487) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				return this._teamId;
			}
			set
			{
				if (1 != 0)
				{
				}
				switch ((2017461023 == 2017461023) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_28:
					break;
				case 1:
					goto IL_29;
				}
				goto IL_28;
				IL_29:
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this._teamId = value;
			}
		}
		public int Spell1Id
		{
			get
			{
				switch ((845323196 == 845323196) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				return this._spell1Id;
			}
			set
			{
				if (1 != 0)
				{
				}
				switch ((1324872874 == 1324872874) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_28:
					break;
				case 1:
					goto IL_29;
				}
				goto IL_28;
				IL_29:
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this._spell1Id = value;
			}
		}
		public int Spell2Id
		{
			get
			{
				switch ((898646469 == 898646469) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				return this._spell2Id;
			}
			set
			{
				switch ((227093769 == 227093769) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				int arg_34_0 = 0;
				this._spell2Id = value;
			}
		}
		public DetailStats DetailStats
		{
			get
			{
				switch ((307548362 == 307548362) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				return this.eval_a;
			}
			set
			{
				switch ((863373822 == 863373822) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				int arg_34_0 = 0;
				this.eval_a = value;
			}
		}
		public PlayerStats()
		{
		}
		public PlayerStats(ASObject pStats)
		{
			this.SkinName = (pStats["skinName"] as string);
			this.BotPlayer = (bool)pStats["botPlayer"];
			this.UserId = ulong.Parse(pStats["userId"].ToString());
			this.GameId = ulong.Parse(pStats["gameId"].ToString());
			this.SummonerName = pStats["summonerName"].ToString();
			this.Leaver = (bool)pStats["leaver"];
			this.TeamId = uint.Parse(pStats["teamId"].ToString());
			this.Spell1Id = int.Parse(pStats["spell1Id"].ToString());
			this.Spell2Id = int.Parse(pStats["spell2Id"].ToString());
			this.DetailStats = new DetailStats(pStats["statistics"] as ArrayCollection);
		}
	}
}
