using BaronReplays.Properties;
using IWshRuntimeLibrary;
using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;
namespace BaronReplays
{
	public class Settings : System.Windows.Controls.UserControl, INotifyPropertyChanged, IComponentConnector
	{
		internal Settings SettingView;
		internal TriStateButton SelectLoLGameExeButton;
		internal System.Windows.Controls.TextBox ReplaySavePathTextbox;
		internal TriStateButton SelectSavePathButton;
		internal TriStateButton OpenFolderPathButton;
		internal System.Windows.Controls.TextBox FileNameFormatTextbox;
		internal TriStateButton FileNameFormatChangeButton;
		internal System.Windows.Controls.ComboBox RegionsComboBox;
		private bool _contentLoaded;
		public event PropertyChangedEventHandler PropertyChanged
		{
			add
			{
				int arg_04_0 = 0;
				switch (0)
				{
				case 0:
					goto IL_28;
				}
				int num;
				PropertyChangedEventHandler propertyChangedEventHandler;
				while (true)
				{
					IL_16:
					PropertyChangedEventHandler propertyChangedEventHandler2;
					switch (num)
					{
					case 0:
						return;
					case 1:
						goto IL_3A;
					case 2:
						switch ((208348724 == 208348724) ? 1 : 0)
						{
						case 0:
						case 2:
							return;
						case 1:
							IL_89:
							if (0 != 0)
							{
							}
							if (propertyChangedEventHandler == propertyChangedEventHandler2)
							{
								num = 0;
								continue;
							}
							goto IL_3A;
						}
						goto IL_89;
					}
					goto IL_28;
					IL_3A:
					if (1 != 0)
					{
					}
					propertyChangedEventHandler2 = propertyChangedEventHandler;
					PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)System.Delegate.Combine(propertyChangedEventHandler2, value);
					propertyChangedEventHandler = System.Threading.Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.PropertyChanged, value2, propertyChangedEventHandler2);
					num = 2;
				}
				return;
				IL_28:
				propertyChangedEventHandler = this.PropertyChanged;
				num = 1;
				goto IL_16;
			}
			remove
			{
				int arg_04_0 = 0;
				switch (0)
				{
				case 0:
					goto IL_32;
				}
				int num;
				PropertyChangedEventHandler propertyChangedEventHandler;
				while (true)
				{
					IL_16:
					if (1 != 0)
					{
					}
					PropertyChangedEventHandler propertyChangedEventHandler2;
					switch (num)
					{
					case 0:
						switch ((1611836218 == 1611836218) ? 1 : 0)
						{
						case 0:
						case 2:
							return;
						case 1:
							IL_89:
							if (0 != 0)
							{
							}
							if (propertyChangedEventHandler == propertyChangedEventHandler2)
							{
								num = 2;
								continue;
							}
							goto IL_44;
						}
						goto IL_89;
					case 1:
						goto IL_44;
					case 2:
						return;
					}
					goto IL_32;
					IL_44:
					propertyChangedEventHandler2 = propertyChangedEventHandler;
					PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)System.Delegate.Remove(propertyChangedEventHandler2, value);
					propertyChangedEventHandler = System.Threading.Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.PropertyChanged, value2, propertyChangedEventHandler2);
					num = 0;
				}
				return;
				IL_32:
				propertyChangedEventHandler = this.PropertyChanged;
				num = 1;
				goto IL_16;
			}
		}
		public event MainWindow.LoadRecordsDelegate ReplayPathChanged
		{
			add
			{
				int arg_04_0 = 0;
				switch (0)
				{
				case 0:
					goto IL_28;
				}
				int num;
				MainWindow.LoadRecordsDelegate loadRecordsDelegate;
				while (true)
				{
					IL_16:
					MainWindow.LoadRecordsDelegate loadRecordsDelegate2;
					switch (num)
					{
					case 0:
						switch ((405599390 == 405599390) ? 1 : 0)
						{
						case 0:
						case 2:
							return;
						case 1:
							IL_7F:
							if (1 != 0)
							{
							}
							if (0 != 0)
							{
							}
							if (loadRecordsDelegate == loadRecordsDelegate2)
							{
								num = 2;
								continue;
							}
							goto IL_3A;
						}
						goto IL_7F;
					case 1:
						goto IL_3A;
					case 2:
						return;
					}
					goto IL_28;
					IL_3A:
					loadRecordsDelegate2 = loadRecordsDelegate;
					MainWindow.LoadRecordsDelegate value2 = (MainWindow.LoadRecordsDelegate)System.Delegate.Combine(loadRecordsDelegate2, value);
					loadRecordsDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.LoadRecordsDelegate>(ref this.ReplayPathChanged, value2, loadRecordsDelegate2);
					num = 0;
				}
				return;
				IL_28:
				loadRecordsDelegate = this.ReplayPathChanged;
				num = 1;
				goto IL_16;
			}
			remove
			{
				int arg_04_0 = 0;
				switch (0)
				{
				case 0:
					goto IL_28;
				}
				int num;
				MainWindow.LoadRecordsDelegate loadRecordsDelegate;
				while (true)
				{
					IL_16:
					MainWindow.LoadRecordsDelegate loadRecordsDelegate2;
					switch (num)
					{
					case 0:
						goto IL_3A;
					case 1:
						switch ((2078676429 == 2078676429) ? 1 : 0)
						{
						case 0:
						case 2:
							return;
						case 1:
							IL_89:
							if (0 != 0)
							{
							}
							if (loadRecordsDelegate == loadRecordsDelegate2)
							{
								num = 2;
								continue;
							}
							goto IL_3A;
						}
						goto IL_89;
					case 2:
						return;
					}
					goto IL_28;
					IL_3A:
					if (1 != 0)
					{
					}
					loadRecordsDelegate2 = loadRecordsDelegate;
					MainWindow.LoadRecordsDelegate value2 = (MainWindow.LoadRecordsDelegate)System.Delegate.Remove(loadRecordsDelegate2, value);
					loadRecordsDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.LoadRecordsDelegate>(ref this.ReplayPathChanged, value2, loadRecordsDelegate2);
					num = 1;
				}
				return;
				IL_28:
				loadRecordsDelegate = this.ReplayPathChanged;
				num = 0;
				goto IL_16;
			}
		}
		public string LoLGameExe
		{
			get
			{
				while (BaronReplays.Properties.Settings.Default.LoLGameExe.Length == 0)
				{
					int arg_15_0 = 0;
					if (1 != 0)
					{
					}
					switch ((1137669680 == 1137669680) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					case 1:
						IL_48:
						if (0 != 0)
						{
						}
						return Utilities.GetString("PleasePlayFirst");
					}
					goto IL_48;
				}
				return BaronReplays.Properties.Settings.Default.LoLGameExe;
			}
		}
		public string ReplaySavePath
		{
			get
			{
				int arg_04_0 = 0;
				switch ((1117511733 == 1117511733) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				return BaronReplays.Properties.Settings.Default.ReplayDir;
			}
			set
			{
				int arg_04_0 = 0;
				switch (0)
				{
				case 0:
					goto IL_28;
				}
				int num;
				while (true)
				{
					IL_16:
					switch (num)
					{
					case 0:
						return;
					case 1:
						switch ((862777618 == 862777618) ? 1 : 0)
						{
						case 0:
						case 2:
							return;
						case 1:
							IL_83:
							if (0 != 0)
							{
							}
							if (1 != 0)
							{
							}
							this.ReplayPathChanged();
							num = 0;
							continue;
						}
						goto IL_83;
					case 2:
						if (this.ReplayPathChanged != null)
						{
							num = 1;
							continue;
						}
						return;
					}
					goto IL_28;
				}
				return;
				IL_28:
				BaronReplays.Properties.Settings.Default.ReplayDir = value;
				BaronReplays.Properties.Settings.Default.Save();
				this.eval_a("ReplaySavePath");
				num = 2;
				goto IL_16;
			}
		}
		public string FileNameFormat
		{
			get
			{
				int arg_04_0 = 0;
				if (1 != 0)
				{
				}
				switch ((839199971 == 839199971) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_34:
					break;
				case 1:
					goto IL_35;
				}
				goto IL_34;
				IL_35:
				if (0 != 0)
				{
				}
				return BaronReplays.Properties.Settings.Default.FileNameFormat;
			}
			set
			{
				int arg_04_0 = 0;
				switch ((1322330026 == 1322330026) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				BaronReplays.Properties.Settings.Default.FileNameFormat = value;
				BaronReplays.Properties.Settings.Default.Save();
				this.eval_a("FileNameFormat");
			}
		}
		public bool KillProcess
		{
			get
			{
				int arg_04_0 = 0;
				if (1 != 0)
				{
				}
				switch ((1275346667 == 1275346667) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_34:
					break;
				case 1:
					goto IL_35;
				}
				goto IL_34;
				IL_35:
				if (0 != 0)
				{
				}
				return BaronReplays.Properties.Settings.Default.KillProcess;
			}
			set
			{
				int arg_04_0 = 0;
				switch ((1744073994 == 1744073994) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				BaronReplays.Properties.Settings.Default.KillProcess = value;
				BaronReplays.Properties.Settings.Default.Save();
				this.eval_a("KillProcess");
			}
		}
		public bool AlwaysTaskbar
		{
			get
			{
				int arg_04_0 = 0;
				switch ((521365286 == 521365286) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				return BaronReplays.Properties.Settings.Default.AlwaysTaskbar;
			}
			set
			{
				int arg_04_0 = 0;
				switch ((1941878278 == 1941878278) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				BaronReplays.Properties.Settings.Default.AlwaysTaskbar = value;
				BaronReplays.Properties.Settings.Default.Save();
				this.eval_a("AlwaysTaskbar");
			}
		}
		public bool StartWithSystem
		{
			get
			{
				int arg_04_0 = 0;
				switch ((2029629895 == 2029629895) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				return BaronReplays.Properties.Settings.Default.StartWithSystem;
			}
			set
			{
				int arg_04_0 = 0;
				switch ((1680658478 == 1680658478) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				BaronReplays.Properties.Settings.Default.StartWithSystem = value;
				BaronReplays.Properties.Settings.Default.Save();
				this.eval_a("StartWithSystem");
				this.eval_a();
			}
		}
		public bool CreateShortCutOnDesktop
		{
			get
			{
				int arg_04_0 = 0;
				switch ((967691261 == 967691261) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				return BaronReplays.Properties.Settings.Default.CreateShortCutOnDesktop;
			}
			set
			{
				int arg_04_0 = 0;
				switch ((845538138 == 845538138) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				BaronReplays.Properties.Settings.Default.CreateShortCutOnDesktop = value;
				BaronReplays.Properties.Settings.Default.Save();
				this.eval_a("CreateShortCutOnDesktop");
				this.eval_b();
			}
		}
		public System.Collections.Generic.List<string> RegionsList
		{
			get
			{
				int arg_04_0 = 0;
				switch ((1041938772 == 1041938772) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				return Utilities.LoLObservePlatformList;
			}
		}
		public int SelectedRegionNumber
		{
			get
			{
				int arg_04_0 = 0;
				switch ((1038319024 == 1038319024) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				return Utilities.LoLObservePlatformList.IndexOf(BaronReplays.Properties.Settings.Default.Platform);
			}
		}
		private void eval_a(string A_0)
		{
			int arg_04_0 = 0;
			int num = 1;
			while (true)
			{
				switch (num)
				{
				case 0:
					switch ((101988031 == 101988031) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					case 1:
						IL_63:
						if (1 != 0)
						{
						}
						if (0 != 0)
						{
						}
						this.PropertyChanged(this, new PropertyChangedEventArgs(A_0));
						num = 2;
						continue;
					}
					goto IL_63;
				case 1:
					switch (0)
					{
					case 0:
						goto IL_31;
					}
					continue;
				case 2:
					return;
				}
				IL_31:
				if (this.PropertyChanged == null)
				{
					break;
				}
				num = 0;
			}
		}
		public Settings()
		{
			this.InitializeComponent();
			base.DataContext = this;
			this.eval_b();
		}
		private void eval_b()
		{
			int num;
			switch ((778760515 == 778760515) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_AF:
				num = 3;
				goto IL_3A;
			case 1:
				IL_1F:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				switch (0)
				{
				case 0:
					goto IL_54;
				}
				goto IL_3A;
			}
			goto IL_1F;
			string text;
			while (true)
			{
				IL_3A:
				switch (num)
				{
				case 0:
					goto IL_A7;
				case 1:
					if (this.CreateShortCutOnDesktop)
					{
						int arg_7A_0 = 0;
						num = 4;
						continue;
					}
					num = 0;
					continue;
				case 2:
					goto IL_9C;
				case 3:
					System.IO.File.Delete(text);
					num = 2;
					continue;
				case 4:
					goto IL_8B;
				}
				goto IL_54;
			}
			IL_8B:
			WshShellClass wshShellClass = new WshShellClass();
			IWshShortcut wshShortcut = (IWshShortcut)wshShellClass.CreateShortcut(text);
			wshShortcut.TargetPath = Process.GetCurrentProcess().MainModule.FileName;
			wshShortcut.Save();
			return;
			IL_9C:
			return;
			IL_A7:
			if (System.IO.File.Exists(text))
			{
				goto IL_AF;
			}
			return;
			IL_54:
			text = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "\\BaronReplays.lnk";
			num = 1;
			goto IL_3A;
		}
		private void eval_a()
		{
			Microsoft.Win32.RegistryKey registryKey;
			while (true)
			{
				IL_00:
				int num = 2;
				while (true)
				{
					switch ((1653592036 == 1653592036) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_00;
					case 1:
						IL_32:
						if (0 != 0)
						{
						}
						switch (num)
						{
						case 0:
							if (1 != 0)
							{
							}
							registryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run", true);
							num = 3;
							continue;
						case 1:
							goto IL_73;
						case 2:
							switch (0)
							{
							case 0:
								goto IL_57;
							}
							continue;
						case 3:
							goto IL_73;
						case 4:
						{
							int arg_80_0 = 0;
							if (this.StartWithSystem)
							{
								num = 5;
								continue;
							}
							goto IL_F8;
						}
						case 5:
							goto IL_9C;
						}
						IL_57:
						if (System.Environment.Is64BitOperatingSystem)
						{
							num = 0;
							continue;
						}
						registryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run", true);
						num = 1;
						continue;
						IL_73:
						num = 4;
						continue;
					}
					goto IL_32;
				}
			}
			IL_9C:
			registryKey.SetValue("BaronReplays", Process.GetCurrentProcess().MainModule.FileName, Microsoft.Win32.RegistryValueKind.String);
			return;
			IL_F8:
			registryKey.DeleteValue("BaronReplays");
		}
		private void eval_d(object A_0, RoutedEventArgs A_1)
		{
			int arg_04_0 = 0;
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			goto IL_14;
			try
			{
				while (true)
				{
					IL_14:
					switch (0)
					{
					case 0:
						goto IL_34;
					}
					int num;
					while (true)
					{
						IL_1E:
						switch (num)
						{
						case 0:
							if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
							{
								num = 2;
								continue;
							}
							goto IL_7F;
						case 1:
							goto IL_88;
						case 2:
							this.ReplaySavePath = folderBrowserDialog.SelectedPath;
							num = 3;
							continue;
						case 3:
							goto IL_7F;
						}
						goto IL_34;
						IL_7F:
						num = 1;
					}
					IL_88:
					switch ((1539746425 == 1539746425) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					}
					break;
					IL_34:
					folderBrowserDialog.ShowNewFolderButton = true;
					folderBrowserDialog.Description = Utilities.GetString("PickReplaySaveFolder");
					num = 0;
					goto IL_1E;
				}
				if (0 != 0)
				{
				}
			}
			finally
			{
				int num = 2;
				while (true)
				{
					switch (num)
					{
					case 0:
						goto IL_FC;
					case 1:
						((System.IDisposable)folderBrowserDialog).Dispose();
						if (1 != 0)
						{
						}
						num = 0;
						continue;
					case 2:
						switch (0)
						{
						case 0:
							goto IL_D5;
						}
						continue;
					}
					IL_D5:
					if (folderBrowserDialog == null)
					{
						break;
					}
					num = 1;
				}
				IL_FC:;
			}
		}
		private void eval_c(object A_0, RoutedEventArgs A_1)
		{
			int num;
			switch ((788680057 == 788680057) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_B4:
				num = 2;
				break;
			case 1:
				goto IL_2B;
			default:
			{
				int arg_22_0 = 0;
				goto IL_2B;
			}
			}
			while (true)
			{
				IL_45:
				switch (num)
				{
				case 0:
					goto IL_8C;
				case 1:
					goto IL_97;
				case 2:
					System.IO.Directory.CreateDirectory(this.ReplaySavePath);
					num = 0;
					continue;
				case 3:
					goto IL_75;
				case 4:
					switch (0)
					{
					case 0:
						goto IL_5F;
					}
					continue;
				}
				IL_5F:
				if (System.IO.Directory.Exists(this.ReplaySavePath))
				{
					num = 3;
				}
				else
				{
					num = 1;
				}
			}
			IL_75:
			if (1 != 0)
			{
			}
			Process.Start(this.ReplaySavePath);
			return;
			IL_8C:
			return;
			IL_97:
			if (System.Windows.MessageBox.Show(Utilities.GetString("DirectoryNotExist"), Utilities.GetString("BR"), MessageBoxButton.OKCancel) != MessageBoxResult.Cancel)
			{
				goto IL_B4;
			}
			return;
			IL_2B:
			if (0 != 0)
			{
			}
			num = 4;
			goto IL_45;
		}
		private void eval_b(object A_0, RoutedEventArgs A_1)
		{
			int arg_04_0 = 0;
			int num = 1;
			while (true)
			{
				switch (num)
				{
				case 0:
					switch ((2072750779 == 2072750779) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					case 1:
						IL_62:
						if (0 != 0)
						{
						}
						this.eval_a("LoLGameExe");
						num = 2;
						continue;
					}
					goto IL_62;
				case 1:
					switch (0)
					{
					case 0:
						goto IL_31;
					}
					continue;
				case 2:
					goto IL_7D;
				}
				IL_31:
				if (!Utilities.SelectLoLExePath())
				{
					return;
				}
				num = 0;
			}
			IL_7D:
			if (1 != 0)
			{
			}
		}
		private void eval_a(object A_0, SelectionChangedEventArgs A_1)
		{
			int arg_04_0 = 0;
			switch ((899894244 == 899894244) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			BaronReplays.Properties.Settings.Default.Platform = Utilities.LoLObservePlatformList[this.RegionsComboBox.SelectedIndex];
			BaronReplays.Properties.Settings.Default.Save();
		}
		private void eval_a(object A_0, RoutedEventArgs A_1)
		{
			int arg_04_0 = 0;
			switch ((1508216326 == 1508216326) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			FileNameFormat element = new FileNameFormat();
			Grid grid = (Grid)base.Parent;
			grid.Children.Clear();
			grid.Children.Add(element);
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), System.Diagnostics.DebuggerNonUserCode]
		public void InitializeComponent()
		{
			while (this._contentLoaded)
			{
				int arg_0C_0 = 0;
				switch ((246647972 == 246647972) ? 1 : 0)
				{
				case 0:
				case 2:
					continue;
				case 1:
					IL_35:
					if (0 != 0)
					{
					}
					if (1 != 0)
					{
					}
					return;
				}
				goto IL_35;
			}
			this._contentLoaded = true;
			Uri resourceLocator = new Uri("/BaronReplays;component/settings.xaml", UriKind.Relative);
			System.Windows.Application.LoadComponent(this, resourceLocator);
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), System.Diagnostics.DebuggerNonUserCode]
		internal System.Delegate eval_a(System.Type A_0, string A_1)
		{
			int arg_04_0 = 0;
			switch ((1310935914 == 1310935914) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			return System.Delegate.CreateDelegate(A_0, this, A_1);
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), System.Diagnostics.DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			while (true)
			{
				switch (0)
				{
				case 0:
					goto IL_1C;
				}
				while (true)
				{
					IL_0A:
					int num;
					switch (num)
					{
					case 0:
						num = 2;
						continue;
					case 1:
						switch (connectionId)
						{
						case 1:
							goto IL_B0;
						case 2:
							goto IL_116;
						case 3:
							goto IL_109;
						case 4:
							goto IL_A3;
						case 5:
							goto IL_D8;
						case 6:
							goto IL_80;
						case 7:
							goto IL_BD;
						case 8:
							goto IL_E5;
						default:
							num = 0;
							continue;
						}
						break;
					case 2:
						goto IL_D6;
					}
					break;
				}
				IL_1C:
				switch ((248550327 == 248550327) ? 1 : 0)
				{
				case 0:
				case 2:
					continue;
				case 1:
				{
					IL_3B:
					if (0 != 0)
					{
					}
					int num = 1;
					goto IL_0A;
				}
				}
				goto IL_3B;
			}
			IL_80:
			if (1 != 0)
			{
			}
			int arg_8E_0 = 0;
			this.FileNameFormatTextbox = (System.Windows.Controls.TextBox)target;
			return;
			IL_A3:
			this.SelectSavePathButton = (TriStateButton)target;
			return;
			IL_B0:
			this.SettingView = (Settings)target;
			return;
			IL_BD:
			this.FileNameFormatChangeButton = (TriStateButton)target;
			return;
			IL_D6:
			this._contentLoaded = true;
			return;
			IL_D8:
			this.OpenFolderPathButton = (TriStateButton)target;
			return;
			IL_E5:
			this.RegionsComboBox = (System.Windows.Controls.ComboBox)target;
			this.RegionsComboBox.SelectionChanged += new SelectionChangedEventHandler(this.eval_a);
			return;
			IL_109:
			this.ReplaySavePathTextbox = (System.Windows.Controls.TextBox)target;
			return;
			IL_116:
			this.SelectLoLGameExeButton = (TriStateButton)target;
		}
	}
}
