using BaronReplays.Properties;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Data;
using System.Windows.Media.Imaging;
namespace BaronReplays
{
	public class StringToChampionIcon : IValueConverter
	{
		public static string ChampionDir;
		public static string ChampionDirFullPath;
		public static object Lock;
		public static System.Collections.Generic.Dictionary<string, BitmapImage> ChampionToBitmapImage;
		public static void CheckChampionDir()
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				int arg_13_0 = 0;
				switch (0)
				{
				case 0:
					goto IL_40;
				}
				int num2;
				string[] array;
				while (true)
				{
					IL_25:
					switch (num)
					{
					case 0:
						goto IL_9E;
					case 1:
					{
						if (num2 >= array.Length)
						{
							num = 3;
							continue;
						}
						string text = array[num2];
						num = 2;
						continue;
					}
					case 2:
						try
						{
							string text;
							StringToChampionIcon.ChampionToBitmapImage.Add(text.Substring(text.LastIndexOf('\\') + 1), Utilities.GetBitmapImage(text));
							goto IL_7E;
						}
						catch
						{
							string text;
							if (System.IO.File.Exists(text))
							{
								switch ((811851721 == 811851721) ? 1 : 0)
								{
								case 0:
								case 2:
									goto IL_11B;
								case 1:
									IL_10A:
									if (0 != 0)
									{
									}
									System.IO.File.Delete(text);
									goto IL_11B;
								}
								goto IL_10A;
							}
							IL_11B:
							goto IL_7E;
						}
						return;
						IL_7E:
						num2++;
						num = 0;
						continue;
					case 3:
						return;
					case 4:
						goto IL_9E;
					}
					goto IL_40;
					IL_9E:
					num = 1;
				}
				return;
				IL_40:
				Utilities.IfDirectoryNotExitThenCreate(StringToChampionIcon.ChampionDirFullPath);
				string[] files = System.IO.Directory.GetFiles(StringToChampionIcon.ChampionDirFullPath, "*.png");
				StringToChampionIcon.ChampionToBitmapImage = new System.Collections.Generic.Dictionary<string, BitmapImage>();
				array = files;
				num2 = 0;
				if (1 != 0)
				{
				}
				num = 4;
				goto IL_25;
			}
			}
			goto IL_0F;
		}
		public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			switch (0)
			{
			case 0:
			{
				IL_0F:
				int num = 0;
				string text;
				while (true)
				{
					switch (num)
					{
					case 0:
						switch (0)
						{
						case 0:
							goto IL_46;
						}
						continue;
					case 1:
						goto IL_1A3;
					case 2:
						goto IL_24B;
					case 3:
						switch ((1546587688 == 1546587688) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_23E;
						case 1:
						{
							IL_222:
							if (0 != 0)
							{
							}
							if ((value as string).Contains("Unknow Champion"))
							{
								goto IL_23E;
							}
							text = value + ".png";
							string str = string.Format("http://ddragon.leagueoflegends.com/cdn/{0}/img/champion/", BaronReplays.Properties.Settings.Default.NAVersion);
							bool flag = false;
							num = 6;
							continue;
						}
						}
						goto IL_222;
						IL_23E:
						num = 2;
						continue;
					case 4:
						if ((value as string).Length == 0)
						{
							num = 1;
							continue;
						}
						num = 3;
						continue;
					case 5:
						goto IL_62;
					case 6:
						goto IL_1DB;
					}
					IL_46:
					if (value == null)
					{
						int arg_50_0 = 0;
						num = 5;
					}
					else
					{
						num = 4;
					}
				}
				IL_62:
				return null;
				IL_17A:
				return null;
				IL_1A3:
				return null;
				IL_1DB:
				if (1 != 0)
				{
				}
				try
				{
					switch (0)
					{
					case 0:
						goto IL_84;
					}
					while (true)
					{
						IL_71:
						switch (num)
						{
						case 0:
							while (true)
							{
								try
								{
									WebClient webClient = new WebClient();
									string str;
									webClient.DownloadFile(str + text, StringToChampionIcon.ChampionDir + text);
									goto IL_B6;
								}
								catch
								{
									return null;
								}
								goto IL_11D;
								try
								{
									IL_B6:
									StringToChampionIcon.ChampionToBitmapImage.Add(text, Utilities.GetBitmapImage(StringToChampionIcon.ChampionDirFullPath + text));
									goto IL_11D;
								}
								catch
								{
									string path = StringToChampionIcon.ChampionDirFullPath + text;
									if (System.IO.File.Exists(path))
									{
										System.IO.File.Delete(path);
									}
									goto IL_11D;
								}
							}
							break;
						case 1:
							goto IL_12A;
						case 2:
							if (!StringToChampionIcon.ChampionToBitmapImage.ContainsKey(text))
							{
								num = 0;
								continue;
							}
							goto IL_11D;
						}
						goto IL_84;
						IL_11D:
						num = 1;
					}
					IL_12A:
					goto IL_1EA;
					IL_84:
					bool flag;
					object @lock;
					System.Threading.Monitor.Enter(@lock = StringToChampionIcon.Lock, ref flag);
					num = 2;
					goto IL_71;
				}
				finally
				{
					num = 2;
					while (true)
					{
						switch (num)
						{
						case 0:
						{
							object @lock;
							System.Threading.Monitor.Exit(@lock);
							num = 1;
							continue;
						}
						case 1:
							goto IL_177;
						case 2:
							switch (0)
							{
							case 0:
								goto IL_156;
							}
							continue;
						}
						IL_156:
						bool flag;
						if (!flag)
						{
							break;
						}
						num = 0;
					}
					IL_177:;
				}
				goto IL_17A;
				IL_1EA:
				return StringToChampionIcon.ChampionToBitmapImage[text];
				IL_24B:
				goto IL_17A;
			}
			}
			goto IL_0F;
		}
		public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			int arg_04_0 = 0;
			switch ((1105592714 == 1105592714) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			throw new System.NotImplementedException();
		}
		static StringToChampionIcon()
		{
			// Note: this type is marked as 'beforefieldinit'.
			int arg_04_0 = 0;
			switch ((418927110 == 418927110) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			StringToChampionIcon.ChampionDir = "Champions\\";
			StringToChampionIcon.ChampionDirFullPath = System.AppDomain.CurrentDomain.BaseDirectory + StringToChampionIcon.ChampionDir;
			StringToChampionIcon.Lock = new object();
		}
	}
}
