using BaronReplays.Properties;
using System;
namespace BaronReplays
{
	public static class FileNameGenerater
	{
		public static string GenerateFilename(LoLRecorder recoder)
		{
			if (1 != 0)
			{
			}
			int num = 4;
			string text;
			while (true)
			{
				switch (num)
				{
				case 0:
					switch ((1294109884 == 1294109884) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_5E;
					}
					goto IL_86;
				case 1:
					if (recoder.record.hasResult)
					{
						num = 3;
						continue;
					}
					goto IL_F0;
				case 2:
					goto IL_48;
				case 3:
					text = FileNameGenerater.ReplaceEndOfGameStatsProperty(recoder, text);
					goto IL_5E;
				case 4:
					switch (0)
					{
					case 0:
						goto IL_37;
					}
					continue;
				}
				IL_37:
				if (!recoder.selfGame)
				{
					num = 2;
					continue;
				}
				text = BaronReplays.Properties.Settings.Default.FileNameFormat;
				text = FileNameGenerater.ReplaceGeneralProperty(recoder, text);
				num = 1;
				continue;
				IL_5E:
				num = 0;
			}
			IL_48:
			int arg_4C_0 = 0;
			return Utilities.GetString("Spectate") + "-" + System.DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss.lpr");
			IL_86:
			if (0 != 0)
			{
			}
			IL_F0:
			text = text.Replace('<', '(');
			text = text.Replace('>', ')');
			return text;
		}
		public static string ReplaceParticipantProperty(LoLRecorder recoder, string format)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_44;
				}
				int num2;
				while (true)
				{
					IL_19:
					switch (num)
					{
					case 0:
						goto IL_18A;
					case 1:
						if (!(recoder.record.gameStats.Players[num2].SummonerName == recoder.selfPlayerInfo.playerName))
						{
							int arg_23C_0 = 0;
							num = 4;
							continue;
						}
						goto IL_A2;
					case 2:
						return format;
					case 3:
						goto IL_1BA;
					case 4:
						num2++;
						num = 3;
						continue;
					case 5:
						goto IL_A2;
					case 6:
						if (num2 >= recoder.record.gameStats.Players.Count)
						{
							num = 5;
							continue;
						}
						if (1 != 0)
						{
						}
						num = 1;
						continue;
					case 7:
						goto IL_9D;
					case 8:
						if (recoder.record.gameStats.Players.Count == 0)
						{
							num = 7;
							continue;
						}
						goto IL_1BA;
					}
					goto IL_44;
					IL_A2:
					format = format.Replace("<Kill>", recoder.record.gameStats.Players[num2].DetailStats.K.ToString());
					format = format.Replace("<Death>", recoder.record.gameStats.Players[num2].DetailStats.D.ToString());
					format = format.Replace("<Assist>", recoder.record.gameStats.Players[num2].DetailStats.A.ToString());
					format = format.Replace("<KDA>", string.Format("{0:N1}", recoder.record.gameStats.Players[num2].DetailStats.KDAValue));
					num = 0;
					continue;
					IL_1BA:
					num = 6;
				}
				IL_18A:
				format = format.Replace("<WinLose>", recoder.record.gameStats.Players[num2].DetailStats.Win ? Utilities.GetString("Win") : Utilities.GetString("Lose"));
				return format;
				IL_44:
				num2 = 0;
				switch ((1616149378 == 1616149378) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_9D:
					num = 2;
					goto IL_19;
				case 1:
					IL_65:
					if (0 != 0)
					{
					}
					num = 8;
					goto IL_19;
				}
				goto IL_65;
			}
			}
			goto IL_0F;
		}
		public static string ReplaceEndOfGameStatsProperty(LoLRecorder recoder, string format)
		{
			switch (0)
			{
			case 0:
				goto IL_42;
			}
			int num;
			while (true)
			{
				IL_0A:
				switch ((427948396 == 427948396) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_89;
				case 1:
					IL_29:
					if (0 != 0)
					{
					}
					switch (num)
					{
					case 0:
						if (1 != 0)
						{
						}
						if (recoder.record.gameStats.Players != null)
						{
							goto IL_89;
						}
						goto IL_AE;
					case 1:
						goto IL_AC;
					case 2:
						format = FileNameGenerater.ReplaceParticipantProperty(recoder, format);
						num = 1;
						continue;
					}
					goto IL_42;
				}
				goto IL_29;
				IL_89:
				num = 2;
			}
			IL_AC:
			IL_AE:
			int arg_B2_0 = 0;
			return format;
			IL_42:
			format = format.Replace("<Map>", Utilities.GetString(recoder.record.gameStats.GameMode));
			num = 0;
			goto IL_0A;
		}
		public static string ReplaceGeneralProperty(LoLRecorder recoder, string format)
		{
			switch (0)
			{
			case 0:
			{
				IL_0F:
				if (1 != 0)
				{
				}
				switch ((590911595 == 590911595) ? 1 : 0)
				{
				case 0:
				case 2:
				{
					IL_6E:
					int num = 0;
					goto IL_53;
				}
				case 1:
				{
					IL_38:
					if (0 != 0)
					{
					}
					int num = 2;
					goto IL_53;
				}
				}
				goto IL_38;
				while (true)
				{
					IL_53:
					int num;
					switch (num)
					{
					case 0:
						format = format.Replace("<SummonerName>", recoder.selfPlayerInfo.playerName);
						format = format.Replace("<Champion>", recoder.selfPlayerInfo.championName);
						num = 1;
						continue;
					case 1:
						goto IL_B4;
					case 2:
						switch (0)
						{
						case 0:
							goto IL_66;
						}
						continue;
					}
					break;
				}
				IL_66:
				if (recoder.selfPlayerInfo != null)
				{
					goto IL_6E;
				}
				IL_B4:
				int arg_BA_0 = 0;
				System.DateTime now = System.DateTime.Now;
				format = format.Replace("<Year>", now.Year.ToString());
				format = format.Replace("<Month>", now.Month.ToString());
				format = format.Replace("<Day>", now.Day.ToString());
				format = format.Replace("<Hour>", now.Hour.ToString());
				format = format.Replace("<Minute>", now.Minute.ToString());
				format = format.Replace("<Second>", now.Second.ToString());
				return format;
			}
			}
			goto IL_0F;
		}
	}
}
