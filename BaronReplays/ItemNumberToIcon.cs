using BaronReplays.Properties;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Data;
using System.Windows.Media.Imaging;
namespace BaronReplays
{
	public class ItemNumberToIcon : IValueConverter
	{
		public static string ItemsDir;
		public static string ItemsDirFullPath;
		public static object Lock;
		public static System.Collections.Generic.Dictionary<string, BitmapImage> ItemNumberToBitmapImage;
		public static void CheckItemDir()
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				int arg_13_0 = 0;
				if (1 != 0)
				{
				}
				switch (0)
				{
				case 0:
					goto IL_4A;
				}
				int num2;
				string[] array;
				while (true)
				{
					IL_2F:
					switch (num)
					{
					case 0:
					{
						if (num2 >= array.Length)
						{
							num = 3;
							continue;
						}
						string text = array[num2];
						num = 4;
						continue;
					}
					case 1:
						goto IL_9E;
					case 2:
						goto IL_9E;
					case 3:
						return;
					case 4:
						try
						{
							string text;
							ItemNumberToIcon.ItemNumberToBitmapImage.Add(text.Substring(text.LastIndexOf('\\') + 1), Utilities.GetBitmapImage(text));
							goto IL_7E;
						}
						catch
						{
							string text;
							if (System.IO.File.Exists(text))
							{
								switch ((318894991 == 318894991) ? 1 : 0)
								{
								case 0:
								case 2:
									goto IL_118;
								case 1:
									IL_107:
									if (0 != 0)
									{
									}
									System.IO.File.Delete(text);
									goto IL_118;
								}
								goto IL_107;
							}
							IL_118:
							goto IL_7E;
						}
						return;
						IL_7E:
						num2++;
						num = 1;
						continue;
					}
					goto IL_4A;
					IL_9E:
					num = 0;
				}
				return;
				IL_4A:
				Utilities.IfDirectoryNotExitThenCreate(ItemNumberToIcon.ItemsDirFullPath);
				string[] files = System.IO.Directory.GetFiles(ItemNumberToIcon.ItemsDirFullPath, "*.png");
				ItemNumberToIcon.ItemNumberToBitmapImage = new System.Collections.Generic.Dictionary<string, BitmapImage>();
				array = files;
				num2 = 0;
				num = 2;
				goto IL_2F;
			}
			}
			goto IL_0F;
		}
		public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			switch (0)
			{
			case 0:
			{
				IL_0F:
				int num = 1;
				string text;
				while (true)
				{
					switch (num)
					{
					case 0:
						goto IL_55;
					case 1:
						switch (0)
						{
						case 0:
							goto IL_36;
						}
						continue;
					case 2:
						goto IL_226;
					}
					IL_36:
					if (1 != 0)
					{
					}
					if ((uint)value == 0u)
					{
						num = 0;
					}
					else
					{
						text = (uint)value + ".png";
						string str = string.Format("http://ddragon.leagueoflegends.com/cdn/{0}/img/item/", BaronReplays.Properties.Settings.Default.NAVersion);
						bool flag = false;
						num = 2;
					}
				}
				IL_55:
				IL_1D2:
				int arg_1D6_0 = 0;
				return null;
				IL_226:
				try
				{
					switch (0)
					{
					case 0:
						goto IL_7B;
					}
					while (true)
					{
						IL_64:
						switch (num)
						{
						case 0:
							if (!ItemNumberToIcon.ItemNumberToBitmapImage.ContainsKey(text))
							{
								num = 2;
								continue;
							}
							goto IL_179;
						case 1:
							goto IL_186;
						case 2:
							try
							{
								WebClient webClient = new WebClient();
								try
								{
									string str;
									webClient.DownloadFile(str + text, ItemNumberToIcon.ItemsDir + text);
								}
								finally
								{
									num = 2;
									while (true)
									{
										switch (num)
										{
										case 0:
											switch ((465760473 == 465760473) ? 1 : 0)
											{
											case 0:
											case 2:
												goto IL_138;
											}
											goto IL_161;
										case 1:
											((System.IDisposable)webClient).Dispose();
											goto IL_138;
										case 2:
											switch (0)
											{
											case 0:
												goto IL_123;
											}
											continue;
										}
										IL_123:
										if (webClient != null)
										{
											num = 1;
											continue;
										}
										goto IL_16A;
										IL_138:
										num = 0;
									}
									IL_161:
									if (0 != 0)
									{
									}
									IL_16A:;
								}
								goto IL_B0;
							}
							catch
							{
								return null;
							}
							goto IL_179;
							IL_B0:
							ItemNumberToIcon.ItemNumberToBitmapImage.Add(text, Utilities.GetBitmapImage(ItemNumberToIcon.ItemsDirFullPath + text));
							num = 3;
							continue;
						case 3:
							goto IL_179;
						}
						goto IL_7B;
						IL_179:
						num = 1;
					}
					IL_186:
					goto IL_1E0;
					IL_7B:
					bool flag;
					object @lock;
					System.Threading.Monitor.Enter(@lock = ItemNumberToIcon.Lock, ref flag);
					num = 0;
					goto IL_64;
				}
				finally
				{
					num = 2;
					while (true)
					{
						switch (num)
						{
						case 0:
							goto IL_1CF;
						case 1:
						{
							object @lock;
							System.Threading.Monitor.Exit(@lock);
							num = 0;
							continue;
						}
						case 2:
							switch (0)
							{
							case 0:
								goto IL_1AF;
							}
							continue;
						}
						IL_1AF:
						bool flag;
						if (!flag)
						{
							break;
						}
						num = 1;
					}
					IL_1CF:;
				}
				goto IL_1D2;
				IL_1E0:
				return ItemNumberToIcon.ItemNumberToBitmapImage[text];
			}
			}
			goto IL_0F;
		}
		public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			switch ((905683332 == 905683332) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			int arg_2D_0 = 0;
			if (0 != 0)
			{
			}
			throw new System.NotImplementedException();
		}
		static ItemNumberToIcon()
		{
			// Note: this type is marked as 'beforefieldinit'.
			switch ((391987189 == 391987189) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			int arg_2D_0 = 0;
			if (0 != 0)
			{
			}
			ItemNumberToIcon.ItemsDir = "Items\\";
			ItemNumberToIcon.ItemsDirFullPath = System.AppDomain.CurrentDomain.BaseDirectory + ItemNumberToIcon.ItemsDir;
			ItemNumberToIcon.Lock = new object();
		}
	}
}
