using BaronReplays.Properties;
using Ionic.Zip;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
namespace BaronReplays
{
	public class LoLLauncher
	{
		private LoLRecord eval_a;
		private string eval_b;
		private string c;
		private bool eval_d;
		private LoLRecordPlayer eval_e;
		private System.Threading.Thread eval_f;
		private LoLRecorder eval_g;
		public MainWindow.LoLLauncherDoneDelegate AnyEvent;
		public Window Parent;
		private DispatcherTimer eval_h;
		private GameInfo eval_i;
		private void eval_f()
		{
			switch ((2117500497 == 2117500497) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			int arg_23_0 = 0;
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.eval_h = new DispatcherTimer();
			this.eval_h.Interval = System.TimeSpan.FromMilliseconds(500.0);
			this.eval_h.Tick += new System.EventHandler(this.eval_a);
			this.eval_h.Start();
		}
		private void eval_a(object A_0, System.EventArgs A_1)
		{
			int num = 1;
			while (true)
			{
				switch ((1433511235 == 1433511235) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_D5;
				case 1:
					IL_32:
					if (0 != 0)
					{
					}
					switch (num)
					{
					case 0:
						if (1 != 0)
						{
						}
						if (this.eval_e != null)
						{
							goto IL_D5;
						}
						goto IL_74;
					case 1:
						switch (0)
						{
						case 0:
							goto IL_5F;
						}
						continue;
					case 2:
						if (this.AnyEvent != null)
						{
							num = 6;
							continue;
						}
						return;
					case 3:
						goto IL_74;
					case 4:
						this.eval_h.Stop();
						num = 0;
						continue;
					case 5:
						this.eval_e.StopPlaying();
						num = 3;
						continue;
					case 6:
						this.AnyEvent(true, null);
						num = 7;
						continue;
					case 7:
						return;
					}
					IL_5F:
					if (!global::eval_a.eval_d())
					{
						num = 4;
						continue;
					}
					return;
					IL_74:
					num = 2;
					continue;
				}
				goto IL_32;
				IL_D5:
				int arg_D9_0 = 0;
				num = 5;
			}
		}
		public LoLLauncher(LoLRecorder recordingRecorder)
		{
			this.eval_g = recordingRecorder;
			this.eval_a = recordingRecorder.record;
			this.eval_d = false;
		}
		public LoLLauncher(SimpleLoLRecord simpleRecord)
		{
			this.eval_a = new LoLRecord();
			this.eval_a.readFromFile(simpleRecord.FileName, false);
			this.eval_d = true;
		}
		public LoLLauncher(string recordFilePath)
		{
			this.eval_a = new LoLRecord();
			this.eval_a.readFromFile(recordFilePath, false);
			this.eval_d = true;
		}
		public LoLLauncher(LoLRecord record)
		{
			this.eval_a = record;
			this.eval_d = true;
		}
		public LoLLauncher(GameInfo info)
		{
			this.eval_i = info;
			this.eval_d = false;
		}
		private void eval_e()
		{
			switch ((696384948 == 696384948) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			int arg_23_0 = 0;
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.eval_e = new LoLRecordPlayer(this.eval_a);
			System.Threading.ThreadStart start = new System.Threading.ThreadStart(this.eval_e.startPlaying);
			this.eval_f = new System.Threading.Thread(start);
			this.eval_f.Start();
		}
		private void eval_d()
		{
			int num = 1;
			while (true)
			{
				switch (num)
				{
				case 0:
					switch ((742641131 == 742641131) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_CF;
					case 1:
					{
						IL_60:
						int arg_64_0 = 0;
						if (0 != 0)
						{
						}
						if (1 != 0)
						{
						}
						num = 2;
						continue;
					}
					}
					goto IL_60;
				case 1:
					switch (0)
					{
					case 0:
						goto IL_2D;
					}
					continue;
				case 2:
					if (this.eval_d)
					{
						num = 4;
						continue;
					}
					this.eval_i = new GameInfo(this.eval_g.platformAddress, new string(this.eval_a.gamePlatform), this.eval_a.gameId, new string(this.eval_a.observerEncryptionKey));
					num = 3;
					continue;
				case 3:
					goto IL_CF;
				case 4:
					goto IL_130;
				}
				IL_2D:
				if (this.eval_i != null)
				{
					break;
				}
				num = 0;
			}
			IL_CF:
			return;
			IL_130:
			this.eval_i = new GameInfo("127.0.0.1:8080", new string(this.eval_a.gamePlatform), this.eval_a.gameId, new string(this.eval_a.observerEncryptionKey));
		}
		public void StartPlaying()
		{
			switch (0)
			{
			case 0:
				goto IL_4C;
			}
			int num;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					num = 14;
					continue;
				case 1:
				{
					int arg_154_0 = 0;
					this.AnyEvent(false, null);
					num = 6;
					continue;
				}
				case 2:
					goto IL_18C;
				case 3:
					goto IL_1BB;
				case 4:
					if (this.AnyEvent != null)
					{
						num = 11;
						continue;
					}
					return;
				case 5:
					this.eval_e();
					num = 2;
					continue;
				case 6:
					goto IL_175;
				case 7:
					if (this.eval_d)
					{
						switch ((1603193025 == 1603193025) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_99;
						case 1:
							IL_10E:
							if (0 != 0)
							{
							}
							num = 5;
							continue;
						}
						goto IL_10E;
					}
					goto IL_1E5;
				case 8:
					if (this.eval_a.IsBroken)
					{
						num = 9;
						continue;
					}
					goto IL_1BD;
				case 9:
					num = 4;
					continue;
				case 10:
					num = 8;
					continue;
				case 11:
					this.AnyEvent(false, Utilities.GetString("BrokenFile"));
					if (1 != 0)
					{
					}
					num = 3;
					continue;
				case 12:
					if (!this.eval_a())
					{
						num = 0;
						continue;
					}
					this.eval_c();
					num = 7;
					continue;
				case 13:
					if (this.eval_d)
					{
						num = 10;
						continue;
					}
					goto IL_1BD;
				case 14:
					if (this.AnyEvent != null)
					{
						goto IL_99;
					}
					return;
				}
				goto IL_4C;
				IL_99:
				num = 1;
				continue;
				IL_1BD:
				num = 12;
			}
			return;
			IL_175:
			return;
			IL_18C:
			goto IL_1E5;
			IL_1BB:
			return;
			IL_1E5:
			this.eval_b();
			this.eval_f();
			return;
			IL_4C:
			this.eval_b = BaronReplays.Properties.Settings.Default.LoLGameExe;
			this.eval_d();
			num = 13;
			goto IL_0A;
		}
		private void eval_c()
		{
			switch ((1243125542 == 1243125542) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			int arg_23_0 = 0;
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.c = string.Concat(new object[]
			{
				"60000 BR.exe Air\\BR.exe \"spectator ",
				this.eval_i.ServerAddress,
				" ",
				this.eval_i.ObKey,
				" ",
				this.eval_i.GameId,
				" ",
				this.eval_i.PlatformId,
				"\""
			});
		}
		private void eval_b()
		{
			if (1 != 0)
			{
			}
			switch ((420931866 == 420931866) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_28:
				int arg_2C_0 = 0;
				goto IL_35;
			}
			case 1:
				goto IL_35;
			}
			goto IL_28;
			IL_35:
			if (0 != 0)
			{
			}
			Process.Start(new ProcessStartInfo
			{
				UseShellExecute = false,
				Arguments = this.c,
				FileName = this.eval_b,
				WorkingDirectory = BaronReplays.Properties.Settings.Default.LoLGameExe.Remove(BaronReplays.Properties.Settings.Default.LoLGameExe.LastIndexOf('\\'))
			});
		}
		private bool eval_a()
		{
			while (true)
			{
				switch (0)
				{
				case 0:
				{
					IL_0F:
					int arg_13_0 = 0;
					switch ((1806941000 == 1806941000) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					}
					goto IL_3A;
				}
				}
				goto IL_0F;
			}
			IL_3A:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			string productVersion = FileVersionInfo.GetVersionInfo(this.eval_b).ProductVersion;
			string text = null;
			string simpleVersionNumber = Utilities.GetSimpleVersionNumber(productVersion);
			bool result;
			try
			{
				int num = 1;
				while (true)
				{
					switch (num)
					{
					case 0:
						goto IL_168;
					case 1:
						switch (0)
						{
						case 0:
							goto IL_A8;
						}
						continue;
					case 2:
						goto IL_15B;
					case 3:
						if (string.Compare(text, simpleVersionNumber) != 0)
						{
							num = 5;
							continue;
						}
						goto IL_15B;
					case 4:
						goto IL_114;
					case 5:
						this.eval_b = System.AppDomain.CurrentDomain.BaseDirectory + "\\LoLExecutable\\" + text + "\\League of Legends.exe";
						num = 7;
						continue;
					case 6:
						result = false;
						num = 4;
						continue;
					case 7:
						if (!this.eval_a(text))
						{
							num = 6;
							continue;
						}
						goto IL_15B;
					case 8:
						text = Utilities.GetSimpleVersionNumber(new string(this.eval_a.lolVersion));
						num = 3;
						continue;
					}
					IL_A8:
					if (this.eval_d)
					{
						num = 8;
						continue;
					}
					text = simpleVersionNumber;
					num = 2;
					continue;
					IL_15B:
					num = 0;
				}
				IL_114:
				return result;
				IL_168:
				return true;
			}
			catch
			{
				result = false;
			}
			return result;
		}
		private bool eval_a(string A_0)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_3C;
				}
				string text;
				while (true)
				{
					IL_19:
					DownloadProgress downloadProgress;
					bool? isSuccess2;
					switch (num)
					{
					case 0:
						return false;
					case 1:
						return true;
					case 2:
						try
						{
							ZipFile zipFile;
							string baseDirectory;
							zipFile.ExtractAll(baseDirectory + "\\LoLExecutable");
							goto IL_1FA;
						}
						finally
						{
							num = 2;
							while (true)
							{
								ZipFile zipFile;
								switch (num)
								{
								case 0:
									goto IL_1C1;
								case 1:
									((System.IDisposable)zipFile).Dispose();
									num = 0;
									continue;
								case 2:
									switch (0)
									{
									case 0:
										goto IL_1A0;
									}
									continue;
								}
								IL_1A0:
								if (zipFile == null)
								{
									break;
								}
								num = 1;
							}
							IL_1C1:;
						}
						goto IL_1C4;
					case 3:
					{
						string path;
						if (System.IO.File.Exists(path))
						{
							num = 1;
							continue;
						}
						string baseDirectory;
						text = baseDirectory + "\\" + A_0 + ".zip";
						string url = "https://sites.google.com/a/ahri.tw/ahri/" + A_0 + ".zip";
						downloadProgress = new DownloadProgress(url, text);
						downloadProgress.Owner = this.Parent;
						downloadProgress.ShowDialog();
						bool? isSuccess = downloadProgress.IsSuccess;
						int arg_130_0 = 0;
						num = 6;
						continue;
					}
					case 4:
					{
						if (!isSuccess2.Value)
						{
							num = 5;
							continue;
						}
						ReadOptions options = new ReadOptions
						{
							StatusMessageWriter = System.Console.Out
						};
						ZipFile zipFile = ZipFile.Read(text, options);
						if (1 != 0)
						{
						}
						num = 2;
						continue;
					}
					case 5:
						return false;
					case 6:
					{
						bool? isSuccess;
						if (!isSuccess.HasValue)
						{
							goto IL_14E;
						}
						goto IL_1C4;
					}
					}
					goto IL_3C;
					IL_1C4:
					isSuccess2 = downloadProgress.IsSuccess;
					num = 4;
				}
				return true;
				IL_1FA:
				System.IO.File.Delete(text);
				return true;
				IL_3C:
				switch ((1885116558 == 1885116558) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_14E:
					num = 0;
					goto IL_19;
				case 1:
				{
					IL_5B:
					if (0 != 0)
					{
					}
					string baseDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
					string path = baseDirectory + "\\LoLExecutable\\" + A_0 + "\\League of Legends.exe";
					num = 3;
					goto IL_19;
				}
				}
				goto IL_5B;
			}
			}
			goto IL_0F;
		}
	}
}
