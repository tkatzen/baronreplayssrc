using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
namespace BaronReplays
{
	public class LoLRecord
	{
		public const int LPRLatestVersion = 4;
		public bool IsBroken;
		public int ThisLPRVersion = 4;
		public string relatedFileName;
		public char[] spectatorClientVersion;
		public ulong gameId;
		public int gameChunkTimeInterval;
		public int gameKeyFrameTimeInterval;
		public int gameStartChunkId;
		public int gameEndKeyFrameId;
		public int gameEndChunkId;
		public int gameEndStartupChunkId;
		public int gameLength;
		public int gameClientAddLag;
		public int gameELOLevel;
		public char[] gamePlatform;
		public char[] observerEncryptionKey;
		public char[] gameCreateTime;
		public char[] gameStartTime;
		public char[] gameEndTime;
		public int gameDelayTime;
		public int gameLastChunkTime;
		public int gameLastChunkDuration;
		public char[] lolVersion;
		public bool hasResult;
		public EndOfGameStats gameStats;
		public System.Collections.Generic.Dictionary<int, byte[]> gameChunks;
		public System.Collections.Generic.Dictionary<int, byte[]> gameKeyFrames;
		private byte[] eval_a;
		public PlayerInfo[] players;
		public void gameMetaAnalyze(JObject metaJson)
		{
			switch ((12097927 == 12097927) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			JObject jObject = JsonConvert.DeserializeObject<JObject>(metaJson["gameKey"].ToString());
			this.gameId = ulong.Parse(jObject["gameId"].ToString());
			this.gamePlatform = jObject["platformId"].ToString().ToCharArray();
			this.gameChunkTimeInterval = int.Parse(metaJson["chunkTimeInterval"].ToString());
			this.gameStartTime = metaJson["startTime"].ToString().ToCharArray();
			this.gameEndTime = metaJson["endTime"].ToString().ToCharArray();
			this.gameEndChunkId = int.Parse(metaJson["lastChunkId"].ToString());
			this.gameEndKeyFrameId = int.Parse(metaJson["lastKeyFrameId"].ToString());
			this.gameEndStartupChunkId = int.Parse(metaJson["endStartupChunkId"].ToString());
			this.gameDelayTime = int.Parse(metaJson["delayTime"].ToString());
			this.gameKeyFrameTimeInterval = int.Parse(metaJson["keyFrameTimeInterval"].ToString());
			this.gameStartChunkId = int.Parse(metaJson["startGameChunkId"].ToString());
			this.gameLength = int.Parse(metaJson["gameLength"].ToString());
			this.gameClientAddLag = int.Parse(metaJson["clientAddedLag"].ToString());
			this.gameELOLevel = int.Parse(metaJson["interestScore"].ToString());
			this.gameCreateTime = metaJson["createTime"].ToString().ToCharArray();
			this.eval_a();
		}
		public string getMetaData()
		{
			switch ((1318111416 == 1318111416) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			int arg_34_0 = 0;
			JObject jObject = new JObject();
			jObject.Add("gameId", this.gameId);
			jObject.Add("platformId", new string(this.gamePlatform));
			return new JObject
			{

				{
					"gameKey",
					jObject
				},

				{
					"gameServerAddress",
					""
				},

				{
					"port",
					0
				},

				{
					"encryptionKey",
					""
				},

				{
					"chunkTimeInterval",
					this.gameChunkTimeInterval
				},

				{
					"startTime",
					new string(this.gameStartTime)
				},

				{
					"endTime",
					new string(this.gameEndTime)
				},

				{
					"gameEnded",
					true
				},

				{
					"lastChunkId",
					this.gameEndChunkId
				},

				{
					"lastKeyFrameId",
					this.gameEndKeyFrameId
				},

				{
					"endStartupChunkId",
					this.gameEndStartupChunkId
				},

				{
					"delayTime",
					this.gameDelayTime
				},

				{
					"pendingAvailableChunkInfo",
					""
				},

				{
					"pendingAvailableKeyFrameInfo",
					""
				},

				{
					"keyFrameTimeInterval",
					this.gameKeyFrameTimeInterval
				},

				{
					"decodedEncryptionKey",
					""
				},

				{
					"startGameChunkId",
					this.gameStartChunkId
				},

				{
					"gameLength",
					this.gameLength
				},

				{
					"clientAddedLag",
					this.gameClientAddLag
				},

				{
					"clientBackFetchingEnabled",
					true
				},

				{
					"clientBackFetchingFreq",
					"50"
				},

				{
					"interestScore",
					this.gameELOLevel
				},

				{
					"featuredGame",
					"false"
				},

				{
					"createTime",
					new string(this.gameCreateTime)
				}
			}.ToString();
		}
		public string getLastChunkInfo()
		{
			switch ((1777022477 == 1777022477) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			int arg_34_0 = 0;
			return new JObject
			{

				{
					"chunkId",
					this.gameEndChunkId
				},

				{
					"availableSince",
					this.gameLastChunkTime
				},

				{
					"nextAvailableChunk",
					0
				},

				{
					"keyFrameId",
					this.gameEndKeyFrameId
				},

				{
					"nextChunkId",
					this.gameEndChunkId
				},

				{
					"endStartupChunkId",
					this.gameEndStartupChunkId
				},

				{
					"startGameChunkId",
					this.gameStartChunkId
				},

				{
					"endGameChunkId",
					this.gameEndChunkId
				},

				{
					"duration",
					this.gameLastChunkDuration
				}
			}.ToString();
		}
		public string getStartUpChunkInfo()
		{
			switch ((234927485 == 234927485) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			return new JObject
			{

				{
					"chunkId",
					this.gameStartChunkId
				},

				{
					"availableSince",
					30000
				},

				{
					"nextAvailableChunk",
					this.gameStartChunkId
				},

				{
					"keyFrameId",
					1
				},

				{
					"nextChunkId",
					this.gameStartChunkId
				},

				{
					"endStartupChunkId",
					this.gameEndStartupChunkId
				},

				{
					"startGameChunkId",
					this.gameStartChunkId
				},

				{
					"endGameChunkId",
					0
				},

				{
					"duration",
					this.gameLastChunkDuration
				}
			}.ToString();
		}
		private void eval_a()
		{
			switch ((1767035118 == 1767035118) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			this.gameKeyFrames = new System.Collections.Generic.Dictionary<int, byte[]>();
			this.gameChunks = new System.Collections.Generic.Dictionary<int, byte[]>();
		}
		public void setKeyFrameContent(int n, byte[] contentBytes)
		{
			int num = 0;
			while (true)
			{
				switch (num)
				{
				case 0:
					switch (0)
					{
					case 0:
						goto IL_25;
					}
					continue;
				case 1:
					this.gameKeyFrames.Remove(n);
					num = 2;
					continue;
				case 2:
					goto IL_90;
				}
				IL_25:
				int arg_29_0 = 0;
				if (this.gameKeyFrames.ContainsKey(n))
				{
					switch ((986886827 == 986886827) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_92;
					case 1:
						IL_5E:
						if (1 != 0)
						{
						}
						if (0 != 0)
						{
						}
						num = 1;
						continue;
					}
					goto IL_5E;
				}
				break;
			}
			IL_90:
			IL_92:
			this.gameKeyFrames.Add(n, contentBytes);
		}
		public byte[] getKeyFrameContent(int n)
		{
			while (this.gameKeyFrames.ContainsKey(n))
			{
				int arg_14_0 = 0;
				if (1 != 0)
				{
				}
				switch ((1422905743 == 1422905743) ? 1 : 0)
				{
				case 0:
				case 2:
					continue;
				case 1:
					IL_45:
					if (0 != 0)
					{
					}
					return this.gameKeyFrames[n];
				}
				goto IL_45;
			}
			return null;
		}
		public void setChunkContent(int n, byte[] contentBytes)
		{
			int num = 2;
			while (true)
			{
				switch (num)
				{
				case 0:
					goto IL_90;
				case 1:
					if (1 != 0)
					{
					}
					this.gameChunks.Remove(n);
					num = 0;
					continue;
				case 2:
					switch (0)
					{
					case 0:
						goto IL_25;
					}
					continue;
				}
				IL_25:
				int arg_29_0 = 0;
				if (this.gameChunks.ContainsKey(n))
				{
					switch ((1833864743 == 1833864743) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_92;
					case 1:
						IL_5E:
						if (0 != 0)
						{
						}
						num = 1;
						continue;
					}
					goto IL_5E;
				}
				break;
			}
			IL_90:
			IL_92:
			this.gameChunks.Add(n, contentBytes);
		}
		public byte[] getChunkContent(int n)
		{
			while (this.gameChunks.ContainsKey(n))
			{
				int arg_14_0 = 0;
				switch ((1204543208 == 1204543208) ? 1 : 0)
				{
				case 0:
				case 2:
					continue;
				case 1:
					IL_3B:
					if (1 != 0)
					{
					}
					if (0 != 0)
					{
					}
					return this.gameChunks[n];
				}
				goto IL_3B;
			}
			return null;
		}
		public void setAllChunksContent(System.Collections.Generic.Dictionary<int, byte[]> chunks)
		{
			switch ((1629058040 == 1629058040) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			this.gameChunks = chunks;
		}
		public void setAllKeyFrameContent(System.Collections.Generic.Dictionary<int, byte[]> keyFrames)
		{
			switch ((1832351951 == 1832351951) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			this.gameKeyFrames = keyFrames;
		}
		public void lastChunkInfoAnalyze(JObject lastChunkJson)
		{
			switch ((465717397 == 465717397) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			this.gameLastChunkTime = int.Parse(lastChunkJson["availableSince"].ToString());
			this.gameLastChunkDuration = int.Parse(lastChunkJson["duration"].ToString());
		}
		public void setEndOfGameStats(byte[] content)
		{
			int num = 9;
			while (true)
			{
				switch (num)
				{
				case 0:
					goto IL_238;
				case 1:
					if (this.gameStats.Players.Count > 0)
					{
						num = 8;
						continue;
					}
					goto IL_245;
				case 2:
					if (!this.gameStats.DecodeData())
					{
						num = 10;
						continue;
					}
					num = 6;
					continue;
				case 3:
					goto IL_16A;
				case 4:
				{
					int num2;
					if (num2 >= this.gameStats.Players.Count)
					{
						num = 3;
						continue;
					}
					this.players[num2] = new PlayerInfo();
					this.players[num2].championName = this.gameStats.Players[num2].SkinName;
					this.players[num2].playerName = this.gameStats.Players[num2].SummonerName;
					this.players[num2].team = this.gameStats.Players[num2].TeamId;
					this.players[num2].clientID = num2;
					num2++;
					num = 5;
					continue;
				}
				case 5:
					goto IL_13C;
				case 6:
					if (this.gameStats.Players != null)
					{
						num = 0;
						continue;
					}
					goto IL_245;
				case 7:
					goto IL_58;
				case 8:
				{
					this.players = new PlayerInfo[this.gameStats.Players.Count];
					int num2 = 0;
					num = 11;
					continue;
				}
				case 9:
					switch (0)
					{
					case 0:
						goto IL_49;
					}
					continue;
				case 10:
					goto IL_137;
				case 11:
					goto IL_13C;
				}
				IL_49:
				if (content == null)
				{
					num = 7;
					continue;
				}
				this.eval_a = content;
				this.gameStats = new EndOfGameStats(this.eval_a);
				switch ((2088238356 == 2088238356) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_238;
				case 1:
					IL_108:
					if (0 != 0)
					{
					}
					num = 2;
					continue;
				}
				goto IL_108;
				IL_13C:
				num = 4;
				continue;
				IL_238:
				num = 1;
			}
			IL_58:
			int arg_5C_0 = 0;
			if (1 != 0)
			{
			}
			this.hasResult = false;
			return;
			IL_137:
			this.hasResult = false;
			return;
			IL_16A:
			IL_245:
			this.hasResult = true;
		}
		public void writeToFile(string path)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_54;
				}
				System.IO.BinaryWriter binaryWriter;
				while (true)
				{
					IL_19:
					System.Collections.Generic.Dictionary<int, byte[]>.Enumerator enumerator;
					switch (num)
					{
					case 0:
						goto IL_4A9;
					case 1:
						if (this.players != null)
						{
							num = 9;
							continue;
						}
						binaryWriter.Write(false);
						num = 0;
						continue;
					case 2:
					{
						try
						{
							num = 2;
							while (true)
							{
								switch (num)
								{
								case 0:
									num = 4;
									continue;
								case 1:
								{
									if (!enumerator.MoveNext())
									{
										num = 0;
										continue;
									}
									System.Collections.Generic.KeyValuePair<int, byte[]> current = enumerator.Current;
									binaryWriter.Write(current.Key);
									binaryWriter.Write(current.Value.Length);
									binaryWriter.Write(current.Value);
									num = 3;
									continue;
								}
								case 2:
									switch (0)
									{
									case 0:
										goto IL_36B;
									}
									continue;
								case 4:
									goto IL_3D7;
								}
								IL_3AB:
								num = 1;
								continue;
								IL_36B:
								goto IL_3AB;
							}
							IL_3D7:
							goto IL_479;
						}
						finally
						{
							((System.IDisposable)enumerator).Dispose();
						}
						goto IL_3EA;
						IL_479:
						binaryWriter.Write(this.gameChunks.Count);
						System.Collections.Generic.Dictionary<int, byte[]>.Enumerator enumerator2 = this.gameChunks.GetEnumerator();
						num = 4;
						continue;
					}
					case 3:
						goto IL_3EA;
					case 4:
						try
						{
							num = 0;
							while (true)
							{
								switch (num)
								{
								case 0:
									switch (0)
									{
									case 0:
										goto IL_293;
									}
									continue;
								case 1:
									num = 3;
									continue;
								case 2:
								{
									System.Collections.Generic.Dictionary<int, byte[]>.Enumerator enumerator2;
									if (!enumerator2.MoveNext())
									{
										num = 1;
										continue;
									}
									System.Collections.Generic.KeyValuePair<int, byte[]> current2 = enumerator2.Current;
									binaryWriter.Write(current2.Key);
									binaryWriter.Write(current2.Value.Length);
									binaryWriter.Write(current2.Value);
									num = 4;
									continue;
								}
								case 3:
									goto IL_2FF;
								}
								IL_2D3:
								num = 2;
								continue;
								IL_293:
								goto IL_2D3;
							}
							IL_2FF:
							goto IL_539;
						}
						finally
						{
							System.Collections.Generic.Dictionary<int, byte[]>.Enumerator enumerator2;
							((System.IDisposable)enumerator2).Dispose();
						}
						goto IL_312;
					case 5:
						goto IL_312;
					case 6:
						if (this.hasResult)
						{
							switch ((814881016 == 814881016) ? 1 : 0)
							{
							case 0:
							case 2:
								goto IL_4C7;
							case 1:
								IL_200:
								if (0 != 0)
								{
								}
								num = 12;
								continue;
							}
							goto IL_200;
						}
						goto IL_312;
					case 7:
						goto IL_50F;
					case 8:
						goto IL_4A9;
					case 9:
					{
						binaryWriter.Write(true);
						binaryWriter.Write(this.players.Length);
						PlayerInfo[] array = this.players;
						int num2 = 0;
						num = 7;
						continue;
					}
					case 10:
						goto IL_50F;
					case 11:
					{
						PlayerInfo[] array;
						int num2;
						if (num2 >= array.Length)
						{
							num = 3;
							continue;
						}
						PlayerInfo playerInfo = array[num2];
						char[] array2 = playerInfo.playerName.ToCharArray();
						binaryWriter.Write(array2.Length);
						binaryWriter.Write(array2);
						char[] array3 = playerInfo.championName.ToCharArray();
						binaryWriter.Write(array3.Length);
						binaryWriter.Write(array3);
						binaryWriter.Write(playerInfo.team);
						binaryWriter.Write(playerInfo.clientID);
						num2++;
						num = 10;
						continue;
					}
					case 12:
						binaryWriter.Write(this.eval_a.Length);
						binaryWriter.Write(this.eval_a);
						num = 5;
						continue;
					}
					goto IL_54;
					IL_312:
					num = 1;
					continue;
					IL_3EA:
					int arg_3EE_0 = 0;
					num = 8;
					continue;
					IL_4C7:
					if (1 != 0)
					{
					}
					num = 2;
					continue;
					IL_4A9:
					binaryWriter.Write(this.gameKeyFrames.Count);
					enumerator = this.gameKeyFrames.GetEnumerator();
					goto IL_4C7;
					IL_50F:
					num = 11;
				}
				IL_539:
				binaryWriter.Close();
				System.IO.FileStream fileStream;
				fileStream.Close();
				this.relatedFileName = path;
				return;
				IL_54:
				fileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
				binaryWriter = new System.IO.BinaryWriter(fileStream);
				binaryWriter.Write(4);
				binaryWriter.Write(this.spectatorClientVersion.Length);
				binaryWriter.Write(this.spectatorClientVersion);
				binaryWriter.Write(this.gameId);
				binaryWriter.Write(this.gameEndStartupChunkId);
				binaryWriter.Write(this.gameStartChunkId);
				binaryWriter.Write(this.gameEndChunkId);
				binaryWriter.Write(this.gameEndKeyFrameId);
				binaryWriter.Write(this.gameLength);
				binaryWriter.Write(this.gameDelayTime);
				binaryWriter.Write(this.gameClientAddLag);
				binaryWriter.Write(this.gameChunkTimeInterval);
				binaryWriter.Write(this.gameKeyFrameTimeInterval);
				binaryWriter.Write(this.gameELOLevel);
				binaryWriter.Write(this.gameLastChunkTime);
				binaryWriter.Write(this.gameLastChunkDuration);
				binaryWriter.Write(this.gamePlatform.Length);
				binaryWriter.Write(this.gamePlatform);
				binaryWriter.Write(this.observerEncryptionKey.Length);
				binaryWriter.Write(this.observerEncryptionKey);
				binaryWriter.Write(this.gameCreateTime.Length);
				binaryWriter.Write(this.gameCreateTime);
				binaryWriter.Write(this.gameStartTime.Length);
				binaryWriter.Write(this.gameStartTime);
				binaryWriter.Write(this.gameEndTime.Length);
				binaryWriter.Write(this.gameEndTime);
				binaryWriter.Write(this.lolVersion.Length);
				binaryWriter.Write(this.lolVersion);
				binaryWriter.Write(this.hasResult);
				num = 6;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		public void readFromFile(string path, bool withOutChunks)
		{
			int arg_04_0 = 0;
			switch ((495054875 == 495054875) ? 1 : 0)
			{
			case 0:
			case 2:
				goto IL_4C8;
			case 1:
				IL_2B:
				if (0 != 0)
				{
				}
				this.relatedFileName = path;
				try
				{
					switch (0)
					{
					case 0:
						goto IL_C3;
					}
					int num;
					System.IO.BinaryReader binaryReader;
					System.IO.FileStream fileStream;
					while (true)
					{
						IL_45:
						int count;
						switch (num)
						{
						case 0:
							num = 10;
							continue;
						case 1:
							if (this.ThisLPRVersion >= 2)
							{
								num = 26;
								continue;
							}
							this.readPlayerOldFormat(binaryReader);
							num = 8;
							continue;
						case 2:
							if (this.ThisLPRVersion >= 4)
							{
								num = 0;
								continue;
							}
							num = 21;
							continue;
						case 3:
							count = binaryReader.ReadInt32();
							this.spectatorClientVersion = binaryReader.ReadChars(count);
							num = 23;
							continue;
						case 4:
							this.eval_b(binaryReader);
							num = 14;
							continue;
						case 5:
							goto IL_1F0;
						case 6:
							goto IL_29C;
						case 7:
							goto IL_29C;
						case 8:
							goto IL_1F0;
						case 9:
							if (binaryReader.ReadBoolean())
							{
								num = 12;
								continue;
							}
							goto IL_1F0;
						case 10:
							if (this.hasResult)
							{
								num = 19;
								continue;
							}
							goto IL_1AA;
						case 11:
							if (this.ThisLPRVersion >= 0)
							{
								num = 3;
								continue;
							}
							goto IL_4B0;
						case 12:
							this.readPlayerOldFormat(binaryReader);
							num = 28;
							continue;
						case 13:
							goto IL_4BC;
						case 14:
							goto IL_20D;
						case 15:
							if (!withOutChunks)
							{
								num = 4;
								continue;
							}
							goto IL_20D;
						case 16:
							goto IL_1AA;
						case 17:
							goto IL_3B9;
						case 18:
							goto IL_3B9;
						case 19:
							count = binaryReader.ReadInt32();
							this.eval_a = binaryReader.ReadBytes(count);
							this.gameStats = new EndOfGameStats(this.eval_a);
							num = 16;
							continue;
						case 20:
							this.gameId = (ulong)((long)binaryReader.ReadInt32());
							num = 7;
							continue;
						case 21:
							if (this.hasResult)
							{
								num = 24;
								continue;
							}
							this.readPlayerOldFormat(binaryReader);
							num = 5;
							continue;
						case 22:
							goto IL_4B0;
						case 23:
							if (this.ThisLPRVersion < 2)
							{
								num = 20;
								continue;
							}
							this.gameId = binaryReader.ReadUInt64();
							num = 6;
							continue;
						case 24:
							count = binaryReader.ReadInt32();
							this.eval_a = binaryReader.ReadBytes(count);
							this.gameStats = new EndOfGameStats(this.eval_a);
							num = 27;
							continue;
						case 25:
							count = binaryReader.ReadInt32();
							this.lolVersion = binaryReader.ReadChars(count);
							num = 18;
							continue;
						case 26:
							this.hasResult = binaryReader.ReadBoolean();
							num = 2;
							continue;
						case 27:
							goto IL_1F0;
						case 28:
							goto IL_1F0;
						case 29:
							if (this.ThisLPRVersion >= 3)
							{
								num = 25;
								continue;
							}
							this.lolVersion = string.Empty.ToCharArray();
							num = 17;
							continue;
						}
						goto IL_C3;
						IL_1AA:
						num = 9;
						continue;
						IL_1F0:
						num = 15;
						continue;
						IL_20D:
						binaryReader.Close();
						fileStream.Close();
						num = 22;
						continue;
						IL_29C:
						this.gameEndStartupChunkId = binaryReader.ReadInt32();
						this.gameStartChunkId = binaryReader.ReadInt32();
						this.gameEndChunkId = binaryReader.ReadInt32();
						this.gameEndKeyFrameId = binaryReader.ReadInt32();
						this.gameLength = binaryReader.ReadInt32();
						this.gameDelayTime = binaryReader.ReadInt32();
						this.gameClientAddLag = binaryReader.ReadInt32();
						this.gameChunkTimeInterval = binaryReader.ReadInt32();
						this.gameKeyFrameTimeInterval = binaryReader.ReadInt32();
						this.gameELOLevel = binaryReader.ReadInt32();
						this.gameLastChunkTime = binaryReader.ReadInt32();
						this.gameLastChunkDuration = binaryReader.ReadInt32();
						count = binaryReader.ReadInt32();
						this.gamePlatform = binaryReader.ReadChars(count);
						count = binaryReader.ReadInt32();
						this.observerEncryptionKey = binaryReader.ReadChars(count);
						count = binaryReader.ReadInt32();
						this.gameCreateTime = binaryReader.ReadChars(count);
						count = binaryReader.ReadInt32();
						this.gameStartTime = binaryReader.ReadChars(count);
						count = binaryReader.ReadInt32();
						this.gameEndTime = binaryReader.ReadChars(count);
						num = 29;
						continue;
						IL_3B9:
						num = 1;
						continue;
						IL_4B0:
						num = 13;
					}
					IL_4BC:
					goto IL_4C8;
					IL_C3:
					fileStream = new System.IO.FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read);
					binaryReader = new System.IO.BinaryReader(fileStream);
					this.ThisLPRVersion = binaryReader.ReadInt32();
					num = 11;
					goto IL_45;
				}
				catch
				{
					this.IsBroken = true;
				}
				goto IL_4C8;
			}
			goto IL_2B;
			IL_4C8:
			if (1 != 0)
			{
			}
		}
		public void readPlayerOldFormat(System.IO.BinaryReader dataReader)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_3C;
				}
				int i;
				while (true)
				{
					IL_19:
					int arg_1D_0 = 0;
					switch (num)
					{
					case 0:
						return;
					case 1:
						goto IL_5B;
					case 2:
						while (i < this.players.Length)
						{
							if (1 != 0)
							{
							}
							switch ((51644167 == 51644167) ? 1 : 0)
							{
							case 0:
							case 2:
								continue;
							case 1:
							{
								IL_AB:
								if (0 != 0)
								{
								}
								this.players[i] = new PlayerInfo();
								int count = dataReader.ReadInt32();
								char[] value = dataReader.ReadChars(count);
								this.players[i].playerName = new string(value);
								count = dataReader.ReadInt32();
								char[] value2 = dataReader.ReadChars(count);
								this.players[i].championName = new string(value2);
								this.players[i].team = dataReader.ReadUInt32();
								this.players[i].clientID = dataReader.ReadInt32();
								i++;
								num = 1;
								goto IL_19;
							}
							}
							goto IL_AB;
						}
						num = 0;
						continue;
					case 3:
						goto IL_5B;
					}
					goto IL_3C;
					IL_5B:
					num = 2;
				}
				return;
				IL_3C:
				this.players = new PlayerInfo[dataReader.ReadInt32()];
				i = 0;
				num = 3;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		private void eval_b(System.IO.BinaryReader A_0)
		{
			int num = 0;
			switch (num)
			{
			case 0:
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_48;
				}
				while (true)
				{
					IL_19:
					int num3;
					int num4;
					switch (num)
					{
					case 0:
						goto IL_6F;
					case 1:
					{
						if (1 != 0)
						{
						}
						int num2;
						if (num2 >= num3)
						{
							num = 0;
							continue;
						}
						int key = A_0.ReadInt32();
						int count = A_0.ReadInt32();
						this.gameKeyFrames.Add(key, A_0.ReadBytes(count));
						num2++;
						num = 9;
						continue;
					}
					case 2:
						goto IL_6A;
					case 3:
					{
						if (num4 >= num3)
						{
							int arg_103_0 = 0;
							num = 5;
							continue;
						}
						int key2 = A_0.ReadInt32();
						int count2 = A_0.ReadInt32();
						this.gameChunks.Add(key2, A_0.ReadBytes(count2));
						num4++;
						num = 7;
						continue;
					}
					case 4:
						goto IL_85;
					case 5:
						return;
					case 6:
					{
						if (this.ThisLPRVersion == 0)
						{
							num = 2;
							continue;
						}
						num3 = A_0.ReadInt32();
						int num2 = 0;
						switch ((886932465 == 886932465) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_6F;
						case 1:
							IL_D7:
							if (0 != 0)
							{
							}
							num = 4;
							continue;
						}
						goto IL_D7;
					}
					case 7:
						goto IL_ED;
					case 8:
						goto IL_ED;
					case 9:
						goto IL_85;
					}
					goto IL_48;
					IL_6F:
					num3 = A_0.ReadInt32();
					num4 = 0;
					num = 8;
					continue;
					IL_85:
					num = 1;
					continue;
					IL_ED:
					num = 3;
				}
				IL_6A:
				this.eval_a(A_0);
				return;
				IL_48:
				this.eval_a();
				num = 6;
				goto IL_19;
			}
			goto IL_0F;
		}
		private void eval_a(System.IO.BinaryReader A_0)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_50;
				}
				int num2;
				while (true)
				{
					IL_19:
					switch (num)
					{
					case 0:
					{
						if (num2 >= this.gameEndKeyFrameId)
						{
							num = 4;
							continue;
						}
						int count = A_0.ReadInt32();
						this.gameKeyFrames.Add(num2 + 1, A_0.ReadBytes(count));
						num2++;
						num = 2;
						continue;
					}
					case 1:
					{
						int num3;
						if (num3 > this.gameEndChunkId - this.gameStartChunkId)
						{
							num = 11;
							continue;
						}
						switch ((1851125250 == 1851125250) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_1C8;
						case 1:
						{
							IL_149:
							if (0 != 0)
							{
							}
							int count = A_0.ReadInt32();
							this.gameChunks.Add(this.gameStartChunkId + num3, A_0.ReadBytes(count));
							num3++;
							num = 3;
							continue;
						}
						}
						goto IL_149;
					}
					case 2:
					{
						if (1 != 0)
						{
						}
						int arg_11D_0 = 0;
						goto IL_8E;
					}
					case 3:
						goto IL_B3;
					case 4:
					{
						int num4 = 0;
						num = 7;
						continue;
					}
					case 5:
						goto IL_8E;
					case 6:
						goto IL_6C;
					case 7:
						goto IL_1C8;
					case 8:
					{
						int num4;
						if (num4 >= this.gameEndStartupChunkId)
						{
							num = 9;
							continue;
						}
						int count = A_0.ReadInt32();
						this.gameChunks.Add(num4 + 1, A_0.ReadBytes(count));
						num4++;
						num = 6;
						continue;
					}
					case 9:
					{
						int num3 = 0;
						num = 10;
						continue;
					}
					case 10:
						goto IL_B3;
					case 11:
						return;
					}
					goto IL_50;
					IL_6C:
					num = 8;
					continue;
					IL_8E:
					num = 0;
					continue;
					IL_B3:
					num = 1;
					continue;
					IL_1C8:
					goto IL_6C;
				}
				return;
				IL_50:
				num2 = 0;
				num = 5;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		private static string[] eval_b(EndOfGameStats A_0)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_48;
				}
				string[] array;
				int num3;
				int num4;
				while (true)
				{
					IL_19:
					int num2;
					switch (num)
					{
					case 0:
						goto IL_B0;
					case 1:
						return array;
					case 2:
						goto IL_B0;
					case 3:
						switch ((275778386 == 275778386) ? 1 : 0)
						{
						case 0:
						case 2:
							return array;
						case 1:
							IL_89:
							if (0 != 0)
							{
							}
							if (A_0.Players.Count > 0)
							{
								num = 9;
								continue;
							}
							return array;
						}
						goto IL_89;
					case 4:
						goto IL_FB;
					case 5:
						if (A_0.Players[num2].TeamId == 100u)
						{
							num = 8;
							continue;
						}
						array[num3++] = A_0.Players[num2].SkinName;
						if (1 != 0)
						{
						}
						num = 0;
						continue;
					case 6:
						if (num2 >= A_0.Players.Count)
						{
							num = 1;
							continue;
						}
						num = 5;
						continue;
					case 7:
						goto IL_FB;
					case 8:
						array[num4--] = A_0.Players[num2].SkinName;
						num = 2;
						continue;
					case 9:
						num2 = 0;
						num = 7;
						continue;
					}
					goto IL_48;
					IL_B0:
					num2++;
					num = 4;
					continue;
					IL_FB:
					num = 6;
				}
				return array;
				IL_48:
				array = new string[10];
				num3 = 5;
				num4 = 4;
				int arg_58_0 = 0;
				num = 3;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		private static string[] eval_a(EndOfGameStats A_0)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_48;
				}
				string[] array;
				int num3;
				int num4;
				while (true)
				{
					IL_19:
					int num2;
					switch (num)
					{
					case 0:
						if (num2 >= A_0.Players.Count)
						{
							num = 2;
							continue;
						}
						num = 3;
						continue;
					case 1:
						goto IL_B0;
					case 2:
						return array;
					case 3:
						if (A_0.Players[num2].TeamId == 100u)
						{
							num = 4;
							continue;
						}
						array[num3++] = A_0.Players[num2].SummonerName;
						num = 7;
						continue;
					case 4:
						array[num4--] = A_0.Players[num2].SummonerName;
						num = 1;
						continue;
					case 5:
						goto IL_FB;
					case 6:
						switch ((1697148261 == 1697148261) ? 1 : 0)
						{
						case 0:
						case 2:
							return array;
						case 1:
							IL_89:
							if (0 != 0)
							{
							}
							if (A_0.Players.Count > 0)
							{
								num = 8;
								continue;
							}
							return array;
						}
						goto IL_89;
					case 7:
						if (1 != 0)
						{
						}
						goto IL_B0;
					case 8:
						num2 = 0;
						num = 9;
						continue;
					case 9:
						goto IL_FB;
					}
					goto IL_48;
					IL_B0:
					num2++;
					num = 5;
					continue;
					IL_FB:
					num = 0;
				}
				return array;
				IL_48:
				array = new string[10];
				num3 = 5;
				num4 = 4;
				int arg_58_0 = 0;
				num = 6;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		private static string[] eval_b(PlayerInfo[] A_0)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch ((445225893 == 445225893) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_127:
					goto IL_96;
				case 1:
					IL_2E:
					if (0 != 0)
					{
					}
					switch (0)
					{
					case 0:
						goto IL_72;
					}
					goto IL_3F;
				}
				goto IL_2E;
				int num2;
				string[] array;
				int num3;
				int num4;
				while (true)
				{
					IL_3F:
					int arg_43_0 = 0;
					switch (num)
					{
					case 0:
						if (A_0[num2].team == 100u)
						{
							num = 2;
							continue;
						}
						array[num3++] = A_0[num2].championName;
						num = 4;
						continue;
					case 1:
						return array;
					case 2:
						array[num4--] = A_0[num2].championName;
						num = 5;
						continue;
					case 3:
						goto IL_E9;
					case 4:
						goto IL_BF;
					case 5:
						goto IL_127;
					case 6:
						if (num2 >= A_0.Length)
						{
							num = 1;
							continue;
						}
						num = 0;
						continue;
					case 7:
						goto IL_E9;
					}
					goto IL_72;
					IL_E9:
					num = 6;
				}
				IL_BF:
				goto IL_96;
				IL_72:
				if (1 != 0)
				{
				}
				array = new string[10];
				num3 = 5;
				num4 = 4;
				num2 = 0;
				num = 7;
				goto IL_3F;
				IL_96:
				num2++;
				num = 3;
				goto IL_3F;
			}
			}
			goto IL_0F;
		}
		private static string[] eval_a(PlayerInfo[] A_0)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch ((1059157671 == 1059157671) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_127:
					goto IL_96;
				case 1:
					IL_2E:
					if (0 != 0)
					{
					}
					switch (0)
					{
					case 0:
						goto IL_72;
					}
					goto IL_3F;
				}
				goto IL_2E;
				int num2;
				string[] array;
				int num3;
				int num4;
				while (true)
				{
					IL_3F:
					int arg_43_0 = 0;
					switch (num)
					{
					case 0:
						goto IL_127;
					case 1:
						if (A_0[num2].team == 100u)
						{
							num = 5;
							continue;
						}
						array[num3++] = A_0[num2].playerName;
						num = 7;
						continue;
					case 2:
						goto IL_E9;
					case 3:
						goto IL_E9;
					case 4:
						if (num2 >= A_0.Length)
						{
							num = 6;
							continue;
						}
						num = 1;
						continue;
					case 5:
						array[num4--] = A_0[num2].playerName;
						num = 0;
						continue;
					case 6:
						return array;
					case 7:
						goto IL_BF;
					}
					goto IL_72;
					IL_E9:
					num = 4;
				}
				IL_BF:
				goto IL_96;
				IL_72:
				if (1 != 0)
				{
				}
				array = new string[10];
				num3 = 5;
				num4 = 4;
				num2 = 0;
				num = 3;
				goto IL_3F;
				IL_96:
				num2++;
				num = 2;
				goto IL_3F;
			}
			}
			goto IL_0F;
		}
		public static SimpleLoLRecord GetSimpleLoLRecord(string path)
		{
			switch ((1827155537 == 1827155537) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			int arg_23_0 = 0;
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			LoLRecord loLRecord = new LoLRecord();
			loLRecord.readFromFile(path, true);
			return LoLRecord.GetSimpleLoLRecord(loLRecord);
		}
		public static SimpleLoLRecord GetSimpleLoLRecord(LoLRecord record)
		{
			int num = 0;
			SimpleLoLRecord simpleLoLRecord;
			while (true)
			{
				int arg_17_0 = 0;
				switch (num)
				{
				case 0:
					goto IL_09;
				case 1:
					goto IL_4F;
				case 2:
					try
					{
						num = 4;
						while (true)
						{
							switch (num)
							{
							case 0:
								simpleLoLRecord.Champions = LoLRecord.eval_b(record.gameStats);
								simpleLoLRecord.SummonerName = LoLRecord.eval_a(record.gameStats);
								num = 3;
								continue;
							case 1:
								goto IL_F1;
							case 2:
								goto IL_FD;
							case 3:
								goto IL_F1;
							case 4:
								switch (0)
								{
								case 0:
									goto IL_81;
								}
								continue;
							}
							IL_81:
							if (record.players == null)
							{
								num = 0;
								continue;
							}
							simpleLoLRecord.Champions = LoLRecord.eval_b(record.players);
							simpleLoLRecord.SummonerName = LoLRecord.eval_a(record.players);
							num = 1;
							continue;
							IL_F1:
							num = 2;
						}
						IL_FD:
						goto IL_106;
					}
					catch (System.Exception)
					{
						return null;
					}
					goto IL_104;
					IL_106:
					switch ((1504244400 == 1504244400) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_09;
					}
					goto IL_125;
				}
				goto IL_31;
				IL_09:
				switch (0)
				{
				case 0:
					IL_31:
					if (1 != 0)
					{
					}
					if (record.IsBroken)
					{
						num = 1;
					}
					else
					{
						simpleLoLRecord = new SimpleLoLRecord();
						simpleLoLRecord.FileName = record.relatedFileName;
						num = 2;
					}
					break;
				}
			}
			IL_4F:
			IL_104:
			return null;
			IL_125:
			if (0 != 0)
			{
			}
			return simpleLoLRecord;
		}
	}
}
