using BaronReplays.Properties;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;
namespace BaronReplays
{
	public class FeaturedGamesView : UserControl, IComponentConnector
	{
		private string _platform;
		private FeaturedGameJson[] _gameData;
		private DispatcherTimer _gameLengthTimer;
		private Storyboard _loadingStoryBoard;
		private WebClient _httpClient;
		private int _nth;
		internal Rectangle LoadingLine;
		internal WrapPanel RegionsButton;
		internal SingleFeaturedGameView Game;
		internal StackPanel SelectPoints;
		private bool _contentLoaded;
		public event MainWindow.StartNewRecodingDelegate FeatureGameClicked
		{
			add
			{
				switch (0)
				{
				case 0:
					goto IL_4E;
				}
				int num;
				MainWindow.StartNewRecodingDelegate startNewRecodingDelegate;
				while (true)
				{
					IL_0A:
					switch ((214683517 == 214683517) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_4E;
					case 1:
						break;
					default:
					{
						int arg_2C_0 = 0;
						break;
					}
					}
					if (0 != 0)
					{
					}
					MainWindow.StartNewRecodingDelegate startNewRecodingDelegate2;
					switch (num)
					{
					case 0:
						return;
					case 1:
						if (startNewRecodingDelegate == startNewRecodingDelegate2)
						{
							if (1 != 0)
							{
							}
							num = 0;
							continue;
						}
						goto IL_60;
					case 2:
						goto IL_60;
					}
					goto IL_4E;
					IL_60:
					startNewRecodingDelegate2 = startNewRecodingDelegate;
					MainWindow.StartNewRecodingDelegate value2 = (MainWindow.StartNewRecodingDelegate)System.Delegate.Combine(startNewRecodingDelegate2, value);
					startNewRecodingDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.StartNewRecodingDelegate>(ref this.FeatureGameClicked, value2, startNewRecodingDelegate2);
					num = 1;
				}
				return;
				IL_4E:
				startNewRecodingDelegate = this.FeatureGameClicked;
				num = 2;
				goto IL_0A;
			}
			remove
			{
				switch (0)
				{
				case 0:
					goto IL_58;
				}
				int num;
				MainWindow.StartNewRecodingDelegate startNewRecodingDelegate;
				while (true)
				{
					IL_0A:
					if (1 != 0)
					{
					}
					int arg_18_0 = 0;
					switch ((2090501050 == 2090501050) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_58;
					case 1:
					{
						IL_3F:
						if (0 != 0)
						{
						}
						MainWindow.StartNewRecodingDelegate startNewRecodingDelegate2;
						switch (num)
						{
						case 0:
							return;
						case 1:
							goto IL_6A;
						case 2:
							if (startNewRecodingDelegate == startNewRecodingDelegate2)
							{
								num = 0;
								continue;
							}
							goto IL_6A;
						}
						goto IL_58;
						IL_6A:
						startNewRecodingDelegate2 = startNewRecodingDelegate;
						MainWindow.StartNewRecodingDelegate value2 = (MainWindow.StartNewRecodingDelegate)System.Delegate.Remove(startNewRecodingDelegate2, value);
						startNewRecodingDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.StartNewRecodingDelegate>(ref this.FeatureGameClicked, value2, startNewRecodingDelegate2);
						num = 2;
						continue;
					}
					}
					goto IL_3F;
				}
				return;
				IL_58:
				startNewRecodingDelegate = this.FeatureGameClicked;
				num = 1;
				goto IL_0A;
			}
		}
		public event MainWindow.PlayGameDelegate FeatureGamePlayOnlyClicked
		{
			add
			{
				switch (0)
				{
				case 0:
					goto IL_58;
				}
				int num;
				MainWindow.PlayGameDelegate playGameDelegate;
				while (true)
				{
					IL_0A:
					switch ((735201507 == 735201507) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_58;
					case 1:
						break;
					default:
					{
						int arg_2C_0 = 0;
						break;
					}
					}
					if (0 != 0)
					{
					}
					if (1 != 0)
					{
					}
					MainWindow.PlayGameDelegate playGameDelegate2;
					switch (num)
					{
					case 0:
						if (playGameDelegate == playGameDelegate2)
						{
							num = 2;
							continue;
						}
						goto IL_6A;
					case 1:
						goto IL_6A;
					case 2:
						return;
					}
					goto IL_58;
					IL_6A:
					playGameDelegate2 = playGameDelegate;
					MainWindow.PlayGameDelegate value2 = (MainWindow.PlayGameDelegate)System.Delegate.Combine(playGameDelegate2, value);
					playGameDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.PlayGameDelegate>(ref this.FeatureGamePlayOnlyClicked, value2, playGameDelegate2);
					num = 0;
				}
				return;
				IL_58:
				playGameDelegate = this.FeatureGamePlayOnlyClicked;
				num = 1;
				goto IL_0A;
			}
			remove
			{
				switch (0)
				{
				case 0:
					goto IL_4E;
				}
				int num;
				MainWindow.PlayGameDelegate playGameDelegate;
				while (true)
				{
					IL_0A:
					switch ((2089413225 == 2089413225) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_4E;
					case 1:
						break;
					default:
					{
						int arg_2C_0 = 0;
						break;
					}
					}
					if (0 != 0)
					{
					}
					MainWindow.PlayGameDelegate playGameDelegate2;
					switch (num)
					{
					case 0:
						if (1 != 0)
						{
						}
						goto IL_6A;
					case 1:
						return;
					case 2:
						if (playGameDelegate == playGameDelegate2)
						{
							num = 1;
							continue;
						}
						goto IL_6A;
					}
					goto IL_4E;
					IL_6A:
					playGameDelegate2 = playGameDelegate;
					MainWindow.PlayGameDelegate value2 = (MainWindow.PlayGameDelegate)System.Delegate.Remove(playGameDelegate2, value);
					playGameDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.PlayGameDelegate>(ref this.FeatureGamePlayOnlyClicked, value2, playGameDelegate2);
					num = 2;
				}
				return;
				IL_4E:
				playGameDelegate = this.FeatureGamePlayOnlyClicked;
				num = 0;
				goto IL_0A;
			}
		}
		public FeaturedGameJson NowDisplay
		{
			get
			{
				int num = 2;
				while (true)
				{
					switch (num)
					{
					case 0:
						if (this._gameData.Length == 0)
						{
							num = 3;
							continue;
						}
						goto IL_9D;
					case 1:
						goto IL_76;
					case 2:
						switch (0)
						{
						case 0:
							goto IL_29;
						}
						continue;
					case 3:
						goto IL_99;
					}
					IL_29:
					if (this._gameData == null)
					{
						int arg_35_0 = 0;
						switch ((202558787 == 202558787) ? 1 : 0)
						{
						case 0:
						case 2:
							continue;
						case 1:
							IL_5C:
							if (1 != 0)
							{
							}
							if (0 != 0)
							{
							}
							num = 1;
							continue;
						}
						goto IL_5C;
					}
					num = 0;
				}
				IL_76:
				return null;
				IL_99:
				return null;
				IL_9D:
				return this._gameData[this._nth];
			}
		}
		public FeaturedGamesView()
		{
			this._httpClient = new WebClient();
			this.InitializeComponent();
		}
		private void eval_f()
		{
			if (1 != 0)
			{
			}
			int arg_0E_0 = 0;
			switch ((441729074 == 441729074) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_34:
				break;
			case 1:
				goto IL_35;
			}
			goto IL_34;
			IL_35:
			if (0 != 0)
			{
			}
			DoubleAnimation doubleAnimation = new DoubleAnimation();
			doubleAnimation.From = new double?(0.0);
			doubleAnimation.To = new double?(base.ActualWidth);
			doubleAnimation.DecelerationRatio = 1.0;
			doubleAnimation.Duration = new Duration(System.TimeSpan.FromSeconds(10.0));
			Storyboard.SetTargetName(doubleAnimation, "Width");
			Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath(FrameworkElement.WidthProperty));
			this._loadingStoryBoard = new Storyboard();
			this._loadingStoryBoard.FillBehavior = FillBehavior.Stop;
			this._loadingStoryBoard.Children.Add(doubleAnimation);
			this._loadingStoryBoard.Completed += new System.EventHandler(this.eval_b);
			base.RegisterName("Width", this.LoadingLine);
			this.LoadingLine.Visibility = Visibility.Visible;
			this._loadingStoryBoard.Begin(this.LoadingLine, true);
		}
		private void eval_e()
		{
			switch ((1296634189 == 1296634189) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_1E:
				int arg_22_0 = 0;
				goto IL_2B;
			}
			case 1:
				goto IL_2B;
			}
			goto IL_1E;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this._loadingStoryBoard.Stop(this.LoadingLine);
			this.LoadingLine.Visibility = Visibility.Collapsed;
		}
		private void eval_b(object A_0, System.EventArgs A_1)
		{
			if (1 != 0)
			{
			}
			int arg_0E_0 = 0;
			switch ((509279129 == 509279129) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_34:
				break;
			case 1:
				goto IL_35;
			}
			goto IL_34;
			IL_35:
			if (0 != 0)
			{
			}
			this.eval_e();
			this.eval_d();
		}
		private void eval_d()
		{
			int num = 0;
			while (true)
			{
				switch (num)
				{
				case 0:
					switch ((742510464 == 742510464) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					case 1:
					{
						IL_28:
						int arg_2C_0 = 0;
						if (0 != 0)
						{
						}
						switch (0)
						{
						case 0:
							goto IL_57;
						}
						continue;
					}
					}
					goto IL_28;
				case 1:
					return;
				case 2:
					this._httpClient.CancelAsync();
					num = 1;
					continue;
				}
				IL_57:
				if (1 != 0)
				{
				}
				if (!this._httpClient.IsBusy)
				{
					break;
				}
				num = 2;
			}
		}
		private FeaturedGameJson[] eval_b(string A_0)
		{
			try
			{
				switch ((1256345742 == 1256345742) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_1E:
					break;
				case 1:
					goto IL_1F;
				}
				goto IL_1E;
				IL_1F:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				string uriString = string.Format("http://{0}/observer-mode/rest/featured", A_0);
				this._httpClient.DownloadDataCompleted += new DownloadDataCompletedEventHandler(this.eval_a);
				this._httpClient.DownloadDataAsync(new Uri(uriString));
			}
			catch (System.Exception)
			{
			}
			int arg_6D_0 = 0;
			return null;
		}
		private void eval_c()
		{
			switch ((2015602531 == 2015602531) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_1E:
				int arg_22_0 = 0;
				goto IL_2B;
			}
			case 1:
				goto IL_2B;
			}
			goto IL_1E;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.Game.Visibility = Visibility.Hidden;
		}
		private void eval_b()
		{
			switch ((1145907236 == 1145907236) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_1E:
				int arg_22_0 = 0;
				goto IL_2B;
			}
			case 1:
				goto IL_2B;
			}
			goto IL_1E;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.Game.Visibility = Visibility.Visible;
		}
		private void eval_a(object A_0, DownloadDataCompletedEventArgs A_1)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_40;
				}
				while (true)
				{
					IL_19:
					switch (num)
					{
					case 0:
					{
						int num2;
						if (num2 >= this._gameData.Length)
						{
							num = 3;
							continue;
						}
						Ellipse ellipse = new Ellipse();
						ellipse.Width = 15.0;
						ellipse.Height = 15.0;
						ellipse.Stroke = new SolidColorBrush(Color.FromRgb(0, 122, 255));
						ellipse.Fill = new SolidColorBrush(Color.FromRgb(255, 255, 255));
						ellipse.Margin = new Thickness(10.0);
						ellipse.Cursor = Cursors.Hand;
						ellipse.MouseLeftButtonUp += new MouseButtonEventHandler(this.eval_a);
						this.SelectPoints.Children.Add(ellipse);
						num2++;
						switch ((2137807347 == 2137807347) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_65;
						case 1:
							IL_164:
							if (0 != 0)
							{
							}
							num = 2;
							continue;
						}
						goto IL_164;
					}
					case 1:
						goto IL_1FD;
					case 2:
						goto IL_1FD;
					case 3:
						num = 5;
						continue;
					case 4:
					{
						if (A_1.Cancelled)
						{
							goto IL_65;
						}
						this.eval_b();
						this.eval_e();
						byte[] result = A_1.Result;
						string @string = System.Text.Encoding.UTF8.GetString(result);
						JObject jObject = JsonConvert.DeserializeObject<JObject>(@string);
						this._gameData = JsonConvert.DeserializeObject<FeaturedGameJson[]>(jObject["gameList"].ToString());
						this._nth = 0;
						int num2 = 0;
						if (1 != 0)
						{
						}
						num = 1;
						continue;
					}
					case 5:
						if (this._gameData.Length == 0)
						{
							num = 7;
							continue;
						}
						goto IL_22A;
					case 6:
						goto IL_6F;
					case 7:
						goto IL_95;
					}
					goto IL_40;
					IL_65:
					num = 6;
					continue;
					IL_1FD:
					num = 0;
				}
				IL_6F:
				int arg_181_0 = 0;
				this.eval_c();
				return;
				IL_95:
				this.eval_c();
				return;
				IL_22A:
				this.eval_a(0);
				return;
				IL_40:
				this.SelectPoints.Children.Clear();
				num = 4;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		private void eval_a(object A_0, MouseButtonEventArgs A_1)
		{
			switch ((1770648394 == 1770648394) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_1E:
				int arg_22_0 = 0;
				goto IL_2B;
			}
			case 1:
				goto IL_2B;
			}
			goto IL_1E;
			IL_2B:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			this.eval_b(this.SelectPoints.Children.IndexOf(A_0 as UIElement));
		}
		private void eval_a()
		{
			switch ((1131137407 == 1131137407) ? 1 : 0)
			{
			case 0:
			case 2:
				return;
			case 1:
				break;
			default:
			{
				int arg_22_0 = 0;
				break;
			}
			}
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.RegionsButton.Children.Clear();
			using (System.Collections.Generic.List<string>.Enumerator enumerator = Utilities.LoLObserveServerList.GetEnumerator())
			{
				int num = 3;
				while (true)
				{
					switch (num)
					{
					case 1:
						num = 4;
						continue;
					case 2:
					{
						if (!enumerator.MoveNext())
						{
							num = 1;
							continue;
						}
						string current = enumerator.Current;
						TriStateButton triStateButton = new TriStateButton();
						triStateButton.Style = (Style)base.FindResource("RegionButtonStyle");
						triStateButton.ContentString = Utilities.GetString(current);
						triStateButton.Click += new RoutedEventHandler(this.eval_f);
						this.RegionsButton.Children.Add(triStateButton);
						num = 0;
						continue;
					}
					case 3:
						switch (0)
						{
						case 0:
							goto IL_86;
						}
						continue;
					case 4:
						goto IL_116;
					}
					IL_E7:
					num = 2;
					continue;
					IL_86:
					goto IL_E7;
				}
				IL_116:;
			}
		}
		private void eval_f(object A_0, RoutedEventArgs A_1)
		{
			switch ((730154180 == 730154180) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_1E:
				int arg_22_0 = 0;
				goto IL_2B;
			}
			case 1:
				goto IL_2B;
			}
			goto IL_1E;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.eval_a(Utilities.LoLObserveServerList[this.RegionsButton.Children.IndexOf(A_0 as UIElement)]);
		}
		private void eval_e(object A_0, RoutedEventArgs A_1)
		{
			switch ((649741916 == 649741916) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_1E:
				int arg_22_0 = 0;
				goto IL_2B;
			}
			case 1:
				goto IL_2B;
			}
			goto IL_1E;
			IL_2B:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			this.eval_a();
			this.eval_a(BaronReplays.Properties.Settings.Default.Platform);
			this._gameLengthTimer = new DispatcherTimer();
			this._gameLengthTimer.Interval = System.TimeSpan.FromSeconds(1.0);
			this._gameLengthTimer.Tick += new System.EventHandler(this.eval_a);
			this._gameLengthTimer.Start();
		}
		private void eval_a(object A_0, System.EventArgs A_1)
		{
			int num = 1;
			while (true)
			{
				switch (num)
				{
				case 0:
					return;
				case 1:
					switch ((1386470064 == 1386470064) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					case 1:
					{
						IL_28:
						int arg_2C_0 = 0;
						if (1 != 0)
						{
						}
						if (0 != 0)
						{
						}
						switch (0)
						{
						case 0:
							goto IL_61;
						}
						continue;
					}
					}
					goto IL_28;
				case 2:
					this.NowDisplay.GameLength += 1u;
					num = 0;
					continue;
				}
				IL_61:
				if (this.NowDisplay == null)
				{
					break;
				}
				num = 2;
			}
		}
		private void eval_d(object A_0, RoutedEventArgs A_1)
		{
			int num = 3;
			LoLRecorder r;
			while (true)
			{
				switch (num)
				{
				case 0:
					return;
				case 1:
				{
					if (1 != 0)
					{
					}
					MessageBoxResult messageBoxResult;
					if (messageBoxResult == MessageBoxResult.Yes)
					{
						num = 4;
						continue;
					}
					num = 6;
					continue;
				}
				case 2:
					switch ((1878135348 == 1878135348) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_67;
					case 1:
						IL_8F:
						if (0 != 0)
						{
						}
						this.FeatureGameClicked(r, false);
						num = 0;
						continue;
					}
					goto IL_8F;
				case 3:
					switch (0)
					{
					case 0:
						goto IL_35;
					}
					continue;
				case 4:
					goto IL_138;
				case 5:
				{
					int arg_4D_0 = 0;
					r = new LoLRecorder(new GameInfo(Utilities.LoLObserveServersIpMapping[this._platform], this.NowDisplay.PlatformId, this.NowDisplay.gameId, this.NowDisplay.observers["encryptionKey"].ToString()));
					MessageBoxResult messageBoxResult = MessageBox.Show(Utilities.GetString("WatchNow"), Utilities.GetString("BR"), MessageBoxButton.YesNo);
					num = 1;
					continue;
				}
				case 6:
				{
					MessageBoxResult messageBoxResult;
					if (messageBoxResult == MessageBoxResult.No)
					{
						goto IL_67;
					}
					return;
				}
				}
				IL_35:
				if (this.FeatureGameClicked != null)
				{
					num = 5;
					continue;
				}
				return;
				IL_67:
				num = 2;
			}
			IL_138:
			this.FeatureGameClicked(r, true);
		}
		private void eval_b(int A_0)
		{
			switch (0)
			{
			case 0:
				goto IL_28;
			}
			int num;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					A_0 = 0;
					num = 5;
					continue;
				case 1:
				{
					int arg_6E_0 = 0;
					if (A_0 >= this._gameData.Length)
					{
						goto IL_81;
					}
					num = 2;
					continue;
				}
				case 2:
					if (A_0 < 0)
					{
						num = 4;
						continue;
					}
					goto IL_105;
				case 3:
					goto IL_CA;
				case 4:
					if (1 != 0)
					{
					}
					switch ((1602504209 == 1602504209) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_81;
					case 1:
						IL_AB:
						if (0 != 0)
						{
						}
						A_0 = this._gameData.Length - 1;
						num = 3;
						continue;
					}
					goto IL_AB;
				case 5:
					goto IL_103;
				}
				goto IL_28;
				IL_81:
				num = 0;
			}
			IL_CA:
			IL_103:
			IL_105:
			this._nth = A_0;
			this.Game.DataContext = this.NowDisplay;
			(this.SelectPoints.Children[this._nth] as Ellipse).Fill = new SolidColorBrush(Color.FromRgb(0, 122, 255));
			return;
			IL_28:
			(this.SelectPoints.Children[this._nth] as Ellipse).Fill = new SolidColorBrush(Color.FromRgb(255, 255, 255));
			num = 1;
			goto IL_0A;
		}
		private void eval_a(int A_0)
		{
			switch ((958089318 == 958089318) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_1E:
				int arg_22_0 = 0;
				goto IL_2B;
			}
			case 1:
				goto IL_2B;
			}
			goto IL_1E;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.eval_b(this._nth + A_0);
		}
		private void eval_c(object A_0, RoutedEventArgs A_1)
		{
			switch ((2019798073 == 2019798073) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_1E:
				int arg_22_0 = 0;
				goto IL_2B;
			}
			case 1:
				goto IL_2B;
			}
			goto IL_1E;
			IL_2B:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			this.eval_a(-1);
		}
		private void eval_b(object A_0, RoutedEventArgs A_1)
		{
			switch ((1314670621 == 1314670621) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_1E:
				int arg_22_0 = 0;
				goto IL_2B;
			}
			case 1:
				goto IL_2B;
			}
			goto IL_1E;
			IL_2B:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			this.eval_a(1);
		}
		private void eval_a(string A_0)
		{
			if (NetworkInterface.GetIsNetworkAvailable())
			{
				int arg_28_0 = 0;
				switch ((1515395418 == 1515395418) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_07;
				case 1:
					IL_4F:
					if (0 != 0)
					{
					}
					if (1 != 0)
					{
					}
					this.eval_d();
					this._platform = A_0;
					this.eval_f();
					this.eval_b(Utilities.LoLObserveServersIpMapping[A_0]);
					return;
				}
				goto IL_4F;
			}
			IL_07:
			MessageBox.Show(Utilities.GetString("NetworkError"), Utilities.GetString("BR"));
		}
		private void eval_a(object A_0, RoutedEventArgs A_1)
		{
			int num = 2;
			while (true)
			{
				switch (num)
				{
				case 0:
					return;
				case 1:
					this.FeatureGamePlayOnlyClicked(new GameInfo(Utilities.LoLObserveServersIpMapping[this._platform], this.NowDisplay.PlatformId, this.NowDisplay.gameId, this.NowDisplay.observers["encryptionKey"].ToString()));
					num = 0;
					continue;
				case 2:
					switch ((695658310 == 695658310) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					case 1:
					{
						IL_28:
						int arg_2C_0 = 0;
						if (0 != 0)
						{
						}
						switch (0)
						{
						case 0:
							goto IL_57;
						}
						continue;
					}
					}
					goto IL_28;
				}
				IL_57:
				if (this.FeatureGamePlayOnlyClicked == null)
				{
					break;
				}
				if (1 != 0)
				{
				}
				num = 1;
			}
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), System.Diagnostics.DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!this._contentLoaded)
			{
				int arg_0F_0 = 0;
				if (1 != 0)
				{
				}
				switch ((880744289 == 880744289) ? 1 : 0)
				{
				case 0:
				case 2:
					return;
				case 1:
				{
					IL_40:
					if (0 != 0)
					{
					}
					this._contentLoaded = true;
					Uri resourceLocator = new Uri("/BaronReplays;component/featuredgamesview.xaml", UriKind.Relative);
					Application.LoadComponent(this, resourceLocator);
					return;
				}
				}
				goto IL_40;
			}
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), System.Diagnostics.DebuggerNonUserCode]
		internal System.Delegate eval_a(System.Type A_0, string A_1)
		{
			if (1 != 0)
			{
			}
			int arg_0E_0 = 0;
			switch ((1930695236 == 1930695236) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_34:
				break;
			case 1:
				goto IL_35;
			}
			goto IL_34;
			IL_35:
			if (0 != 0)
			{
			}
			return System.Delegate.CreateDelegate(A_0, this, A_1);
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), System.Diagnostics.DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (0)
			{
			case 0:
				goto IL_1C;
			}
			while (true)
			{
				IL_0A:
				int num;
				switch (num)
				{
				case 0:
					goto IL_B0;
				case 1:
					goto IL_4D;
				case 2:
					num = 0;
					continue;
				}
				goto IL_1C;
			}
			IL_B0:
			this._contentLoaded = true;
			return;
			IL_1C:
			switch ((1526833578 == 1526833578) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_4D:
				int arg_51_0 = 0;
				switch (connectionId)
				{
				case 1:
					((FeaturedGamesView)target).Loaded += new RoutedEventHandler(this.eval_e);
					return;
				case 2:
					if (1 != 0)
					{
					}
					this.LoadingLine = (Rectangle)target;
					return;
				case 3:
					this.RegionsButton = (WrapPanel)target;
					return;
				case 4:
					this.Game = (SingleFeaturedGameView)target;
					return;
				case 5:
					this.SelectPoints = (StackPanel)target;
					return;
				default:
				{
					int num = 2;
					goto IL_0A;
				}
				}
				break;
			}
			case 1:
			{
				IL_3B:
				if (0 != 0)
				{
				}
				int num = 1;
				goto IL_0A;
			}
			}
			goto IL_3B;
		}
	}
}
