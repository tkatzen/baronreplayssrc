using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
namespace BaronReplays
{
	public class DownloadProgress : Window, INotifyPropertyChanged, IComponentConnector
	{
		private PropertyChangedEventHandler eval_a;
		private string b;
		private string eval_c;
		private WebClient eval_d;
		private int eval_e;
		private bool? eval_f = null;
		private int eval_g;
		private bool eval_h;
		public event PropertyChangedEventHandler PropertyChanged
		{
			add
			{
				int num;
				PropertyChangedEventHandler propertyChangedEventHandler;
				switch (0)
				{
				case 0:
					break;
				default:
					while (true)
					{
						IL_0A:
						PropertyChangedEventHandler propertyChangedEventHandler2;
						switch (num)
						{
						case 0:
							return;
						case 1:
							switch ((702698435 == 702698435) ? 1 : 0)
							{
							case 0:
							case 2:
								goto IL_23;
							case 1:
								IL_57:
								if (1 != 0)
								{
								}
								if (0 != 0)
								{
								}
								goto IL_6A;
							}
							goto IL_57;
						case 2:
							if (propertyChangedEventHandler == propertyChangedEventHandler2)
							{
								num = 0;
								continue;
							}
							goto IL_6A;
						}
						goto IL_1C;
						IL_6A:
						propertyChangedEventHandler2 = propertyChangedEventHandler;
						PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)System.Delegate.Combine(propertyChangedEventHandler2, value);
						propertyChangedEventHandler = System.Threading.Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.eval_a, value2, propertyChangedEventHandler2);
						num = 2;
					}
					return;
				}
				IL_1C:
				propertyChangedEventHandler = this.eval_a;
				IL_23:
				int arg_27_0 = 0;
				num = 1;
				goto IL_0A;
			}
			remove
			{
				int num;
				PropertyChangedEventHandler propertyChangedEventHandler;
				switch (0)
				{
				case 0:
					break;
				default:
					while (true)
					{
						IL_0A:
						PropertyChangedEventHandler propertyChangedEventHandler2;
						switch (num)
						{
						case 0:
							if (1 != 0)
							{
							}
							if (propertyChangedEventHandler == propertyChangedEventHandler2)
							{
								num = 2;
								continue;
							}
							goto IL_60;
						case 1:
							switch ((1462699039 == 1462699039) ? 1 : 0)
							{
							case 0:
							case 2:
								goto IL_23;
							case 1:
								IL_57:
								if (0 != 0)
								{
								}
								goto IL_60;
							}
							goto IL_57;
						case 2:
							return;
						}
						goto IL_1C;
						IL_60:
						propertyChangedEventHandler2 = propertyChangedEventHandler;
						PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)System.Delegate.Remove(propertyChangedEventHandler2, value);
						propertyChangedEventHandler = System.Threading.Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.eval_a, value2, propertyChangedEventHandler2);
						num = 0;
					}
					return;
				}
				IL_1C:
				propertyChangedEventHandler = this.eval_a;
				IL_23:
				int arg_27_0 = 0;
				num = 1;
				goto IL_0A;
			}
		}
		public int Percentage
		{
			get
			{
				int arg_04_0 = 0;
				switch ((1877501628 == 1877501628) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				return this.eval_e;
			}
			set
			{
				int arg_04_0 = 0;
				switch ((1189331520 == 1189331520) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				this.eval_e = value;
				this.eval_a("Percentage");
				this.eval_a("PercentageString");
			}
		}
		public string PercentageString
		{
			get
			{
				int arg_04_0 = 0;
				switch ((1335491323 == 1335491323) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (1 != 0)
				{
				}
				if (0 != 0)
				{
				}
				return string.Format("{0}%", this.Percentage);
			}
		}
		public bool? IsSuccess
		{
			get
			{
				int arg_04_0 = 0;
				switch ((1605338077 == 1605338077) ? 1 : 0)
				{
				case 0:
				case 2:
					IL_2A:
					break;
				case 1:
					goto IL_2B;
				}
				goto IL_2A;
				IL_2B:
				if (0 != 0)
				{
				}
				if (1 != 0)
				{
				}
				return this.eval_f;
			}
		}
		public DownloadProgress(string url, string location)
		{
			this.InitializeComponent();
			this.b = url;
			this.eval_c = location;
		}
		private void eval_b()
		{
			int arg_04_0 = 0;
			switch ((2000213584 == 2000213584) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.eval_d = new WebClient();
			this.eval_d.DownloadProgressChanged += new DownloadProgressChangedEventHandler(this.eval_a);
			this.eval_d.DownloadFileCompleted += new AsyncCompletedEventHandler(this.eval_a);
			this.eval_d.DownloadFileAsync(new Uri(this.b), this.eval_c);
		}
		private void eval_a(object A_0, AsyncCompletedEventArgs A_1)
		{
			int arg_04_0 = 0;
			switch ((1025540551 == 1025540551) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_B7:
				this.eval_f = new bool?(false);
				int num = 3;
				goto IL_45;
			}
			case 1:
			{
				IL_2B:
				if (0 != 0)
				{
				}
				int num = 1;
				goto IL_45;
			}
			}
			goto IL_2B;
			while (true)
			{
				IL_45:
				int num;
				switch (num)
				{
				case 0:
					goto IL_78;
				case 1:
					switch (0)
					{
					case 0:
						goto IL_67;
					}
					continue;
				case 2:
					goto IL_7A;
				case 3:
					goto IL_7A;
				case 4:
					return;
				case 5:
					base.Close();
					num = 4;
					continue;
				case 6:
					if (!A_1.Cancelled)
					{
						num = 5;
						continue;
					}
					return;
				}
				IL_67:
				if (A_1.Error != null)
				{
					num = 0;
					continue;
				}
				this.eval_f = new bool?(true);
				num = 2;
				continue;
				IL_7A:
				if (1 != 0)
				{
				}
				num = 6;
			}
			IL_78:
			goto IL_B7;
		}
		private void eval_a(object A_0, DownloadProgressChangedEventArgs A_1)
		{
			int arg_04_0 = 0;
			switch ((1027374466 == 1027374466) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.eval_g = A_1.ProgressPercentage;
			this.eval_a();
		}
		private void eval_a()
		{
			int arg_04_0 = 0;
			if (1 != 0)
			{
			}
			switch ((1583808245 == 1583808245) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_34:
				break;
			case 1:
				goto IL_35;
			}
			goto IL_34;
			IL_35:
			if (0 != 0)
			{
			}
			this.Percentage = this.eval_g;
		}
		private void eval_a(object A_0, RoutedEventArgs A_1)
		{
			int arg_04_0 = 0;
			switch ((65036531 == 65036531) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_2A:
				break;
			case 1:
				goto IL_2B;
			}
			goto IL_2A;
			IL_2B:
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			base.DataContext = this;
			this.eval_b();
		}
		private void eval_a(string A_0)
		{
			int num = 1;
			while (true)
			{
				switch ((1218391711 == 1218391711) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_57;
				case 1:
				{
					IL_32:
					int arg_36_0 = 0;
					if (0 != 0)
					{
					}
					switch (num)
					{
					case 0:
						this.eval_a(this, new PropertyChangedEventArgs(A_0));
						if (1 != 0)
						{
						}
						num = 2;
						continue;
					case 1:
						switch (0)
						{
						case 0:
							goto IL_57;
						}
						continue;
					case 2:
						return;
					}
					goto IL_57;
				}
				}
				goto IL_32;
				IL_57:
				if (this.eval_a == null)
				{
					break;
				}
				num = 0;
			}
		}
		private void eval_a(object A_0, CancelEventArgs A_1)
		{
			if (1 != 0)
			{
			}
			int num = 2;
			while (true)
			{
				switch ((1694272082 == 1694272082) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_61;
				case 1:
					break;
				default:
				{
					int arg_3F_0 = 0;
					break;
				}
				}
				if (0 != 0)
				{
				}
				switch (num)
				{
				case 0:
					return;
				case 1:
					this.eval_d.CancelAsync();
					num = 0;
					continue;
				case 2:
					switch (0)
					{
					case 0:
						goto IL_61;
					}
					continue;
				}
				IL_61:
				if (this.eval_f.HasValue)
				{
					break;
				}
				num = 1;
			}
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), System.Diagnostics.DebuggerNonUserCode]
		public void InitializeComponent()
		{
			switch ((1929570571 == 1929570571) ? 1 : 0)
			{
			case 0:
			case 2:
				return;
			case 1:
				break;
			default:
			{
				int arg_22_0 = 0;
				break;
			}
			}
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			if (!this.eval_h)
			{
				this.eval_h = true;
				Uri resourceLocator = new Uri("/BaronReplays;component/downloadprogress.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
				return;
			}
		}
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), System.Diagnostics.DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch ((1594730807 == 1594730807) ? 1 : 0)
			{
			case 0:
			case 2:
				goto IL_38;
			case 1:
				break;
			default:
			{
				int arg_22_0 = 0;
				break;
			}
			}
			if (0 != 0)
			{
			}
			if (connectionId != 1)
			{
				this.eval_h = true;
				return;
			}
			IL_38:
			if (1 != 0)
			{
			}
			((DownloadProgress)target).Loaded += new RoutedEventHandler(this.eval_a);
			((DownloadProgress)target).Closing += new CancelEventHandler(this.eval_a);
		}
	}
}
