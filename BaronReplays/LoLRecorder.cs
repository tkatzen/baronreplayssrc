using BaronReplays.Properties;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
namespace BaronReplays
{
	public class LoLRecorder
	{
		public string filePath;
		public int chunkCount;
		public int startupChunkEnd;
		public int gameChunkStart;
		public int chunkGot;
		public int keyframeGot;
		public int keyframeCount;
		private System.Collections.Generic.Dictionary<int, byte[]> eval_a;
		private System.Collections.Generic.Dictionary<int, byte[]> eval_b;
		public LoLRecord record;
		private MainWindow.RecordDoneDelegate eval_c;
		private MainWindow.RecordInfoDoneDelegate eval_d;
		private string eval_e;
		public bool selfGame;
		public PlayerInfo selfPlayerInfo;
		public string platformAddress;
		public string specAddressPrefix;
		private static System.Collections.Generic.List<LoLRecorder> eval_f;
		private System.Collections.Generic.List<string> eval_g;
		private static uint eval_h;
		private string eval_i;
		public event MainWindow.RecordDoneDelegate doneEvent
		{
			add
			{
				switch (0)
				{
				case 0:
					goto IL_1C;
				}
				while (true)
				{
					IL_0A:
					int num;
					MainWindow.RecordDoneDelegate recordDoneDelegate;
					MainWindow.RecordDoneDelegate recordDoneDelegate2;
					switch (num)
					{
					case 0:
						goto IL_77;
					case 1:
						if (recordDoneDelegate == recordDoneDelegate2)
						{
							num = 0;
							continue;
						}
						goto IL_44;
					case 2:
					{
						if (1 != 0)
						{
						}
						int arg_3A_0 = 0;
						goto IL_44;
					}
					}
					goto IL_1C;
					IL_44:
					recordDoneDelegate2 = recordDoneDelegate;
					MainWindow.RecordDoneDelegate value2 = (MainWindow.RecordDoneDelegate)System.Delegate.Combine(recordDoneDelegate2, value);
					recordDoneDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.RecordDoneDelegate>(ref this.eval_c, value2, recordDoneDelegate2);
					num = 1;
				}
				IL_77:
				switch ((2000528524 == 2000528524) ? 1 : 0)
				{
				case 0:
				case 2:
				{
					IL_1C:
					MainWindow.RecordDoneDelegate recordDoneDelegate = this.eval_c;
					int num = 2;
					goto IL_0A;
				}
				case 1:
					IL_96:
					if (0 != 0)
					{
					}
					return;
				}
				goto IL_96;
			}
			remove
			{
				switch (0)
				{
				case 0:
					goto IL_1C;
				}
				while (true)
				{
					IL_0A:
					int num;
					MainWindow.RecordDoneDelegate recordDoneDelegate;
					MainWindow.RecordDoneDelegate recordDoneDelegate2;
					switch (num)
					{
					case 0:
						goto IL_77;
					case 1:
					{
						if (1 != 0)
						{
						}
						int arg_3A_0 = 0;
						goto IL_44;
					}
					case 2:
						if (recordDoneDelegate == recordDoneDelegate2)
						{
							num = 0;
							continue;
						}
						goto IL_44;
					}
					goto IL_1C;
					IL_44:
					recordDoneDelegate2 = recordDoneDelegate;
					MainWindow.RecordDoneDelegate value2 = (MainWindow.RecordDoneDelegate)System.Delegate.Remove(recordDoneDelegate2, value);
					recordDoneDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.RecordDoneDelegate>(ref this.eval_c, value2, recordDoneDelegate2);
					num = 2;
				}
				IL_77:
				switch ((1860538800 == 1860538800) ? 1 : 0)
				{
				case 0:
				case 2:
				{
					IL_1C:
					MainWindow.RecordDoneDelegate recordDoneDelegate = this.eval_c;
					int num = 1;
					goto IL_0A;
				}
				case 1:
					IL_96:
					if (0 != 0)
					{
					}
					return;
				}
				goto IL_96;
			}
		}
		public event MainWindow.RecordInfoDoneDelegate infoDoneEvent
		{
			add
			{
				switch (0)
				{
				case 0:
					goto IL_1C;
				}
				while (true)
				{
					IL_0A:
					int num;
					MainWindow.RecordInfoDoneDelegate recordInfoDoneDelegate;
					MainWindow.RecordInfoDoneDelegate recordInfoDoneDelegate2;
					switch (num)
					{
					case 0:
						if (recordInfoDoneDelegate == recordInfoDoneDelegate2)
						{
							if (1 != 0)
							{
							}
							num = 1;
							continue;
						}
						goto IL_2E;
					case 1:
						goto IL_77;
					case 2:
						goto IL_2E;
					}
					goto IL_1C;
					IL_2E:
					int arg_32_0 = 0;
					recordInfoDoneDelegate2 = recordInfoDoneDelegate;
					MainWindow.RecordInfoDoneDelegate value2 = (MainWindow.RecordInfoDoneDelegate)System.Delegate.Combine(recordInfoDoneDelegate2, value);
					recordInfoDoneDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.RecordInfoDoneDelegate>(ref this.eval_d, value2, recordInfoDoneDelegate2);
					num = 0;
				}
				IL_77:
				switch ((949283110 == 949283110) ? 1 : 0)
				{
				case 0:
				case 2:
				{
					IL_1C:
					MainWindow.RecordInfoDoneDelegate recordInfoDoneDelegate = this.eval_d;
					int num = 2;
					goto IL_0A;
				}
				case 1:
					IL_96:
					if (0 != 0)
					{
					}
					return;
				}
				goto IL_96;
			}
			remove
			{
				switch (0)
				{
				case 0:
					goto IL_1C;
				}
				while (true)
				{
					IL_0A:
					int num;
					MainWindow.RecordInfoDoneDelegate recordInfoDoneDelegate;
					MainWindow.RecordInfoDoneDelegate recordInfoDoneDelegate2;
					switch (num)
					{
					case 0:
						goto IL_77;
					case 1:
						if (recordInfoDoneDelegate == recordInfoDoneDelegate2)
						{
							num = 0;
							continue;
						}
						goto IL_44;
					case 2:
					{
						int arg_3A_0 = 0;
						goto IL_44;
					}
					}
					goto IL_1C;
					IL_44:
					recordInfoDoneDelegate2 = recordInfoDoneDelegate;
					MainWindow.RecordInfoDoneDelegate value2 = (MainWindow.RecordInfoDoneDelegate)System.Delegate.Remove(recordInfoDoneDelegate2, value);
					recordInfoDoneDelegate = System.Threading.Interlocked.CompareExchange<MainWindow.RecordInfoDoneDelegate>(ref this.eval_d, value2, recordInfoDoneDelegate2);
					num = 1;
				}
				IL_77:
				switch ((1449558842 == 1449558842) ? 1 : 0)
				{
				case 0:
				case 2:
				{
					IL_1C:
					if (1 != 0)
					{
					}
					MainWindow.RecordInfoDoneDelegate recordInfoDoneDelegate = this.eval_d;
					int num = 2;
					goto IL_0A;
				}
				case 1:
					IL_96:
					if (0 != 0)
					{
					}
					return;
				}
				goto IL_96;
			}
		}
		public LoLRecorder()
		{
			if (LoLRecorder.eval_f == null)
			{
				LoLRecorder.eval_f = new System.Collections.Generic.List<LoLRecorder>();
			}
		}
		public LoLRecorder(GameInfo gameinfo)
		{
			if (LoLRecorder.eval_f == null)
			{
				LoLRecorder.eval_f = new System.Collections.Generic.List<LoLRecorder>();
			}
			this.record = new LoLRecord();
			this.record.gameId = gameinfo.GameId;
			this.record.observerEncryptionKey = gameinfo.ObKey.ToCharArray();
			this.record.gamePlatform = gameinfo.PlatformId.ToCharArray();
			this.setPlatformAddress(gameinfo.ServerAddress);
		}
		public void setPlatformAddress(string url)
		{
			switch ((1640353866 == 1640353866) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			int arg_23_0 = 0;
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			this.platformAddress = url;
			this.specAddressPrefix = "http://" + this.platformAddress + "/observer-mode/rest/consumer/";
		}
		private bool eval_q()
		{
			int num = 6;
			while (true)
			{
				switch ((1163956827 == 1163956827) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_F0;
				case 1:
				{
					IL_32:
					if (1 != 0)
					{
					}
					if (0 != 0)
					{
					}
					Match match;
					switch (num)
					{
					case 0:
						this.eval_p();
						num = 4;
						continue;
					case 1:
						goto IL_F0;
					case 2:
						return false;
					case 3:
						if (!match.Success)
						{
							num = 2;
							continue;
						}
						this.record.observerEncryptionKey = match.Groups["KEY"].Value.ToCharArray();
						num = 5;
						continue;
					case 4:
						if (this.record.observerEncryptionKey == null)
						{
							num = 1;
							continue;
						}
						return true;
					case 5:
						return true;
					case 6:
						switch (0)
						{
						case 0:
							goto IL_65;
						}
						continue;
					}
					IL_65:
					if (this.selfGame)
					{
						num = 0;
						continue;
					}
					Regex regex = new Regex("spectator " + this.platformAddress + " (?<KEY>.+) (?<GID>[0-9]+)", RegexOptions.IgnoreCase);
					match = regex.Match(this.eval_e);
					num = 3;
					continue;
				}
				}
				goto IL_32;
			}
			return false;
			IL_F0:
			int arg_F4_0 = 0;
			return false;
		}
		private void eval_p()
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_70;
				}
				eval_c eval_c;
				Match match;
				while (true)
				{
					IL_19:
					uint[] array;
					int num2;
					byte[] a_;
					int num3;
					switch (num)
					{
					case 0:
						if (LoLRecorder.eval_h != 0u)
						{
							num = 19;
							continue;
						}
						goto IL_15A;
					case 1:
						goto IL_131;
					case 2:
						LoLRecorder.eval_h = array[num2];
						num = 16;
						continue;
					case 3:
						goto IL_287;
					case 4:
						goto IL_287;
					case 5:
						array = eval_c.eval_a(match.Groups["KEY"].Value, System.Text.Encoding.ASCII, 0);
						num2 = 0;
						num = 12;
						continue;
					case 6:
						if (this.eval_a(a_))
						{
							switch ((484762541 == 484762541) ? 1 : 0)
							{
							case 0:
							case 2:
								goto IL_CE;
							case 1:
								IL_118:
								if (0 != 0)
								{
								}
								num = 2;
								continue;
							}
							goto IL_118;
						}
						num2++;
						num = 17;
						continue;
					case 7:
						LoLRecorder.eval_h = array[num3];
						num = 3;
						continue;
					case 8:
						if (this.eval_a(eval_c.eval_a(LoLRecorder.eval_h, 256u)))
						{
							num = 13;
							continue;
						}
						goto IL_15A;
					case 9:
					{
						byte[] a_2;
						if (this.eval_a(a_2))
						{
							num = 7;
							continue;
						}
						num3++;
						num = 18;
						continue;
					}
					case 10:
						goto IL_215;
					case 11:
						if (this.record.observerEncryptionKey == null)
						{
							num = 5;
							continue;
						}
						goto IL_305;
					case 12:
						goto IL_1F1;
					case 13:
						goto IL_2E8;
					case 14:
					{
						if (num3 >= array.Length)
						{
							num = 4;
							continue;
						}
						byte[] a_2 = eval_c.eval_a(array[num3], 256u);
						num = 9;
						continue;
					}
					case 15:
						if (num2 >= array.Length)
						{
							num = 10;
							continue;
						}
						goto IL_CE;
					case 16:
						goto IL_282;
					case 17:
					{
						int arg_1AB_0 = 0;
						goto IL_1F1;
					}
					case 18:
						goto IL_131;
					case 19:
						num = 8;
						continue;
					}
					goto IL_70;
					IL_CE:
					a_ = eval_c.eval_a(array[num2], 256u);
					num = 6;
					continue;
					IL_131:
					num = 14;
					continue;
					IL_15A:
					eval_c.eval_c();
					array = eval_c.eval_a(match.Groups["KEY"].Value, System.Text.Encoding.ASCII, 1);
					num3 = 0;
					num = 1;
					continue;
					IL_1F1:
					num = 15;
					continue;
					IL_287:
					num = 11;
				}
				IL_215:
				IL_282:
				goto IL_305;
				IL_2E8:
				eval_c.eval_a();
				return;
				IL_305:
				eval_c.eval_a();
				return;
				IL_70:
				Regex regex = new Regex(" [0-9]+ (?<KEY>.+) <id_removed>", RegexOptions.IgnoreCase);
				match = regex.Match(this.eval_e);
				eval_c = new eval_c();
				eval_c.eval_a("LolClient");
				if (1 != 0)
				{
				}
				num = 0;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		private bool eval_a(byte[] A_0)
		{
			bool result;
			try
			{
				int num = 0;
				switch (num)
				{
				case 0:
				{
					IL_0F:
					switch (0)
					{
					case 0:
						goto IL_84;
					}
					short num2;
					System.Text.StringBuilder stringBuilder;
					bool flag;
					System.Collections.Generic.List<short> list;
					while (true)
					{
						IL_19:
						int num3;
						switch (num)
						{
						case 0:
						{
							Match match;
							if (match.Success)
							{
								num = 17;
								continue;
							}
							goto IL_13C;
						}
						case 1:
							goto IL_21F;
						case 2:
							goto IL_13C;
						case 3:
							goto IL_277;
						case 4:
							num = 20;
							continue;
						case 5:
							if (A_0[(int)num2] < 125)
							{
								num = 18;
								continue;
							}
							goto IL_A6;
						case 6:
							goto IL_1F5;
						case 7:
							goto IL_307;
						case 8:
							goto IL_21F;
						case 9:
						{
							string[] array;
							this.record.observerEncryptionKey = array[num3].Substring(1, 32).ToCharArray();
							num = 11;
							continue;
						}
						case 10:
						{
							string[] array = stringBuilder.ToString().Split(new char[]
							{
								' '
							});
							Regex regex = new Regex("(?<ADDR>\\w+\\.\\w+\\.[\\w\\.]+)", RegexOptions.IgnoreCase);
							this.eval_g = new System.Collections.Generic.List<string>();
							num3 = 0;
							num = 6;
							continue;
						}
						case 11:
							goto IL_13C;
						case 12:
							if (A_0[(int)num2] > 32)
							{
								num = 24;
								continue;
							}
							goto IL_A6;
						case 13:
						{
							string[] array;
							if (num3 >= array.Length)
							{
								num = 19;
								continue;
							}
							num = 22;
							continue;
						}
						case 14:
							if (!flag)
							{
								num = 16;
								continue;
							}
							goto IL_21F;
						case 15:
							goto IL_1F5;
						case 16:
							list.Add(num2);
							stringBuilder.Append(' ');
							flag = true;
							num = 8;
							continue;
						case 17:
						{
							string[] array;
							this.eval_g.Add(array[num3]);
							num = 2;
							continue;
						}
						case 18:
							stringBuilder.Append((char)A_0[(int)num2]);
							flag = false;
							num = 1;
							continue;
						case 19:
							num = 7;
							continue;
						case 20:
						{
							string[] array;
							if (array[num3][0] == 'A')
							{
								num = 9;
								continue;
							}
							goto IL_13C;
						}
						case 21:
							if ((int)num2 >= A_0.Length)
							{
								num = 10;
								continue;
							}
							num = 12;
							continue;
						case 22:
						{
							string[] array;
							if (array[num3].Length == 33)
							{
								num = 4;
								continue;
							}
							Regex regex;
							Match match = regex.Match(array[num3]);
							num = 0;
							continue;
						}
						case 23:
							goto IL_277;
						case 24:
							num = 5;
							continue;
						}
						goto IL_84;
						IL_A6:
						num = 14;
						continue;
						IL_13C:
						num3++;
						num = 15;
						continue;
						IL_1F5:
						num = 13;
						continue;
						IL_21F:
						num2 += 1;
						num = 23;
						continue;
						IL_277:
						num = 21;
					}
					IL_307:
					goto IL_30F;
					IL_84:
					num2 = 0;
					stringBuilder = new System.Text.StringBuilder();
					list = new System.Collections.Generic.List<short>();
					flag = false;
					num = 3;
					goto IL_19;
				}
				}
				goto IL_0F;
			}
			catch
			{
				result = false;
				return result;
			}
			IL_30F:
			switch ((1866535082 == 1866535082) ? 1 : 0)
			{
			case 0:
			case 2:
				return result;
			case 1:
			{
				IL_32E:
				if (0 != 0)
				{
				}
				int arg_339_0 = 0;
				if (1 != 0)
				{
				}
				return this.record.observerEncryptionKey != null;
			}
			}
			goto IL_32E;
		}
		private bool eval_o()
		{
			while (true)
			{
				WebClient webClient = new WebClient();
				bool result;
				try
				{
					try
					{
						webClient.DownloadString(string.Concat(new object[]
						{
							this.specAddressPrefix,
							"getLastChunkInfo/",
							new string(this.record.gamePlatform),
							"/",
							this.record.gameId,
							"/30000/token"
						}));
					}
					catch (WebException)
					{
						result = false;
						return result;
					}
					goto IL_08;
				}
				finally
				{
					int num = 0;
					while (true)
					{
						switch (num)
						{
						case 0:
							switch (0)
							{
							case 0:
								goto IL_D2;
							}
							continue;
						case 1:
							((System.IDisposable)webClient).Dispose();
							num = 2;
							continue;
						case 2:
							goto IL_EF;
						}
						IL_D2:
						if (webClient == null)
						{
							break;
						}
						num = 1;
					}
					IL_EF:;
				}
				return result;
				IL_08:
				int arg_0C_0 = 0;
				switch ((1326442460 == 1326442460) ? 1 : 0)
				{
				case 0:
				case 2:
					continue;
				}
				break;
			}
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			return true;
		}
		private bool eval_a(string A_0, int A_1)
		{
			while (true)
			{
				WebClient webClient = new WebClient();
				bool result;
				try
				{
					try
					{
						webClient.DownloadString(string.Concat(new object[]
						{
							"http://",
							A_0,
							":",
							A_1,
							"/observer-mode/rest/consumer/getLastChunkInfo/",
							new string(this.record.gamePlatform),
							"/",
							this.record.gameId,
							"/30000/token"
						}));
					}
					catch (WebException)
					{
						result = false;
						return result;
					}
					goto IL_08;
				}
				finally
				{
					int num = 0;
					while (true)
					{
						switch (num)
						{
						case 0:
							switch (0)
							{
							case 0:
								goto IL_E7;
							}
							continue;
						case 1:
							((System.IDisposable)webClient).Dispose();
							num = 2;
							continue;
						case 2:
							goto IL_104;
						}
						IL_E7:
						if (webClient == null)
						{
							break;
						}
						num = 1;
					}
					IL_104:;
				}
				return result;
				IL_08:
				int arg_0C_0 = 0;
				switch ((1383688813 == 1383688813) ? 1 : 0)
				{
				case 0:
				case 2:
					continue;
				}
				break;
			}
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			return true;
		}
		private void eval_n()
		{
			switch ((1097186755 == 1097186755) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			int arg_23_0 = 0;
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			this.eval_e = global::eval_a.eval_a(this.filePath);
		}
		private bool eval_m()
		{
			switch (0)
			{
			case 0:
				goto IL_24;
			}
			int num;
			int num2;
			Match match;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					goto IL_BE;
				case 1:
					goto IL_130;
				case 2:
					if (num2++ >= 36)
					{
						num = 3;
						continue;
					}
					goto IL_BE;
				case 3:
					return false;
				case 4:
					switch ((218617312 == 218617312) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_24;
					case 1:
					{
						IL_112:
						if (0 != 0)
						{
						}
						if (match.Success)
						{
							num = 1;
							continue;
						}
						int arg_38_0 = 0;
						if (1 != 0)
						{
						}
						num = 2;
						continue;
					}
					}
					goto IL_112;
				}
				goto IL_24;
				IL_BE:
				System.Threading.Thread.Sleep(5000);
				this.eval_n();
				Regex regex = new Regex("GameID: (?<GID>[a-f0-9]+), PlatformID: (?<PID>[A-Z0-9_]+)", RegexOptions.IgnoreCase);
				match = regex.Match(this.eval_e);
				num = 4;
			}
			return false;
			IL_130:
			this.record.gameId = ulong.Parse(match.Groups["GID"].Value, System.Globalization.NumberStyles.HexNumber);
			this.record.gamePlatform = match.Groups["PID"].Value.ToCharArray();
			return true;
			IL_24:
			num2 = 0;
			num = 0;
			goto IL_0A;
		}
		private bool eval_l()
		{
			switch (0)
			{
			case 0:
				goto IL_20;
			}
			int num;
			Match match;
			while (true)
			{
				IL_0A:
				switch (num)
				{
				case 0:
					goto IL_A7;
				case 1:
					this.setPlatformAddress(match.Groups["ADDR"].Value);
					this.selfGame = false;
					num = 0;
					continue;
				case 2:
					if (match.Success)
					{
						num = 1;
						continue;
					}
					goto IL_65;
				case 3:
					goto IL_A7;
				}
				goto IL_20;
				IL_65:
				this.selfGame = true;
				num = 3;
				continue;
				IL_A7:
				switch ((29262141 == 29262141) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_65;
				case 1:
					goto IL_D2;
				}
				break;
			}
			int arg_C9_0 = 0;
			IL_D2:
			if (0 != 0)
			{
			}
			return true;
			IL_20:
			if (1 != 0)
			{
			}
			this.eval_n();
			Regex regex = new Regex("spectator (?<ADDR>[A-Za-z0-9\\.]+:[0-9]*)", RegexOptions.IgnoreCase);
			match = regex.Match(this.eval_e);
			num = 2;
			goto IL_0A;
		}
		private bool eval_k()
		{
			bool result;
			while (true)
			{
				switch (0)
				{
				case 0:
				{
					IL_0F:
					int arg_13_0 = 0;
					Regex regex = new Regex("(?<ADDR>[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)", RegexOptions.IgnoreCase);
					this.eval_g.Reverse();
					using (System.Collections.Generic.List<string>.Enumerator enumerator = this.eval_g.GetEnumerator())
					{
						int num = 2;
						while (true)
						{
							switch (num)
							{
							case 0:
							{
								string current;
								string text = current;
								num = 10;
								continue;
							}
							case 1:
								goto IL_1B9;
							case 2:
								switch (0)
								{
								case 0:
									goto IL_96;
								}
								continue;
							case 3:
							{
								string current;
								if (regex.Match(current).Success)
								{
									num = 0;
									continue;
								}
								string text = current.Substring(1);
								num = 9;
								continue;
							}
							case 4:
							{
								if (!enumerator.MoveNext())
								{
									num = 7;
									continue;
								}
								string current = enumerator.Current;
								num = 3;
								continue;
							}
							case 5:
							{
								string text;
								if (this.eval_a(text, 8088))
								{
									num = 13;
									continue;
								}
								num = 12;
								continue;
							}
							case 6:
							{
								string text;
								this.setPlatformAddress(text + ":80");
								result = true;
								num = 11;
								continue;
							}
							case 7:
								num = 1;
								continue;
							case 8:
								goto IL_193;
							case 9:
								goto IL_138;
							case 10:
								goto IL_138;
							case 11:
								goto IL_B5;
							case 12:
							{
								string text;
								if (this.eval_a(text, 80))
								{
									num = 6;
									continue;
								}
								break;
							}
							case 13:
							{
								string text;
								this.setPlatformAddress(text + ":8088");
								result = true;
								num = 8;
								continue;
							}
							}
							IL_113:
							num = 4;
							continue;
							IL_96:
							goto IL_113;
							IL_138:
							num = 5;
						}
						IL_B5:
						IL_193:
						goto IL_1CC;
						IL_1B9:
						return false;
					}
					IL_1CC:
					if (1 != 0)
					{
					}
					switch ((618630219 == 618630219) ? 1 : 0)
					{
					case 0:
					case 2:
						continue;
					}
					goto IL_1F5;
				}
				}
				goto IL_0F;
			}
			return false;
			IL_1F5:
			if (0 != 0)
			{
			}
			return result;
		}
		public void StopRecording(bool isSuccess, string reason)
		{
			switch ((201330073 == 201330073) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			int arg_23_0 = 0;
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			this.eval_c(this, isSuccess, reason);
		}
		private bool eval_j()
		{
			while (true)
			{
				bool result;
				using (System.Collections.Generic.List<LoLRecorder>.Enumerator enumerator = LoLRecorder.eval_f.GetEnumerator())
				{
					int num = 7;
					while (true)
					{
						switch (num)
						{
						case 0:
						{
							if (!enumerator.MoveNext())
							{
								num = 2;
								continue;
							}
							LoLRecorder current = enumerator.Current;
							num = 3;
							continue;
						}
						case 1:
						{
							LoLRecorder current;
							if (current.record.players != null)
							{
								num = 10;
								continue;
							}
							System.Threading.Thread.Sleep(10000);
							num = 12;
							continue;
						}
						case 2:
							num = 6;
							continue;
						case 3:
						{
							LoLRecorder current;
							if (this.record.gameId == current.record.gameId)
							{
								num = 11;
								continue;
							}
							break;
						}
						case 4:
							goto IL_E9;
						case 5:
						{
							LoLRecorder current;
							current.record.players = this.record.players;
							current.selfPlayerInfo = this.selfPlayerInfo;
							num = 4;
							continue;
						}
						case 6:
							goto IL_1AF;
						case 7:
							switch (0)
							{
							case 0:
								goto IL_A3;
							}
							continue;
						case 8:
							goto IL_E9;
						case 9:
							goto IL_B0;
						case 10:
							result = false;
							num = 9;
							continue;
						case 11:
							num = 8;
							continue;
						case 12:
							if (this.eval_g())
							{
								num = 5;
								continue;
							}
							goto IL_E9;
						}
						goto IL_A3;
						IL_E9:
						num = 1;
						continue;
						IL_110:
						num = 0;
						continue;
						IL_A3:
						goto IL_110;
					}
					IL_B0:
					return result;
					IL_1AF:
					goto IL_0D;
				}
				return result;
				IL_0D:
				int arg_11_0 = 0;
				switch ((690996964 == 690996964) ? 1 : 0)
				{
				case 0:
				case 2:
					continue;
				}
				break;
			}
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			LoLRecorder.eval_f.Add(this);
			return true;
		}
		public void removeFromList()
		{
			int num = 1;
			while (true)
			{
				switch ((716260560 == 716260560) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_6D;
				case 1:
					IL_32:
					if (1 != 0)
					{
					}
					if (0 != 0)
					{
					}
					switch (num)
					{
					case 0:
						goto IL_6D;
					case 1:
						switch (0)
						{
						case 0:
							goto IL_55;
						}
						continue;
					case 2:
						goto IL_82;
					}
					IL_55:
					if (LoLRecorder.eval_f.Contains(this))
					{
						num = 0;
						continue;
					}
					goto IL_84;
				}
				goto IL_32;
				IL_6D:
				LoLRecorder.eval_f.Remove(this);
				num = 2;
			}
			IL_82:
			IL_84:
			int arg_88_0 = 0;
		}
		private void eval_c(string A_0)
		{
			switch ((1539483939 == 1539483939) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			int arg_23_0 = 0;
			if (0 != 0)
			{
			}
			if (1 != 0)
			{
			}
			this.StopRecording(false, Utilities.GetString(A_0));
			this.removeFromList();
		}
		private bool eval_i()
		{
			while (this.selfGame)
			{
				int arg_0C_0 = 0;
				bool result;
				try
				{
					switch (0)
					{
					case 0:
						goto IL_34;
					}
					int num;
					while (true)
					{
						IL_22:
						switch (num)
						{
						case 0:
							goto IL_6E;
						case 1:
							if (!this.eval_o())
							{
								num = 0;
								continue;
							}
							num = 2;
							continue;
						case 2:
							goto IL_7F;
						}
						goto IL_34;
					}
					IL_6E:
					throw new System.Exception();
					IL_7F:
					break;
					IL_34:
					this.setPlatformAddress(Utilities.LoLObserveServersIpMapping[new string(this.record.gamePlatform)]);
					num = 1;
					goto IL_22;
				}
				catch
				{
					result = this.eval_k();
				}
				if (1 != 0)
				{
				}
				switch ((145153480 == 145153480) ? 1 : 0)
				{
				case 0:
				case 2:
					continue;
				case 1:
					IL_B4:
					if (0 != 0)
					{
					}
					return result;
				}
				goto IL_B4;
			}
			return true;
		}
		public void startRecording()
		{
			int num = 8;
			JObject jObject;
			while (true)
			{
				switch (num)
				{
				case 0:
					if (!this.eval_q())
					{
						num = 14;
						continue;
					}
					num = 2;
					continue;
				case 1:
					goto IL_2BF;
				case 2:
					if (!this.eval_i())
					{
						int arg_2FA_0 = 0;
						num = 6;
						continue;
					}
					this.record.lolVersion = FileVersionInfo.GetVersionInfo(this.eval_i).ProductVersion.ToCharArray();
					num = 3;
					continue;
				case 3:
					goto IL_39A;
				case 4:
					if (!this.eval_m())
					{
						num = 11;
						continue;
					}
					num = 13;
					continue;
				case 5:
					goto IL_266;
				case 6:
					goto IL_30E;
				case 7:
					if (jObject == null)
					{
						num = 17;
						continue;
					}
					this.record.gameMetaAnalyze(jObject);
					jObject = this.getLastChunkInfo();
					num = 18;
					continue;
				case 8:
					switch (0)
					{
					case 0:
						goto IL_89;
					}
					continue;
				case 9:
					goto IL_17B;
				case 10:
					goto IL_C8;
				case 11:
					goto IL_114;
				case 12:
					goto IL_39A;
				case 13:
					if (!this.eval_j())
					{
						if (1 != 0)
						{
						}
						num = 5;
						continue;
					}
					num = 22;
					continue;
				case 14:
					goto IL_297;
				case 15:
					if (!this.eval_c())
					{
						num = 1;
						continue;
					}
					jObject = this.getGameMeta();
					goto IL_1AF;
				case 16:
					this.record = new LoLRecord();
					this.eval_i = global::eval_a.eval_a();
					this.filePath = global::eval_a.eval_b(this.eval_i);
					num = 23;
					continue;
				case 17:
					goto IL_1CD;
				case 18:
					if (jObject == null)
					{
						num = 21;
						continue;
					}
					switch ((109792739 == 109792739) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_1AF;
					}
					goto IL_407;
				case 19:
					if (!this.eval_e())
					{
						num = 10;
						continue;
					}
					num = 15;
					continue;
				case 20:
					goto IL_3E3;
				case 21:
					goto IL_156;
				case 22:
					if (!this.eval_l())
					{
						num = 25;
						continue;
					}
					num = 0;
					continue;
				case 23:
					if (this.filePath == null)
					{
						num = 26;
						continue;
					}
					num = 4;
					continue;
				case 24:
					if (!this.eval_d())
					{
						num = 20;
						continue;
					}
					num = 19;
					continue;
				case 25:
					goto IL_1A3;
				case 26:
					goto IL_234;
				case 27:
					if (!this.eval_j())
					{
						num = 9;
						continue;
					}
					this.record.lolVersion = FileVersionInfo.GetVersionInfo(BaronReplays.Properties.Settings.Default.LoLGameExe).ProductVersion.ToCharArray();
					num = 12;
					continue;
				}
				IL_89:
				if (this.record == null)
				{
					num = 16;
					continue;
				}
				num = 27;
				continue;
				IL_1AF:
				num = 7;
				continue;
				IL_39A:
				this.record.spectatorClientVersion = this.eval_a(this.specAddressPrefix + "version").ToCharArray();
				num = 24;
			}
			IL_C8:
			this.eval_c("ErrorGettingSpecMode");
			return;
			IL_114:
			this.eval_c("ErrorDetectGameId");
			return;
			IL_156:
			this.eval_c("ErrorGettingContent");
			return;
			IL_17B:
			this.eval_c("ErrorDuplicateGame");
			return;
			IL_1A3:
			this.eval_c("ErrorUnsuppotedRegion");
			this.removeFromList();
			return;
			IL_1CD:
			this.eval_c("ErrorGettingContent");
			return;
			IL_234:
			this.eval_c("ErrorDetectGameId");
			return;
			IL_266:
			this.eval_c("ErrorDuplicateGame");
			return;
			IL_297:
			this.eval_c("ErrorDetectObKey");
			return;
			IL_2BF:
			this.eval_c("ErrorGettingContent");
			return;
			IL_30E:
			this.eval_c("ErrorUnsuppotedRegion");
			this.removeFromList();
			return;
			IL_3E3:
			this.eval_c("ErrorGettingSpecMode");
			return;
			IL_407:
			if (0 != 0)
			{
			}
			this.record.lastChunkInfoAnalyze(jObject);
			this.record.setAllChunksContent(this.eval_b);
			this.record.setAllKeyFrameContent(this.eval_a);
			this.record.setEndOfGameStats(this.eval_h());
			this.removeFromList();
			this.StopRecording(true, string.Empty);
		}
		private byte[] eval_h()
		{
			byte[] array;
			while (true)
			{
				string text = this.eval_a(string.Concat(new object[]
				{
					this.specAddressPrefix,
					"endOfGameStats/",
					new string(this.record.gamePlatform),
					"/",
					this.record.gameId,
					"/null"
				}));
				array = null;
				byte[] result;
				try
				{
					array = System.Convert.FromBase64String(text.Replace(" ", "+"));
					goto IL_5F;
				}
				catch
				{
					Utilities.LogWriter.WriteLine("Cannot get EndOfGameStatas");
					Utilities.LogWriter.WriteLine("Requested URL is: {0} ", string.Concat(new object[]
					{
						this.specAddressPrefix,
						"endOfGameStats/",
						new string(this.record.gamePlatform),
						"/",
						this.record.gameId,
						"/null"
					}));
					Utilities.LogWriter.WriteLine("Stats tring is: {0} ", text);
					if (array == null)
					{
						Utilities.LogWriter.WriteLine("Data byte is null");
					}
					else
					{
						Utilities.LogWriter.WriteLine("Data byte is {0} bytes", array.Length);
					}
					Utilities.LogWriter.Flush();
					result = null;
				}
				return result;
				IL_5F:
				switch ((1439890665 == 1439890665) ? 1 : 0)
				{
				case 0:
				case 2:
					continue;
				}
				break;
			}
			if (0 != 0)
			{
			}
			int arg_89_0 = 0;
			if (1 != 0)
			{
			}
			return array;
		}
		private bool eval_g()
		{
			if (1 != 0)
			{
			}
			int num = 0;
			switch (num)
			{
			case 0:
				IL_19:
				switch (0)
				{
				case 0:
					goto IL_66;
				}
				while (true)
				{
					IL_23:
					int num2;
					switch (num)
					{
					case 0:
						return true;
					case 1:
						goto IL_1CD;
					case 2:
						switch ((941370853 == 941370853) ? 1 : 0)
						{
						case 0:
						case 2:
							goto IL_17A;
						case 1:
							IL_B9:
							if (0 != 0)
							{
							}
							this.selfPlayerInfo = this.record.players[num2];
							num = 12;
							continue;
						}
						goto IL_B9;
					case 3:
						return false;
					case 4:
						this.eval_d(this);
						num = 11;
						continue;
					case 5:
					{
						int num3;
						if (num3 == this.record.players[num2].clientID)
						{
							num = 2;
							continue;
						}
						goto IL_174;
					}
					case 6:
					{
						if (!this.eval_e.Contains("EndSpawn"))
						{
							num = 3;
							continue;
						}
						Regex regex = new Regex("Spawning champion [(](?<CNAME>.+)[)] with skinID [0-9]+ on team (?<TEAM>[0-9]+) for clientID (?<CID>-*[0-9]+) and summonername [(](?<PNAME>.+)[)] [(]is", RegexOptions.IgnoreCase);
						MatchCollection matchCollection = regex.Matches(this.eval_e);
						this.record.players = new PlayerInfo[matchCollection.Count];
						int num4 = 0;
						num = 14;
						continue;
					}
					case 7:
					{
						Regex regex2 = new Regex("netUID: (?<PLAYERID>[0-9]+) defaultname");
						Match match = regex2.Match(this.eval_e);
						int num3 = int.Parse(match.Groups["PLAYERID"].Value);
						num2 = 0;
						num = 1;
						continue;
					}
					case 8:
						goto IL_1CD;
					case 9:
						goto IL_1F6;
					case 10:
					{
						MatchCollection matchCollection;
						int num4;
						if (num4 >= matchCollection.Count)
						{
							num = 4;
							continue;
						}
						this.record.players[num4] = new PlayerInfo(matchCollection[num4].Groups["PNAME"].Value, matchCollection[num4].Groups["CNAME"].Value, uint.Parse(matchCollection[num4].Groups["TEAM"].Value), int.Parse(matchCollection[num4].Groups["CID"].Value));
						num4++;
						num = 9;
						continue;
					}
					case 11:
						if (this.selfGame)
						{
							num = 7;
							continue;
						}
						return true;
					case 12:
						goto IL_174;
					case 13:
					{
						MatchCollection matchCollection;
						if (num2 >= matchCollection.Count)
						{
							num = 0;
							continue;
						}
						num = 5;
						continue;
					}
					case 14:
						goto IL_1F6;
					}
					goto IL_66;
					IL_17A:
					int arg_17E_0 = 0;
					num = 8;
					continue;
					IL_174:
					num2++;
					goto IL_17A;
					IL_1CD:
					num = 13;
					continue;
					IL_1F6:
					num = 10;
				}
				return false;
				IL_66:
				this.eval_n();
				num = 6;
				goto IL_23;
			}
			goto IL_19;
		}
		private string eval_f()
		{
			if (1 != 0)
			{
			}
			int arg_0E_0 = 0;
			switch ((33383263 == 33383263) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_34:
				break;
			case 1:
				goto IL_35;
			}
			goto IL_34;
			IL_35:
			if (0 != 0)
			{
			}
			this.eval_n();
			Regex regex = new Regex("netUID: (?<PLAYERID>[0-9]+) defaultname");
			Match match = regex.Match(this.eval_e);
			int num = int.Parse(match.Groups["PLAYERID"].Value);
			Regex regex2 = new Regex("Spawning champion [(](?<CNAME>.+)[)] with skinID [0-9]+ on team (?<TEAM>[0-9]+) for clientID " + num + " and summonername [(](?<PNAME>.+)[)] [(]is", RegexOptions.IgnoreCase);
			Match match2 = regex2.Match(this.eval_e);
			return match2.Groups["PNAME"].ToString();
		}
		private JObject eval_b(string A_0)
		{
			JObject result;
			JObject result2;
			while (true)
			{
				result = null;
				try
				{
					result = JsonConvert.DeserializeObject<JObject>(this.eval_a(A_0));
					goto IL_04;
				}
				catch (System.Exception)
				{
					result2 = null;
				}
				goto IL_4C;
				IL_04:
				switch ((1592522357 == 1592522357) ? 1 : 0)
				{
				case 0:
				case 2:
					continue;
				}
				break;
			}
			if (0 != 0)
			{
			}
			int arg_2E_0 = 0;
			return result;
			IL_4C:
			if (1 != 0)
			{
			}
			return result2;
		}
		private string eval_a(string A_0)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				int arg_13_0 = 0;
				if (1 != 0)
				{
				}
				string text = null;
				WebClient webClient = new WebClient();
				string result;
				try
				{
					int num2;
					switch (0)
					{
					case 0:
						break;
					default:
						while (true)
						{
							IL_3B:
							switch (num)
							{
							case 0:
								if (text != null)
								{
									switch ((1770697300 == 1770697300) ? 1 : 0)
									{
									case 0:
									case 2:
										goto IL_54;
									case 1:
										IL_8C:
										if (0 != 0)
										{
										}
										num = 1;
										continue;
									}
									goto IL_8C;
								}
								goto IL_9F;
							case 1:
								goto IL_C6;
							case 2:
								goto IL_D3;
							case 3:
								goto IL_9F;
							}
							goto IL_52;
							try
							{
								IL_9F:
								text = webClient.DownloadString(A_0);
								goto IL_60;
							}
							catch (WebException)
							{
								if (num2++ < 3)
								{
									System.Threading.Thread.Sleep(10000);
									goto IL_60;
								}
								result = null;
								return result;
							}
							goto IL_C6;
							IL_60:
							num = 0;
							continue;
							IL_C6:
							num = 2;
						}
						IL_D3:
						return text;
					}
					IL_52:
					num2 = 0;
					IL_54:
					num = 3;
					goto IL_3B;
				}
				finally
				{
					num = 0;
					while (true)
					{
						switch (num)
						{
						case 0:
							switch (0)
							{
							case 0:
								goto IL_FF;
							}
							continue;
						case 1:
							goto IL_11E;
						case 2:
							((System.IDisposable)webClient).Dispose();
							num = 1;
							continue;
						}
						IL_FF:
						if (webClient == null)
						{
							break;
						}
						num = 2;
					}
					IL_11E:;
				}
				return result;
			}
			}
			goto IL_0F;
		}
		private bool eval_e()
		{
			JObject jObject = this.eval_b(string.Concat(new object[]
			{
				this.specAddressPrefix,
				"getGameMetaData/",
				new string(this.record.gamePlatform),
				"/",
				this.record.gameId,
				"/1/token"
			}));
			if (jObject == null)
			{
				switch ((893356972 == 893356972) ? 1 : 0)
				{
				case 0:
				case 2:
					goto IL_9E;
				case 1:
				{
					IL_7D:
					if (1 != 0)
					{
					}
					if (0 != 0)
					{
					}
					int arg_94_0 = 0;
					return false;
				}
				}
				goto IL_7D;
			}
			IL_9E:
			this.chunkCount = int.Parse(jObject["lastChunkId"].ToString());
			this.startupChunkEnd = int.Parse(jObject["endStartupChunkId"].ToString());
			this.gameChunkStart = int.Parse(jObject["startGameChunkId"].ToString());
			return true;
		}
		private bool eval_d()
		{
			switch (0)
			{
			case 0:
			{
				IL_0F:
				int num = 0;
				bool result;
				while (true)
				{
					int num2;
					JObject jObject;
					System.DateTime now;
					switch (num)
					{
					case 0:
						switch (0)
						{
						case 0:
							goto IL_42;
						}
						continue;
					case 1:
						if (num2 >= 0)
						{
							num = 3;
							continue;
						}
						goto IL_10F;
					case 2:
						goto IL_10F;
					case 3:
						goto IL_E7;
					case 4:
						try
						{
							num2 = int.Parse(jObject["nextAvailableChunk"].ToString());
							goto IL_C6;
						}
						catch
						{
							if ((System.DateTime.Now - now).TotalMinutes > 3.0)
							{
								result = false;
								goto IL_18E;
							}
							goto IL_C6;
						}
						goto IL_99;
						IL_C6:
						num = 1;
						continue;
					case 5:
						return true;
					}
					IL_42:
					if (!this.selfGame)
					{
						num = 5;
						continue;
					}
					IL_99:
					num2 = 0;
					now = System.DateTime.Now;
					num = 2;
					continue;
					IL_10F:
					System.Threading.Thread.Sleep(10000);
					jObject = this.eval_b(string.Concat(new object[]
					{
						this.specAddressPrefix,
						"getLastChunkInfo/",
						new string(this.record.gamePlatform),
						"/",
						this.record.gameId,
						"/30000/token"
					}));
					num = 4;
				}
				return true;
				IL_E7:
				switch ((2122087621 == 2122087621) ? 1 : 0)
				{
				case 0:
				case 2:
					return true;
				case 1:
				{
					IL_106:
					if (0 != 0)
					{
					}
					int arg_B4_0 = 0;
					int num2;
					System.Threading.Thread.Sleep(num2);
					return true;
				}
				}
				goto IL_106;
				IL_18E:
				if (1 != 0)
				{
				}
				return result;
			}
			}
			goto IL_0F;
		}
		private bool eval_c()
		{
			switch (0)
			{
			case 0:
				goto IL_48;
			}
			int num;
			int num2;
			while (true)
			{
				IL_0A:
				JObject jObject;
				switch (num)
				{
				case 0:
					num = 13;
					continue;
				case 1:
					if (jObject == null)
					{
						num = 9;
						continue;
					}
					this.chunkCount = int.Parse(jObject["chunkId"].ToString());
					this.keyframeCount = int.Parse(jObject["keyFrameId"].ToString());
					num2 = int.Parse(jObject["nextAvailableChunk"].ToString());
					System.Threading.Thread.Sleep(num2);
					num = 7;
					continue;
				case 2:
					if (this.record.players == null)
					{
						num = 0;
						continue;
					}
					goto IL_1CD;
				case 3:
					if (!this.eval_a())
					{
						int arg_A1_0 = 0;
						num = 12;
						continue;
					}
					switch ((1797063116 == 1797063116) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_15B;
					case 1:
						IL_1A3:
						if (0 != 0)
						{
						}
						num = 10;
						continue;
					}
					goto IL_1A3;
				case 4:
					return true;
				case 5:
					goto IL_1CD;
				case 6:
					return false;
				case 7:
					if (!this.eval_b())
					{
						num = 6;
						continue;
					}
					if (1 != 0)
					{
					}
					num = 3;
					continue;
				case 8:
					this.eval_g();
					num = 5;
					continue;
				case 9:
					return false;
				case 10:
					if (num2 == 0)
					{
						num = 4;
						continue;
					}
					goto IL_15B;
				case 11:
					goto IL_15B;
				case 12:
					return false;
				case 13:
					if (this.filePath != null)
					{
						num = 8;
						continue;
					}
					goto IL_1CD;
				}
				goto IL_48;
				IL_15B:
				num = 2;
				continue;
				IL_1CD:
				jObject = this.eval_b(string.Concat(new object[]
				{
					this.specAddressPrefix,
					"getLastChunkInfo/",
					new string(this.record.gamePlatform),
					"/",
					this.record.gameId,
					"/30000/token"
				}));
				num = 1;
			}
			return false;
			IL_48:
			this.eval_b = new System.Collections.Generic.Dictionary<int, byte[]>();
			this.eval_a = new System.Collections.Generic.Dictionary<int, byte[]>();
			this.chunkGot = 1;
			this.keyframeGot = 1;
			num2 = 0;
			num = 11;
			goto IL_0A;
		}
		public JObject getGameMeta()
		{
			switch ((475243994 == 475243994) ? 1 : 0)
			{
			case 0:
			case 2:
			{
				IL_1E:
				int arg_22_0 = 0;
				goto IL_2B;
			}
			case 1:
				goto IL_2B;
			}
			goto IL_1E;
			IL_2B:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			return this.eval_b(string.Concat(new object[]
			{
				this.specAddressPrefix,
				"getGameMetaData/",
				new string(this.record.gamePlatform),
				"/",
				this.record.gameId,
				"/1/token"
			}));
		}
		public JObject getLastChunkInfo()
		{
			if (1 != 0)
			{
			}
			int arg_0E_0 = 0;
			switch ((1757660217 == 1757660217) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_34:
				break;
			case 1:
				goto IL_35;
			}
			goto IL_34;
			IL_35:
			if (0 != 0)
			{
			}
			return this.eval_b(string.Concat(new object[]
			{
				this.specAddressPrefix,
				"getLastChunkInfo/",
				new string(this.record.gamePlatform),
				"/",
				this.record.gameId,
				"/30000/token"
			}));
		}
		private bool eval_a(int A_0, bool A_1)
		{
			int num = 0;
			switch (num)
			{
			case 0:
			{
				IL_0F:
				switch (0)
				{
				case 0:
					goto IL_44;
				}
				WebClient webClient;
				int num2;
				while (true)
				{
					IL_19:
					byte[] value;
					switch (num)
					{
					case 0:
						if (A_1)
						{
							num = 5;
							continue;
						}
						num = 4;
						continue;
					case 1:
						try
						{
							IL_A6:
							num = 4;
							while (true)
							{
								switch (num)
								{
								case 0:
									goto IL_227;
								case 1:
									goto IL_21A;
								case 2:
									goto IL_21A;
								case 3:
									switch ((22695822 == 22695822) ? 1 : 0)
									{
									case 0:
									case 2:
										goto IL_227;
									case 1:
										IL_104:
										if (0 != 0)
										{
										}
										value = webClient.DownloadData(string.Concat(new object[]
										{
											this.specAddressPrefix,
											"getKeyFrame/",
											new string(this.record.gamePlatform),
											"/",
											this.record.gameId,
											"/",
											A_0,
											"/token"
										}));
										num = 1;
										continue;
									}
									goto IL_104;
								case 4:
									switch (0)
									{
									case 0:
										goto IL_D5;
									}
									continue;
								}
								IL_D5:
								if (A_1)
								{
									num = 3;
									continue;
								}
								value = webClient.DownloadData(string.Concat(new object[]
								{
									this.specAddressPrefix,
									"getGameDataChunk/",
									new string(this.record.gamePlatform),
									"/",
									this.record.gameId,
									"/",
									A_0,
									"/token"
								}));
								num = 2;
								continue;
								IL_21A:
								num = 0;
							}
							IL_227:
							goto IL_2A0;
						}
						catch (WebException)
						{
							if (num2++ < 3)
							{
								System.Threading.Thread.Sleep(5000);
								goto IL_A6;
							}
							return false;
						}
						goto IL_247;
						IL_2A0:
						num = 0;
						continue;
					case 2:
						goto IL_96;
					case 3:
						goto IL_247;
					case 4:
						if (!this.eval_b.ContainsKey(A_0))
						{
							num = 3;
							continue;
						}
						goto IL_98;
					case 5:
						num = 6;
						continue;
					case 6:
						if (!this.eval_a.ContainsKey(A_0))
						{
							num = 8;
							continue;
						}
						goto IL_98;
					case 7:
						goto IL_261;
					case 8:
						if (1 != 0)
						{
						}
						this.eval_a.Add(A_0, value);
						num = 2;
						continue;
					}
					goto IL_44;
					IL_247:
					this.eval_b.Add(A_0, value);
					num = 7;
				}
				IL_96:
				IL_98:
				int arg_9C_0 = 0;
				return true;
				IL_261:
				goto IL_98;
				IL_44:
				webClient = new WebClient();
				num2 = 0;
				num = 1;
				goto IL_19;
			}
			}
			goto IL_0F;
		}
		private bool eval_b()
		{
			int num = 5;
			while (true)
			{
				switch (num)
				{
				case 0:
					return false;
				case 1:
					return true;
				case 2:
					goto IL_BC;
				case 4:
					switch ((1241064882 == 1241064882) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_BC;
					case 1:
					{
						IL_76:
						int arg_7A_0 = 0;
						if (0 != 0)
						{
						}
						if (1 != 0)
						{
						}
						if (!this.eval_a(this.chunkGot, false))
						{
							num = 0;
							continue;
						}
						this.chunkGot++;
						num = 3;
						continue;
					}
					}
					goto IL_76;
				case 5:
					switch (0)
					{
					case 0:
						goto IL_31;
					}
					continue;
				}
				goto IL_31;
				IL_BC:
				if (this.chunkGot > this.chunkCount)
				{
					num = 1;
					continue;
				}
				num = 4;
				continue;
				IL_B0:
				num = 2;
				continue;
				IL_31:
				goto IL_B0;
			}
			return false;
		}
		private bool eval_a()
		{
			int num = 2;
			while (true)
			{
				switch (num)
				{
				case 0:
					goto IL_B0;
				case 1:
					goto IL_CA;
				case 2:
					switch (0)
					{
					case 0:
						goto IL_31;
					}
					continue;
				case 3:
					switch ((906888771 == 906888771) ? 1 : 0)
					{
					case 0:
					case 2:
						goto IL_B0;
					case 1:
						IL_80:
						if (0 != 0)
						{
						}
						if (!this.eval_a(this.keyframeGot, true))
						{
							num = 5;
							continue;
						}
						if (1 != 0)
						{
						}
						this.keyframeGot++;
						num = 4;
						continue;
					}
					goto IL_80;
				case 5:
					return false;
				}
				goto IL_31;
				IL_B0:
				if (this.keyframeGot > this.keyframeCount)
				{
					num = 1;
					continue;
				}
				num = 3;
				continue;
				IL_A4:
				num = 0;
				continue;
				IL_31:
				goto IL_A4;
			}
			return false;
			IL_CA:
			int arg_CE_0 = 0;
			return true;
		}
		static LoLRecorder()
		{
			// Note: this type is marked as 'beforefieldinit'.
			switch ((921605583 == 921605583) ? 1 : 0)
			{
			case 0:
			case 2:
				IL_1E:
				break;
			case 1:
				goto IL_1F;
			}
			goto IL_1E;
			IL_1F:
			if (1 != 0)
			{
			}
			if (0 != 0)
			{
			}
			int arg_34_0 = 0;
			LoLRecorder.eval_h = 0u;
		}
	}
}
